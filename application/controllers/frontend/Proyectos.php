<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Proyectos extends CI_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->model('frontend/proyectos_model');
		$this->load->model('frontend/empresa_model');
	}

	public function index()
	{	
		/*if ($this->input->post('tipo_proyecto')) {
			$proyectos = $this->proyectos_model->proyectos_tipo($this->input->post('tipo_proyecto'));
		} else {
			$proyectos = $this->proyectos_model->proyectos();
		}*/

		$data_header = array(
			'proyectos'	=> $this->proyectos_model->proyectos(),
			'telefonos' => $this->empresa_model->telefonos(),
			'correos' => $this->empresa_model->correos()	
		);

		$data = array(
			'sliders' => $this->proyectos_model->slider(),
			'correos' => $this->empresa_model->correos(),	
			'redes' => $this->empresa_model->redes(),	
			'telefonos' => $this->empresa_model->telefonos(),
			'empresa' => $this->empresa_model->getEmpresa(1),
			'activado'	=> false		
		);

		$this->load->view('frontend/layout/header', $data_header);
		$this->load->view('frontend/proyectos', $data);
		$this->load->view('frontend/layout/footer');
	}

	public function venta()
	{
		$proyectos = $this->proyectos_model->proyectos_tipo('p_venta');

		$data_header = array(
			'proyectos'	=> $proyectos,
			'telefonos' => $this->empresa_model->telefonos(),
			'correos' => $this->empresa_model->correos()	
		);

		$data = array(
			'sliders' => $this->proyectos_model->slider(),
			'correos' => $this->empresa_model->correos(),	
			'redes' => $this->empresa_model->redes(),	
			'telefonos' => $this->empresa_model->telefonos(),
			'empresa' => $this->empresa_model->getEmpresa(1),
			'activado'	=> 'venta'	
		);

		$this->load->view('frontend/layout/header', $data_header);
		$this->load->view('frontend/proyectos', $data);
		$this->load->view('frontend/layout/footer');
	}

	public function proximos()
	{
		$proyectos = $this->proyectos_model->proyectos_tipo('p_proximo');

		$data_header = array(
			'proyectos'	=> $proyectos,
			'telefonos' => $this->empresa_model->telefonos(),
			'correos' => $this->empresa_model->correos()	
		);

		$data = array(
			'sliders' => $this->proyectos_model->slider(),
			'correos' => $this->empresa_model->correos(),	
			'redes' => $this->empresa_model->redes(),	
			'telefonos' => $this->empresa_model->telefonos(),
			'empresa' => $this->empresa_model->getEmpresa(1),
			'activado'	=> 'proximos'	
		);

		$this->load->view('frontend/layout/header', $data_header);
		$this->load->view('frontend/proyectos', $data);
		$this->load->view('frontend/layout/footer');
	}

	public function entregados()
	{
		$proyectos = $this->proyectos_model->proyectos_tipo('p_entregado');

		$data_header = array(
			'proyectos'	=> $proyectos,
			'telefonos' => $this->empresa_model->telefonos(),
			'correos' => $this->empresa_model->correos()	
		);

		$data = array(
			'sliders' => $this->proyectos_model->slider(),
			'correos' => $this->empresa_model->correos(),	
			'redes' => $this->empresa_model->redes(),	
			'telefonos' => $this->empresa_model->telefonos(),
			'empresa' => $this->empresa_model->getEmpresa(1),
			'activado'	=> 'entregados'	
		);

		$this->load->view('frontend/layout/header', $data_header);
		$this->load->view('frontend/proyectos', $data);
		$this->load->view('frontend/layout/footer');
	}

	public function render($slug){
		$proyectos = $this->proyectos_model->buscar_proyecto($slug);
		$data_header = array(
			'proyecto'	=> $proyectos,
			'telefonos' => $this->empresa_model->telefonos()	
		);

		$data = array(
			'sliders' => $this->proyectos_model->slider(),
			'correos' => $this->empresa_model->correos(),	
			'redes' => $this->empresa_model->redes(),	
			'telefonos' => $this->empresa_model->telefonos(),
			'empresa' => $this->empresa_model->getEmpresa(1),
			'galerias'	=> $this->proyectos_model->galerias($proyectos->id),
			'planos'	=> $this->proyectos_model->planos($proyectos->id),
			'proyectos'	=> $this->proyectos_model->proyectos(),
		);
		$this->load->view('frontend/layout/header', $data_header);
		$this->load->view('frontend/proyecto', $data);
		$this->load->view('frontend/layout/footer');
	}

}
?>