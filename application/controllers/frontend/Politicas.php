<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Politicas extends CI_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->model('frontend/proyectos_model');
		$this->load->model('frontend/empresa_model');
		$this->load->model('administrador/politicas_model');
	}

	public function navegacion(){
		$data_header = array(
			'proyectos'	=> $this->proyectos_model->proyectos(),
			'telefonos' => $this->empresa_model->telefonos(),
			'correos' => $this->empresa_model->correos()		
		);

		$data = array(
			'empresa' => $this->empresa_model->getEmpresa(1),
			'correos' => $this->empresa_model->correos(),	
			'redes' => $this->empresa_model->redes(),	
			'telefonos' => $this->empresa_model->telefonos(),
			'politicas' => $this->politicas_model->getPoliticas()
		);

		$this->load->view('frontend/layout/header', $data_header);
		$this->load->view('frontend/politicas/navegacion', $data);
		$this->load->view('frontend/layout/footer');
	}

	public function privacidad(){
		$data_header = array(
			'proyectos'	=> $this->proyectos_model->proyectos(),
			'telefonos' => $this->empresa_model->telefonos(),
			'correos' => $this->empresa_model->correos()		
		);

		$data = array(
			'empresa' => $this->empresa_model->getEmpresa(1),
			'correos' => $this->empresa_model->correos(),	
			'redes' => $this->empresa_model->redes(),	
			'telefonos' => $this->empresa_model->telefonos(),
			'politicas' => $this->politicas_model->getPoliticas()
		);

		$this->load->view('frontend/layout/header', $data_header);
		$this->load->view('frontend/politicas/privacidad', $data);
		$this->load->view('frontend/layout/footer');
	}

	public function proteccion(){
		$data_header = array(
			'proyectos'	=> $this->proyectos_model->proyectos(),
			'telefonos' => $this->empresa_model->telefonos(),
			'correos' => $this->empresa_model->correos()		
		);

		$data = array(
			'empresa' => $this->empresa_model->getEmpresa(1),
			'correos' => $this->empresa_model->correos(),	
			'redes' => $this->empresa_model->redes(),	
			'telefonos' => $this->empresa_model->telefonos(),
			'politicas' => $this->politicas_model->getPoliticas()
		);

		$this->load->view('frontend/layout/header', $data_header);
		$this->load->view('frontend/politicas/proteccion', $data);
		$this->load->view('frontend/layout/footer');
	}

}

/* End of file Nosotros.php */
/* Location: ./application/controllers/frontend/Nosotros.php */