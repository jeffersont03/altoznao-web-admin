<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Referidos extends CI_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->model('frontend/proyectos_model');
		$this->load->model('frontend/empresa_model');

	}

	public function index(){
		$data_header = array(
			'proyectos'	=> $this->proyectos_model->proyectos(),
			'telefonos' => $this->empresa_model->telefonos(),
			'correos' => $this->empresa_model->correos(),		
		);

		$data = array(
			'empresa' => $this->empresa_model->getEmpresa(1),
			'correos' => $this->empresa_model->correos(),	
			'redes' => $this->empresa_model->redes(),	
			'telefonos' => $this->empresa_model->telefonos(),
			'referidos' => $this->empresa_model->referidos()
		);

		$this->load->view('frontend/layout/header', $data_header);
		$this->load->view('frontend/referidos', $data);
		$this->load->view('frontend/layout/footer');
	}

	
}

?>