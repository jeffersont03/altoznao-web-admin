<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Contacto extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('frontend/proyectos_model');
		$this->load->model('frontend/empresa_model');	
	}

	public function index(){
		$data_header = array(
			'proyectos'	=> $this->proyectos_model->proyectos(),
			'correos' => $this->empresa_model->correos(),	
			'redes' => $this->empresa_model->redes(),	
			'telefonos' => $this->empresa_model->telefonos(),
			'empresa' => $this->empresa_model->getEmpresa(1),
		);

		$this->load->view('frontend/layout/header', $data_header);
		$this->load->view('frontend/contacto');
		$this->load->view('frontend/layout/footer');
	}

}

/* End of file Contacto.php */
/* Location: ./application/controllers/frontend/Contacto.php */