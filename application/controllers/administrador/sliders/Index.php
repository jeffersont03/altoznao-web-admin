<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Index extends CI_Controller {

	private $ruta;

	public function __construct(){
		parent::__construct();
		$this->load->model('administrador/slider_model');
		$this->load->model('administrador/proyectos_model');
		$this->ruta = './public/frontend/img/proyectos/';

		date_default_timezone_set('America/Lima');
	}

	public function index()
	{
		$this->load->view('administrador/layouts/header');
		$this->load->view('administrador/layouts/aside');
		$this->load->view('administrador/sliders/index');
		$this->load->view('administrador/layouts/footer');
	}

	public function buscar()
	{
		$id = $this->input->post('id_slider');
		$data = $this->slider_model->buscar($id);
		echo json_encode($data);
	}

	public function listar()
	{
		$data = $this->slider_model->listar();
		echo json_encode($data);
	}

	public function nuevo()
	{
		$texto =  $this->input->post('texto');
		
		# imagen banner
		$config['upload_path']   = './public/frontend/img/slide';
        $config['allowed_types'] = 'jpg|png|jpeg';
        $config['encrypt_name'] = TRUE;
        $this->load->library('upload', $config);

        if ($this->upload->do_upload('imagen')) {
        	# se guardo
        	$data = array('upload_data' => $this->upload->data());
        	$imagen = 'slide/' . $data['upload_data']['file_name'];

        	$data = array(
		        'imagen'	=> $imagen,
		        'texto' 	=> $texto
			);

			$respuesta = false;
			$respuesta = $this->slider_model->nuevo($data);

			if ($respuesta) {
				$response = array(
					'cabecera'	=> 'M_0',
					'mensaje'	=> 'El proyecto se registro correctamente',
					'id'		=> $respuesta
				);
				echo json_encode($response);exit();
			} else {
				$response = array(
					'cabecera'	=> 'M_1',
					'mensaje'	=> 'Hubo problemas al registrar, intentelo nuevamente.'
				);
				echo json_encode($response);exit();
			}	

        } else {
        	$response = array(
				'cabecera'	=> 'M_1',
				'mensaje'	=> 'No se pudo subir la imagen del banner.'
			);
			echo json_encode($response);exit();
        }
	}

	public function editar()
	{
		$id =  $this->input->post('id_banner');
		$texto =  $this->input->post('texto');
		$data = [];
		
		# imagen banner
		$config['upload_path']   = $this->ruta . 'sliders/';
        $config['allowed_types'] = 'jpg|png|jpeg';
        $config['encrypt_name'] = TRUE;
        $this->load->library('upload', $config);

        if ($this->upload->do_upload('imagen')) {
        	# eliminando imagen
			$data_temp = $this->slider_model->buscar($id);
			unlink($this->ruta . $data_temp->imagen);

        	# se guardo
        	$data = array('upload_data' => $this->upload->data());
        	$imagen = 'sliders/' . $data['upload_data']['file_name'];

        	$data = array(
		        'imagen' 		=> $imagen,
		        'texto' 	=> $texto
			);
        } else {
        	$data = array(
		        'texto' 	=> $texto
			);
        }

        try {
			$this->slider_model->actualizar($data, $id);
			$response = array(
				'cabecera'	=> 'M_0',
				'mensaje'	=> 'El banner se actualizo correctamente.'
			);
			echo json_encode($response);exit();
		} catch (Exception $e) {
			$response = array(
				'cabecera'	=> 'M_1',
				'mensaje'	=> 'Hubo problemas al eliminar' . $e->getMessage()
			);
			echo json_encode($response);exit();
		}
	}

	public function eliminar()
	{
		$id = $this->input->post('id_banner');

		// eliminando imagen del server
		$data_temp = $this->slider_model->buscar($id);
		if (file_exists('./public/frontend/img/' . $data_temp->imagen)) {
			unlink('./public/frontend/img/' . $data_temp->imagen);
		}

		try {
			$this->slider_model->eliminar($id);
			$response = array(
				'cabecera'	=> 'M_0',
				'mensaje'	=> 'El banner se elimino correctamente'
			);
			echo json_encode($response);exit();
		} catch (Exception $e) {
			$response = array(
				'cabecera'	=> 'M_1',
				'mensaje'	=> 'Hubo problemas al eliminar' . $e->getMessage()
			);
			echo json_encode($response);exit();
		}
	}
}
?>