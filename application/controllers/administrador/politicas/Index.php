<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Index extends CI_Controller {

	private $ruta;

	public function __construct(){
		parent::__construct();
		$this->load->model('administrador/politicas_model');
		$this->ruta = './public/frontend/img/pdf/';
        
        date_default_timezone_set('America/Lima');
	}
	
	public function navegacion(){
		$this->load->view('administrador/layouts/header');
		$this->load->view('administrador/layouts/aside');
		$this->load->view('administrador/politicas/navegacion');
		$this->load->view('administrador/layouts/footer');
	}
	
	public function privacidad(){
		$this->load->view('administrador/layouts/header');
		$this->load->view('administrador/layouts/aside');
		$this->load->view('administrador/politicas/privacidad');
		$this->load->view('administrador/layouts/footer');
	}
	
	public function proteccion(){
		$this->load->view('administrador/layouts/header');
		$this->load->view('administrador/layouts/aside');
		$this->load->view('administrador/politicas/proteccion');
		$this->load->view('administrador/layouts/footer');
	}

	public function procedimiento_arcos(){
		$this->load->view('administrador/layouts/header');
		$this->load->view('administrador/layouts/aside');
		$this->load->view('administrador/politicas/procedimiento-arcos');
		$this->load->view('administrador/layouts/footer');
	}

	public function formulario_arcos(){
		$this->load->view('administrador/layouts/header');
		$this->load->view('administrador/layouts/aside');
		$this->load->view('administrador/politicas/formulario-arcos');
		$this->load->view('administrador/layouts/footer');
	}

	public function subir_pdf(){
		$data = array(
			'pdfs' => $this->politicas_model->getPdfs()
		);

		$this->load->view('administrador/layouts/header');
		$this->load->view('administrador/layouts/aside');
		$this->load->view('administrador/politicas/subir-pdf', $data);
		$this->load->view('administrador/layouts/footer');
	}

	public function subir_documento()
	{
		$id = $this->input->post('p_id');
		$titulo = $this->input->post('titulo');
		$type = $this->input->post('type');

		$config_doc['upload_path']   = $this->ruta;
		$config_doc['allowed_types'] = 'pdf|doc|docx|DOC|DOCX';

		$config['max_size'] = 10000;
		
		$this->load->library('upload', $config_doc);
		// $this->upload->initialize($config_doc);

        if ($this->upload->do_upload('file')) {
			# eliminando file
			// $data_temp = $this->proyectos_model->buscar_plano($id);
			// unlink($this->ruta . $data_temp->imagen);

        	# se guardo
        	$data = array('upload_data' => $this->upload->data());
			$file = $data['upload_data']['file_name'];

        	$data = array(
				'titulo' 			=> ($type == 'doc') ? ('PDF: ' . $titulo) : $titulo,
		        'contenido' 		=> $file,
			);

			try {
				if(isset($id) && !is_null($id) && $id !== 'null') {
					$this->politicas_model->actualizar_politicas($data, $id);
				} else {
					$this->politicas_model->guardar_politicas($data);
				}

				$response = array(
					'cabecera'	=> 'M_0',
					'mensaje'	=> 'El archivo se actualizó correctamente.'
				);
				echo json_encode($response);exit();
			} catch (Exception $e) {
				$response = array(
					'cabecera'	=> 'M_1',
					'mensaje'	=> 'Hubo problemas al actualizar' . $e->getMessage()
				);
				echo json_encode($response);exit();
			}
        } else {
        	$response = array(
				'cabecera'	=> 'M_1',
				'mensaje'	=> 'No se pudo subir el archivo.'
			);
			echo json_encode($response);exit();
        }     
	}
    
    public function politicas_datos()
	{
		$id = $this->input->post("id");
		if(isset($id)){ $data = $this->politicas_model->getPoliticas($id); }
		else { $data = $this->politicas_model->getPoliticas(); }
		echo json_encode($data);
	}

	public function actualizar_politicas()
	{
		$data_reg['titulo'] = $this->input->post('titulo');
		$data_reg['contenido'] = $this->input->post('contenido');
		$id = $this->input->post('p_id');

		try {
			$this->politicas_model->actualizar_politicas($data_reg, $id);
			$response = array(
				'cabecera'	=> 'M_0',
				'mensaje'	=> 'Los datos de la política se actualizaron correctamente.'
			);
			echo json_encode($response);exit();
		} catch (Exception $e) {
			$response = array(
				'cabecera'	=> 'M_1',
				'mensaje'	=> 'Hubo problemas al actualizar' . $e->getMessage()
			);
			echo json_encode($response);exit();
		}
	}
}

/* End of file Index.php */
/* Location: ./application/controllers/administrador/politicas/Index.php */