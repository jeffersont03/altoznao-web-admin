<?php
	defined('BASEPATH') OR exit('No direct script access allowed');

	class Correo extends CI_Controller {

		public function __construct(){
			parent::__construct();
			$this->load->helper('url');
		}

		public function enviar(){
			$nombres = $this->input->post('nombres');
			$telefono = $this->input->post('telefono');
			$correo = $this->input->post('email');
			$mensaje = $this->input->post('mensaje');
			
			$recaptcha = $_POST["g-recaptcha-response"];
			
			$url = 'https://www.google.com/recaptcha/api/siteverify';
        	$data = array(
        		'secret' => '6LdZ-qYUAAAAAJKTIXPenNtFURqr1EnaFRzSSdEA',
        		'response' => $recaptcha
        	);
        	$options = array(
        		'http' => array (
        			'method' => 'POST',
        			'header'  => 'Content-type: application/x-www-form-urlencoded',
        			'content' => http_build_query($data)
        		)
        	);
        	$context  = stream_context_create($options);
        	$verify = file_get_contents($url, false, $context);
        	$captcha_success = json_decode($verify);
        	if ($captcha_success->success) {

    			$título = 'Contacto WEB - Altozano';
    
    			$para = $this->input->post('destino'); 
    
    			$message = "<table width=\"100%\" cellspacing=\"0\" cellpadding=\"0\" border=\"0\" style=\"width:100.0%;background:#5b5b5b;\">";
    			$message .= "<tbody><tr><td style=\"padding:0in 0in 0in 0in;\"><div align=\"center\">";
    			$message .= "<table width=\"850\" cellspacing=\"0\" cellpadding=\"0\" border=\"0\" style=\"width:600.5pt;\">";
    			$message .= "<tbody><tr><td style=\"padding:0in 0in 0in 0in;\"><div align=\"center\">";
    			$message .= "<table cellspacing=\"0\" cellpadding=\"0\" border=\"0\" width=\"100%\"><tbody><tr><td style=\"padding:0in 0in 0in 0in;\">";
    			$message .= "<div><p align=\"center\" style=\"text-align:center;line-height:22.5pt;\">&nbsp;</p></div>";
    			$message .= "<table width=\"100%\" cellspacing=\"0\" cellpadding=\"0\" border=\"1\" style=\"width:100.0%;background:white;border-top:solid #bd1a2e 4.5pt;border-left:none;border-bottom:solid #d1d1d1 1.0pt;border-right:none;border-radius:5px;\">";
    			$message .= "<tbody><tr><td style=\"border:none;padding:16.25pt 0in 7.5pt 0in;\">";
    			$message .= "<div>";
    
    			$message .= "<p align=\"center\" style=\"margin:9px 0px 15px;text-align:left;padding:0 20px;\"><span style=\"font-family:arial;font-size:15px;\">";
    			$message .= "Contacto WEB - Altozano";
    			$message .= "</span></p>";
    
    			$message .= "<p align=\"center\" style=\"margin:9px 0px;text-align:left;padding:0 20px;\"><span style=\"font-family:arial;font-size:15px;\">";
    			$message .= "<strong style=\"margin-right:5px;\">Nombres:</strong>".$nombres;
    			$message .= "</span></p>";
    
    			$message .= "<p align=\"center\" style=\"margin:9px 0px;text-align:left;padding:0 20px;\"><span style=\"font-family:arial;font-size:15px;\">";
    			$message .= "<strong style=\"margin-right:5px;\">Correo:</strong>".$correo;
    			$message .= "</span></p>";
    
    			if ($this->input->post('dni')) {
    				$message .= "<p align=\"center\" style=\"margin:9px 0px;text-align:left;padding:0 20px;\"><span style=\"font-family:arial;font-size:15px;\">";
    				$message .= "<strong style=\"margin-right:5px;\">DNI:</strong>".$this->input->post('dni');
    				$message .= "</span></p>";
    			}
    
    			$message .= "<p align=\"center\" style=\"margin:9px 0px;text-align:left;padding:0 20px;\"><span style=\"font-family:arial;font-size:15px;\">";
    			$message .= "<strong style=\"margin-right:5px;\">Telefono:</strong>".$telefono;
    			$message .= "</span></p>";
    
    			$message .= "<p align=\"center\" style=\"margin:9px 0px;text-align:left;padding:0 20px;\"><span style=\"font-family:arial;font-size:15px;\">";
    			$message .= "<strong style=\"margin-right:5px;\">Mensaje:</strong>".$mensaje;
    			$message .= "</span></p>";
    
    			$message .= "<p align=\"center\" style=\"text-align:center;\">";
    			$message .= "</p>";
    
    			$message .= "</div>";
    			$message .= "<p><span>&nbsp;</span></p>";
    			$message .= "</td></tr></tbody>";
    			$message .= "</table></td></tr></tbody>";
    			$message .= "</table></div></td></tr>";
    			$message .= "</tbody>";
    			$message .= "</table></div><br><br>\r\n\r\n</td></tr>";
    			$message .= "</tbody>";
    			$message .= "</table>";
    
    
    			$cabeceras  = 'MIME-Version: 1.0' . "\r\n";
    			$cabeceras .= 'Content-type: text/html; charset=UTF-8' . "\r\n";
    			$cabeceras .= 'From: WEB-Altozano<'.$correo.'>' . "\r\n";
    
    			try {
    				mail($para, $título, $message, $cabeceras);
    				redirect(base_url() . 'gracias');
    			} catch (Exception $e) {
    				echo 'Excepción capturada: ',  $e->getMessage(), "\n";
    			}
        	} else {
        	    $this->session->set_flashdata('valida','Debe seleccionar el captcha'); 
        		header("Location:".$_SERVER['HTTP_REFERER']);  
        	}			
		}

		public function enviar_proyecto(){
			$nombres = $this->input->post('nombres');
			$telefono = $this->input->post('telefono');
			$correo = $this->input->post('email');
			$mensaje = $this->input->post('mensaje');
			
			$recaptcha = $_POST["g-recaptcha-response"];
			
			$url = 'https://www.google.com/recaptcha/api/siteverify';
        	$data = array(
        		'secret' => '6LdZ-qYUAAAAAJKTIXPenNtFURqr1EnaFRzSSdEA',
        		'response' => $recaptcha
        	);
        	$options = array(
        		'http' => array (
        			'method' => 'POST',
        			'header'  => 'Content-type: application/x-www-form-urlencoded',
        			'content' => http_build_query($data)
        		)
        	);
        	$context  = stream_context_create($options);
        	$verify = file_get_contents($url, false, $context);
        	$captcha_success = json_decode($verify);
        	if ($captcha_success->success) {
        		$título = 'Contacto WEB - Altozano | Proyecto ' . $this->input->post('proyecto_env');

    			$para = $this->input->post('destino'); 
    
    			$message = "<table width=\"100%\" cellspacing=\"0\" cellpadding=\"0\" border=\"0\" style=\"width:100.0%;background:#5b5b5b;\">";
    			$message .= "<tbody><tr><td style=\"padding:0in 0in 0in 0in;\"><div align=\"center\">";
    			$message .= "<table width=\"850\" cellspacing=\"0\" cellpadding=\"0\" border=\"0\" style=\"width:600.5pt;\">";
    			$message .= "<tbody><tr><td style=\"padding:0in 0in 0in 0in;\"><div align=\"center\">";
    			$message .= "<table cellspacing=\"0\" cellpadding=\"0\" border=\"0\" width=\"100%\"><tbody><tr><td style=\"padding:0in 0in 0in 0in;\">";
    			$message .= "<div><p align=\"center\" style=\"text-align:center;line-height:22.5pt;\">&nbsp;</p></div>";
    			$message .= "<table width=\"100%\" cellspacing=\"0\" cellpadding=\"0\" border=\"1\" style=\"width:100.0%;background:white;border-top:solid #bd1a2e 4.5pt;border-left:none;border-bottom:solid #d1d1d1 1.0pt;border-right:none;border-radius:5px;\">";
    			$message .= "<tbody><tr><td style=\"border:none;padding:16.25pt 0in 7.5pt 0in;\">";
    			$message .= "<div>";
    
    			$message .= "<p align=\"center\" style=\"margin:9px 0px 15px;text-align:left;padding:0 20px;\"><span style=\"font-family:arial;font-size:15px;\">";
    			$message .= "Contacto WEB - Altozano";
    			$message .= "</span></p>";
    
    			$message .= "<p align=\"center\" style=\"margin:9px 0px;text-align:left;padding:0 20px;\"><span style=\"font-family:arial;font-size:15px;\">";
    			$message .= "<strong style=\"margin-right:5px;\">Nombres:</strong>".$nombres;
    			$message .= "</span></p>";
    
    			$message .= "<p align=\"center\" style=\"margin:9px 0px;text-align:left;padding:0 20px;\"><span style=\"font-family:arial;font-size:15px;\">";
    			$message .= "<strong style=\"margin-right:5px;\">Correo:</strong>".$correo;
    			$message .= "</span></p>";
    
    			$message .= "<p align=\"center\" style=\"margin:9px 0px;text-align:left;padding:0 20px;\"><span style=\"font-family:arial;font-size:15px;\">";
    			$message .= "<strong style=\"margin-right:5px;\">Telefono:</strong>".$telefono;
    			$message .= "</span></p>";
    
    			$message .= "<p align=\"center\" style=\"margin:9px 0px;text-align:left;padding:0 20px;\"><span style=\"font-family:arial;font-size:15px;\">";
    			$message .= "<strong style=\"margin-right:5px;\">Mensaje:</strong>".$mensaje;
    			$message .= "</span></p>";
    
    			$message .= "<p align=\"center\" style=\"text-align:center;\">";
    			$message .= "</p>";
    
    			$message .= "</div>";
    			$message .= "<p><span>&nbsp;</span></p>";
    			$message .= "</td></tr></tbody>";
    			$message .= "</table></td></tr></tbody>";
    			$message .= "</table></div></td></tr>";
    			$message .= "</tbody>";
    			$message .= "</table></div><br><br>\r\n\r\n</td></tr>";
    			$message .= "</tbody>";
    			$message .= "</table>";
    
    
    			$cabeceras  = 'MIME-Version: 1.0' . "\r\n";
    			$cabeceras .= 'Content-type: text/html; charset=UTF-8' . "\r\n";
    			$cabeceras .= 'From: WEB-Altozano<'.$correo.'>' . "\r\n";
    
    			try {
    				mail($para, $título, $message, $cabeceras);
    				redirect(base_url() . 'gracias');
    			} catch (Exception $e) {
    				echo 'Excepción capturada: ',  $e->getMessage(), "\n";
    			}
        	} else {
        	    $this->session->set_flashdata('valida','Debe seleccionar el captcha'); 
        		header("Location:".$_SERVER['HTTP_REFERER']);  
        	}

			
						
		}

		public function cita(){
			$nombres = $this->input->post('nombres');
			$telefono = $this->input->post('telefono');
			$correo = $this->input->post('correo');
			$fecha = $this->input->post('fecha');
			$mensaje = $this->input->post('mensaje');
			
			$recaptcha = $_POST["g-recaptcha-response"];
			
			$url = 'https://www.google.com/recaptcha/api/siteverify';
        	$data = array(
        		'secret' => '6LdZ-qYUAAAAAJKTIXPenNtFURqr1EnaFRzSSdEA',
        		'response' => $recaptcha
        	);
        	$options = array(
        		'http' => array (
        			'method' => 'POST',
        			'header'  => 'Content-type: application/x-www-form-urlencoded',
        			'content' => http_build_query($data)
        		)
        	);
        	$context  = stream_context_create($options);
        	$verify = file_get_contents($url, false, $context);
        	$captcha_success = json_decode($verify);
        	if ($captcha_success->success) {

    			$título = 'Agendar Cita WEB - Altozano';
    
    			$para = $this->input->post('destino'); 
    
    			$message = "<table width=\"100%\" cellspacing=\"0\" cellpadding=\"0\" border=\"0\" style=\"width:100.0%;background:#5b5b5b;\">";
    			$message .= "<tbody><tr><td style=\"padding:0in 0in 0in 0in;\"><div align=\"center\">";
    			$message .= "<table width=\"850\" cellspacing=\"0\" cellpadding=\"0\" border=\"0\" style=\"width:600.5pt;\">";
    			$message .= "<tbody><tr><td style=\"padding:0in 0in 0in 0in;\"><div align=\"center\">";
    			$message .= "<table cellspacing=\"0\" cellpadding=\"0\" border=\"0\" width=\"100%\"><tbody><tr><td style=\"padding:0in 0in 0in 0in;\">";
    			$message .= "<div><p align=\"center\" style=\"text-align:center;line-height:22.5pt;\">&nbsp;</p></div>";
    			$message .= "<table width=\"100%\" cellspacing=\"0\" cellpadding=\"0\" border=\"1\" style=\"width:100.0%;background:white;border-top:solid #bd1a2e 4.5pt;border-left:none;border-bottom:solid #d1d1d1 1.0pt;border-right:none;border-radius:5px;\">";
    			$message .= "<tbody><tr><td style=\"border:none;padding:16.25pt 0in 7.5pt 0in;\">";
    			$message .= "<div>";
    
    			$message .= "<p align=\"center\" style=\"margin:9px 0px 15px;text-align:left;padding:0 20px;\"><span style=\"font-family:arial;font-size:15px;\">";
    			$message .= "Agendar Cita - Altozano";
    			$message .= "</span></p>";
    
    			if ($this->input->post('proyecto')) {
    				$message .= "<p align=\"center\" style=\"margin:9px 0px;text-align:left;padding:0 20px;\"><span style=\"font-family:arial;font-size:15px;\">";
    				$message .= "<strong style=\"margin-right:5px;\">Proyecto:</strong>".$this->input->post('proyecto');
    				$message .= "</span></p>";
    			}
    
    			$message .= "<p align=\"center\" style=\"margin:9px 0px;text-align:left;padding:0 20px;\"><span style=\"font-family:arial;font-size:15px;\">";
    			$message .= "<strong style=\"margin-right:5px;\">Nombres:</strong>".$nombres;
    			$message .= "</span></p>";
    
    			$message .= "<p align=\"center\" style=\"margin:9px 0px;text-align:left;padding:0 20px;\"><span style=\"font-family:arial;font-size:15px;\">";
    			$message .= "<strong style=\"margin-right:5px;\">Correo:</strong>".$correo;
    			$message .= "</span></p>";
    
    			$message .= "<p align=\"center\" style=\"margin:9px 0px;text-align:left;padding:0 20px;\"><span style=\"font-family:arial;font-size:15px;\">";
    			$message .= "<strong style=\"margin-right:5px;\">Telefono:</strong>".$telefono;
    			$message .= "</span></p>";
    
    			$message .= "<p align=\"center\" style=\"margin:9px 0px;text-align:left;padding:0 20px;\"><span style=\"font-family:arial;font-size:15px;\">";
    			$message .= "<strong style=\"margin-right:5px;\">Fecha de la cita:</strong>".$fecha;
    			$message .= "</span></p>";
    
    			$message .= "<p align=\"center\" style=\"margin:9px 0px;text-align:left;padding:0 20px;\"><span style=\"font-family:arial;font-size:15px;\">";
    			$message .= "<strong style=\"margin-right:5px;\">Mensaje:</strong>".$mensaje;
    			$message .= "</span></p>";
    
    			$message .= "<p align=\"center\" style=\"text-align:center;\">";
    			$message .= "</p>";
    
    			$message .= "</div>";
    			$message .= "<p><span>&nbsp;</span></p>";
    			$message .= "</td></tr></tbody>";
    			$message .= "</table></td></tr></tbody>";
    			$message .= "</table></div></td></tr>";
    			$message .= "</tbody>";
    			$message .= "</table></div><br><br>\r\n\r\n</td></tr>";
    			$message .= "</tbody>";
    			$message .= "</table>";
    
    
    			$cabeceras  = 'MIME-Version: 1.0' . "\r\n";
    			$cabeceras .= 'Content-type: text/html; charset=UTF-8' . "\r\n";
    			$cabeceras .= 'From: Agendar Cita WEB-Altozano<'.$correo.'>' . "\r\n";
    
    			try {
    				mail($para, $título, $message, $cabeceras);
    				redirect(base_url() . 'gracias');
    			} catch (Exception $e) {
    				echo 'Excepción capturada: ',  $e->getMessage(), "\n";
    			}
        	} else {
        	    $this->session->set_flashdata('valida_cita','Debe seleccionar el captcha'); 
        		header("Location:".$_SERVER['HTTP_REFERER']);  
        	}			
		}


		public function cotiza(){
			$nombres = $this->input->post('nombres');
			$telefono = $this->input->post('telefono');
			$correo = $this->input->post('correo');
			$fecha = $this->input->post('fecha');
			$mensaje = $this->input->post('dni');
			
			$recaptcha = $_POST["g-recaptcha-response"];
			
			$url = 'https://www.google.com/recaptcha/api/siteverify';
        	$data = array(
        		'secret' => '6LdZ-qYUAAAAAJKTIXPenNtFURqr1EnaFRzSSdEA',
        		'response' => $recaptcha
        	);
        	$options = array(
        		'http' => array (
        			'method' => 'POST',
        			'header'  => 'Content-type: application/x-www-form-urlencoded',
        			'content' => http_build_query($data)
        		)
        	);
        	$context  = stream_context_create($options);
        	$verify = file_get_contents($url, false, $context);
        	$captcha_success = json_decode($verify);
        	if ($captcha_success->success) {

    			$título = 'Agendar Cita WEB - Altozano';
    
    			$para = $this->input->post('destino'); 
    
    			$message = "<table width=\"100%\" cellspacing=\"0\" cellpadding=\"0\" border=\"0\" style=\"width:100.0%;background:#5b5b5b;\">";
    			$message .= "<tbody><tr><td style=\"padding:0in 0in 0in 0in;\"><div align=\"center\">";
    			$message .= "<table width=\"850\" cellspacing=\"0\" cellpadding=\"0\" border=\"0\" style=\"width:600.5pt;\">";
    			$message .= "<tbody><tr><td style=\"padding:0in 0in 0in 0in;\"><div align=\"center\">";
    			$message .= "<table cellspacing=\"0\" cellpadding=\"0\" border=\"0\" width=\"100%\"><tbody><tr><td style=\"padding:0in 0in 0in 0in;\">";
    			$message .= "<div><p align=\"center\" style=\"text-align:center;line-height:22.5pt;\">&nbsp;</p></div>";
    			$message .= "<table width=\"100%\" cellspacing=\"0\" cellpadding=\"0\" border=\"1\" style=\"width:100.0%;background:white;border-top:solid #bd1a2e 4.5pt;border-left:none;border-bottom:solid #d1d1d1 1.0pt;border-right:none;border-radius:5px;\">";
    			$message .= "<tbody><tr><td style=\"border:none;padding:16.25pt 0in 7.5pt 0in;\">";
    			$message .= "<div>";
    
    			$message .= "<p align=\"center\" style=\"margin:9px 0px 15px;text-align:left;padding:0 20px;\"><span style=\"font-family:arial;font-size:15px;\">";
    			$message .= "Agendar Cita - Altozano";
    			$message .= "</span></p>";
    
    			if ($this->input->post('proyecto')) {
    				$message .= "<p align=\"center\" style=\"margin:9px 0px;text-align:left;padding:0 20px;\"><span style=\"font-family:arial;font-size:15px;\">";
    				$message .= "<strong style=\"margin-right:5px;\">Proyecto:</strong>".$this->input->post('proyecto');
    				$message .= "</span></p>";
    			}
    
    			$message .= "<p align=\"center\" style=\"margin:9px 0px;text-align:left;padding:0 20px;\"><span style=\"font-family:arial;font-size:15px;\">";
    			$message .= "<strong style=\"margin-right:5px;\">Nombres:</strong>".$nombres;
    			$message .= "</span></p>";
    
    			$message .= "<p align=\"center\" style=\"margin:9px 0px;text-align:left;padding:0 20px;\"><span style=\"font-family:arial;font-size:15px;\">";
    			$message .= "<strong style=\"margin-right:5px;\">Correo:</strong>".$correo;
    			$message .= "</span></p>";
    
    			$message .= "<p align=\"center\" style=\"margin:9px 0px;text-align:left;padding:0 20px;\"><span style=\"font-family:arial;font-size:15px;\">";
    			$message .= "<strong style=\"margin-right:5px;\">Telefono:</strong>".$telefono;
    			$message .= "</span></p>";
    
    			$message .= "<p align=\"center\" style=\"margin:9px 0px;text-align:left;padding:0 20px;\"><span style=\"font-family:arial;font-size:15px;\">";
    			$message .= "<strong style=\"margin-right:5px;\">Fecha de la cita:</strong>".$fecha;
    			$message .= "</span></p>";
    
    			$message .= "<p align=\"center\" style=\"margin:9px 0px;text-align:left;padding:0 20px;\"><span style=\"font-family:arial;font-size:15px;\">";
    			$message .= "<strong style=\"margin-right:5px;\">DNI:</strong>".$mensaje;
    			$message .= "</span></p>";
    
    			$message .= "<p align=\"center\" style=\"text-align:center;\">";
    			$message .= "</p>";
    
    			$message .= "</div>";
    			$message .= "<p><span>&nbsp;</span></p>";
    			$message .= "</td></tr></tbody>";
    			$message .= "</table></td></tr></tbody>";
    			$message .= "</table></div></td></tr>";
    			$message .= "</tbody>";
    			$message .= "</table></div><br><br>\r\n\r\n</td></tr>";
    			$message .= "</tbody>";
    			$message .= "</table>";
    
    
    			$cabeceras  = 'MIME-Version: 1.0' . "\r\n";
    			$cabeceras .= 'Content-type: text/html; charset=UTF-8' . "\r\n";
    			$cabeceras .= 'From: Agendar Cita WEB-Altozano<'.$correo.'>' . "\r\n";
    
    			try {
    				mail($para, $título, $message, $cabeceras);
    				redirect(base_url() . 'gracias');
    			} catch (Exception $e) {
    				echo 'Excepción capturada: ',  $e->getMessage(), "\n";
    			}
        	} else {
        	    $this->session->set_flashdata('valida','Debe seleccionar el captcha'); 
        		header("Location:".$_SERVER['HTTP_REFERER']);  
        	}				
		}
		
		public function referidos(){
		    $nombres_propietario = $this->input->post('nombres_propietario');
            $dni_propietario = $this->input->post('dni_propietario');
            $telefono_propietario = $this->input->post('telefono_propietario');
            $proyecto_propietario = $this->input->post('proyecto_propietario');
            $torre_propietario = $this->input->post('torre_propietario');
            $correo_propietario = $this->input->post('correo_propietario');
            $nombres_referido = $this->input->post('nombres_referido');
            $proyecto_referido = $this->input->post('proyecto_referido');
            $dni_referido = $this->input->post('dni_referido');
            $telefono_referido = $this->input->post('telefono_referido');
			
			$recaptcha = $_POST["g-recaptcha-response"];
			
			$url = 'https://www.google.com/recaptcha/api/siteverify';
        	$data = array(
        		'secret' => '6LdZ-qYUAAAAAJKTIXPenNtFURqr1EnaFRzSSdEA',
        		'response' => $recaptcha
        	);
        	$options = array(
        		'http' => array (
        			'method' => 'POST',
        			'header'  => 'Content-type: application/x-www-form-urlencoded',
        			'content' => http_build_query($data)
        		)
        	);
        	$context  = stream_context_create($options);
        	$verify = file_get_contents($url, false, $context);
        	$captcha_success = json_decode($verify);
        	if ($captcha_success->success) {

    			$título = 'Referidos - Altozano';
    
    			$para = $this->input->post('correo_referido'); 
    
    			$message = "<table width=\"100%\" cellspacing=\"0\" cellpadding=\"0\" border=\"0\" style=\"width:100.0%;background:#5b5b5b;\">";
    			$message .= "<tbody><tr><td style=\"padding:0in 0in 0in 0in;\"><div align=\"center\">";
    			$message .= "<table width=\"850\" cellspacing=\"0\" cellpadding=\"0\" border=\"0\" style=\"width:600.5pt;\">";
    			$message .= "<tbody><tr><td style=\"padding:0in 0in 0in 0in;\"><div align=\"center\">";
    			$message .= "<table cellspacing=\"0\" cellpadding=\"0\" border=\"0\" width=\"100%\"><tbody><tr><td style=\"padding:0in 0in 0in 0in;\">";
    			$message .= "<div><p align=\"center\" style=\"text-align:center;line-height:22.5pt;\">&nbsp;</p></div>";
    			$message .= "<table width=\"100%\" cellspacing=\"0\" cellpadding=\"0\" border=\"1\" style=\"width:100.0%;background:white;border-top:solid #bd1a2e 4.5pt;border-left:none;border-bottom:solid #d1d1d1 1.0pt;border-right:none;border-radius:5px;\">";
    			$message .= "<tbody><tr><td style=\"border:none;padding:16.25pt 0in 7.5pt 0in;\">";
    			$message .= "<div>";
    
    			$message .= "<p align=\"center\" style=\"margin:9px 0px 15px;text-align:left;padding:0 20px;\"><span style=\"font-family:arial;font-size:15px;\">";
    			$message .= "Página de Referido - Altozano";
    			$message .= "</span></p>";
    
    			$message .= "<p align=\"center\" style=\"margin:9px 0px;text-align:left;padding:0 20px;\"><span style=\"font-family:arial;font-size:15px;\">";
    			$message .= "<strong style=\"margin-right:5px;\">Nombres y apellido del propietario:</strong>".$nombres_propietario;
    			$message .= "</span></p>";
    
    			$message .= "<p align=\"center\" style=\"margin:9px 0px;text-align:left;padding:0 20px;\"><span style=\"font-family:arial;font-size:15px;\">";
    			$message .= "<strong style=\"margin-right:5px;\">DNI Propietario:</strong>".$dni_propietario;
    			$message .= "</span></p>";
    
    			$message .= "<p align=\"center\" style=\"margin:9px 0px;text-align:left;padding:0 20px;\"><span style=\"font-family:arial;font-size:15px;\">";
    			$message .= "<strong style=\"margin-right:5px;\">Telefono Propietario:</strong>".$telefono_propietario;
    			$message .= "</span></p>";
    
    			$message .= "<p align=\"center\" style=\"margin:9px 0px;text-align:left;padding:0 20px;\"><span style=\"font-family:arial;font-size:15px;\">";
    			$message .= "<strong style=\"margin-right:5px;\">Proyecto Propietario:</strong>".$proyecto_propietario;
    			$message .= "</span></p>";
    
    			$message .= "<p align=\"center\" style=\"margin:9px 0px;text-align:left;padding:0 20px;\"><span style=\"font-family:arial;font-size:15px;\">";
    			$message .= "<strong style=\"margin-right:5px;\">Torre y n° de dpto:</strong>".$torre_propietario;
    			$message .= "</span></p>";
    			
    			$message .= "<p align=\"center\" style=\"margin:9px 0px;text-align:left;padding:0 20px;\"><span style=\"font-family:arial;font-size:15px;\">";
    			$message .= "<strong style=\"margin-right:5px;\">Correo propietario:</strong>".$correo_propietario;
    			$message .= "</span></p><br><br>";
    			
    			$message .= "<p align=\"center\" style=\"margin:9px 0px;text-align:left;padding:0 20px;\"><span style=\"font-family:arial;font-size:15px;\">";
    			$message .= "<strong style=\"margin-right:5px;\">Nombres referidos:</strong>".$nombres_referido;
    			$message .= "</span></p>";
    
    			$message .= "<p align=\"center\" style=\"margin:9px 0px;text-align:left;padding:0 20px;\"><span style=\"font-family:arial;font-size:15px;\">";
    			$message .= "<strong style=\"margin-right:5px;\">Proyecto referido:</strong>".$proyecto_referido;
    			$message .= "</span></p>";
    
    			$message .= "<p align=\"center\" style=\"margin:9px 0px;text-align:left;padding:0 20px;\"><span style=\"font-family:arial;font-size:15px;\">";
    			$message .= "<strong style=\"margin-right:5px;\">DNI referido:</strong>".$dni_referido;
    			$message .= "</span></p>";
    			
    			$message .= "<p align=\"center\" style=\"margin:9px 0px;text-align:left;padding:0 20px;\"><span style=\"font-family:arial;font-size:15px;\">";
    			$message .= "<strong style=\"margin-right:5px;\">Telefono referido:</strong>".$telefono_referido;
    			$message .= "</span></p><br><br>";
    
    			$message .= "<p align=\"center\" style=\"text-align:center;\">";
    			$message .= "</p>";
    
    			$message .= "</div>";
    			$message .= "<p><span>&nbsp;</span></p>";
    			$message .= "</td></tr></tbody>";
    			$message .= "</table></td></tr></tbody>";
    			$message .= "</table></div></td></tr>";
    			$message .= "</tbody>";
    			$message .= "</table></div><br><br>\r\n\r\n</td></tr>";
    			$message .= "</tbody>";
    			$message .= "</table>";
    
    
    			$cabeceras  = 'MIME-Version: 1.0' . "\r\n";
    			$cabeceras .= 'Content-type: text/html; charset=UTF-8' . "\r\n";
    			$cabeceras .= 'From: Agendar Cita WEB-Altozano<'.$correo_propietario.'>' . "\r\n";
    
    			try {
    				mail($para, $título, $message, $cabeceras);
    				redirect(base_url() . 'gracias');
    			} catch (Exception $e) {
    				echo 'Excepción capturada: ',  $e->getMessage(), "\n";
    			}
        	} else {
        	    $this->session->set_flashdata('valida','Debe seleccionar el captcha'); 
        		header("Location:".$_SERVER['HTTP_REFERER']);  
        	}				
		}
	}

?>