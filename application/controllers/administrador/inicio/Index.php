<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Index extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('administrador/empresa_model');
		$this->ruta = './public/frontend/img/';
	}
	
	public function index()
	{
		$this->load->view('administrador/layouts/header');
		$this->load->view('administrador/layouts/aside');
		$this->load->view('administrador/inicio/index');
		$this->load->view('administrador/layouts/footer');
	}

	public function actualizar_datos_inicio()
	{
		$data_reg['textoConfia'] 		= $this->input->post('textoConfia');
		$data_reg['subtituloConfia'] 	= $this->input->post('subtituloConfia');
		$data_reg['tituloConfia'] 	= $this->input->post('tituloConfia');

		$config['upload_path']   = $this->ruta;
        $config['allowed_types'] = 'jpg|png|jpeg';
        $data_temp = $this->empresa_model->getEmpresa(1);
        $this->load->library('upload', $config);

        if ($this->upload->do_upload('imagen')) {
        	# se guardo
        	if (file_exists($this->ruta . $data_temp->imagenConfia)) {
        		unlink($this->ruta . $data_temp->imagenConfia);
        	}
        	$data = array('upload_data' => $this->upload->data());
        	$data_reg['imagenConfia'] = $data['upload_data']['file_name'];
		}

		#icono
		$config_icono['upload_path']   = $this->ruta;
        $config_icono['allowed_types'] = 'jpg|png|jpeg';
        $this->upload->initialize($config_icono);
		if ($this->upload->do_upload('icono')) {
        	# se guardo icono
        	if (file_exists($this->ruta . $data_temp->iconoConfia)) {
        		unlink($this->ruta . $data_temp->iconoConfia);
        	}
        	$data = array('upload_data' => $this->upload->data());
			$data_reg['iconoConfia'] = $data['upload_data']['file_name'];
		}

		try {
			$this->empresa_model->actualizar_empresa($data_reg, '1');
			$response = array(
				'cabecera'	=> 'M_0',
				'mensaje'	=> 'Los datos se actualizaron correctamente.'
			);
			echo json_encode($response);exit();
		} catch (Exception $e) {
			$response = array(
				'cabecera'	=> 'M_1',
				'mensaje'	=> 'Hubo problemas al eliminar' . $e->getMessage()
			);
			echo json_encode($response);exit();
		}
	}

}
