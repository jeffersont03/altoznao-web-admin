<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Index extends CI_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->model('administrador/empresa_model');
		$this->ruta = './public/frontend/img/';
	}

	public function index(){

		$this->load->view('administrador/layouts/header');
		$this->load->view('administrador/layouts/aside');
		$this->load->view('administrador/nosotros/index');
		$this->load->view('administrador/layouts/footer');
	}

	public function empresa_datos()
	{
		$id = $this->input->post("id");
		$data = $this->empresa_model->getEmpresa($id);
		echo json_encode($data);
	}

	public function redes_empresa()
	{
		$data = $this->empresa_model->redes();
		echo json_encode($data);
	}

	public function correos_empresa()
	{
		$data = $this->empresa_model->correos();
		echo json_encode($data);
	}

	public function telefonos_empresas()
	{
		$data = $this->empresa_model->telefonos();
		echo json_encode($data);
	}

	public function eliminar_comun()
	{
		$id = $this->input->post('id');
		$tabla = $this->input->post('tabla');

		try {
			$this->empresa_model->eliminar_comunes($id, $tabla);
			$response = array(
				'cabecera'	=> 'M_0',
				'mensaje'	=> 'El item se elimino correctamente.'
			);
			echo json_encode($response);exit();
		} catch (Exception $e) {
			$response = array(
				'cabecera'	=> 'M_1',
				'mensaje'	=> 'Hubo problemas al eliminar' . $e->getMessage()
			);
			echo json_encode($response);exit();
		}
	}

	public function insertar_correo()
	{
		$correos = $this->input->post('correo');
		$id_empresa = 1;

		$data = array(
			'correos' => $correos,
			'id_empresa' => $id_empresa
		);

		try {
			$this->empresa_model->insertar_comunes($data, 'correos');
			$response = array(
				'cabecera'	=> 'M_0',
				'mensaje'	=> 'Se agrego el correo correctamente.'
			);
			echo json_encode($response);exit();
		} catch (Exception $e) {
			$response = array(
				'cabecera'	=> 'M_1',
				'mensaje'	=> 'Hubo problemas al eliminar' . $e->getMessage()
			);
			echo json_encode($response);exit();
		}
	}

	public function insertar_telefono()
	{
		$telefono = $this->input->post('telefono');
		$id_empresa = 1;

		$data = array(
			'telefono' => $telefono,
			'id_empresa' => $id_empresa
		);

		try {
			$this->empresa_model->insertar_comunes($data, 'telefonos');
			$response = array(
				'cabecera'	=> 'M_0',
				'mensaje'	=> 'Se agrego el telefono correctamente.'
			);
			echo json_encode($response);exit();
		} catch (Exception $e) {
			$response = array(
				'cabecera'	=> 'M_1',
				'mensaje'	=> 'Hubo problemas al eliminar' . $e->getMessage()
			);
			echo json_encode($response);exit();
		}
	}

	public function insertar_redes()
	{
		$redes 		= $this->input->post('redes_temporal');

		$temporal = explode("_", $redes);

		$link 		= $this->input->post('link');
		$icono 		= $temporal[0];
		$red 		= ucwords($temporal[1]);
		$id_empresa = 1;

		$data = array(
			'redes' => $red,
			'link' => $link,
			'icono' => $icono,
			'id_empresa' => $id_empresa
		);

		try {
			$this->empresa_model->insertar_comunes($data, 'redes_sociales');
			$response = array(
				'cabecera'	=> 'M_0',
				'mensaje'	=> 'Se agrego la red social correctamente.'
			);
			echo json_encode($response);exit();
		} catch (Exception $e) {
			$response = array(
				'cabecera'	=> 'M_1',
				'mensaje'	=> 'Hubo problemas al eliminar' . $e->getMessage()
			);
			echo json_encode($response);exit();
		}
	}

	public function actualizar_empresa()
	{
		$data_reg['nombre'] = $this->input->post('nombre');
		$data_reg['nosotros'] = $this->input->post('nosotros');
		$data_reg['vision'] = $this->input->post('vision');
		$data_reg['mision'] = $this->input->post('mision');
		$data_reg['titulo_historia'] = $this->input->post('titulo_historia');
		$data_reg['titulo_valores'] = $this->input->post('titulo_valores');
		$data_reg['titulo_valores_colaboradores'] = $this->input->post('titulo_valores_colaboradores');
		$data_reg['historia'] = $this->input->post('historia');
		$data_reg['valores'] = $this->input->post('valores');
		$data_reg['valores_colaboradores'] = $this->input->post('valores_colaboradores');
		$data_reg['contacto'] = $this->input->post('contacto');
		$data_temp 			 		= $this->empresa_model->getEmpresa(1);



		# imagen banner
		$config_banner['upload_path']   = $this->ruta;
        $config_banner['allowed_types'] = 'jpg|png|jpeg';
        $this->load->library('upload', $config_banner);

        if ($this->upload->do_upload('imagen')) {
        	# se guardo
        	if (file_exists($this->ruta . $data_temp->imagen)) {
        		unlink($this->ruta . $data_temp->imagen);
        	}
        	$data = array('upload_data' => $this->upload->data());
        	$data_reg['imagen'] = $data['upload_data']['file_name'];
		}

		try {
			$this->empresa_model->actualizar_empresa($data_reg, '1');
			$response = array(
				'cabecera'	=> 'M_0',
				'mensaje'	=> 'Los datos de la empresa se actualizaron correctamente.'
			);
			echo json_encode($response);exit();
		} catch (Exception $e) {
			$response = array(
				'cabecera'	=> 'M_1',
				'mensaje'	=> 'Hubo problemas al eliminar' . $e->getMessage()
			);
			echo json_encode($response);exit();
		}

	}

	public function buscar_comun()
	{
		$id = $this->input->post('id');
		$tabla = $this->input->post('editar');

		$data = $this->empresa_model->buscar_comun($id, $tabla);
		echo json_encode($data);
	}

	public function actualizar_correo()
	{
		$id = $this->input->post('id');
		$data['correos'] = $this->input->post('correos');
		$tabla = 'correos';

		try {
			$this->empresa_model->actualizar_comunes($data, $id, $tabla);
			$response = array(
				'cabecera'	=> 'M_0',
				'mensaje'	=> 'El correo se actualizo correctamente.'
			);
			echo json_encode($response);exit();
		} catch (Exception $e) {
			$response = array(
				'cabecera'	=> 'M_1',
				'mensaje'	=> 'Hubo problemas al eliminar' . $e->getMessage()
			);
			echo json_encode($response);exit();
		}
	}

	public function actualizar_telefonos()
	{
		$id = $this->input->post('id');
		$data['telefono'] = $this->input->post('telefonos');
		$tabla = 'telefonos';

		try {
			$this->empresa_model->actualizar_comunes($data, $id, $tabla);
			$response = array(
				'cabecera'	=> 'M_0',
				'mensaje'	=> 'El telefono se actualizo correctamente.'
			);
			echo json_encode($response);exit();
		} catch (Exception $e) {
			$response = array(
				'cabecera'	=> 'M_1',
				'mensaje'	=> 'Hubo problemas al eliminar' . $e->getMessage()
			);
			echo json_encode($response);exit();
		}
	}

	public function actualizar_redsocial()
	{
		// $redes 		= $this->input->post('redes_temporal');

		// $temporal = explode("_", $redes);

		$id 				= $this->input->post('id');
		$data['link'] 		= $this->input->post('link');
		// $data['icono'] 		= $temporal[0];
		// $data['redes'] 		= ucwords($temporal[1]);
		$tabla 				= 'redes_sociales';

		try {
			$this->empresa_model->actualizar_comunes($data, $id, $tabla);
			$response = array(
				'cabecera'	=> 'M_0',
				'mensaje'	=> 'La red social se actualizo correctamente.'
			);
			echo json_encode($response);exit();
		} catch (Exception $e) {
			$response = array(
				'cabecera'	=> 'M_1',
				'mensaje'	=> 'Hubo problemas al eliminar' . $e->getMessage()
			);
			echo json_encode($response);exit();
		}
	}

}

/* End of file Index.php */
/* Location: ./application/controllers/administrador/nosotros/Index.php */