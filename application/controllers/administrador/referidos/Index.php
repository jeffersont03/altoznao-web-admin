<?php 

defined('BASEPATH') OR exit('No direct script access allowed');

class Index extends CI_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->model('administrador/empresa_model');
		$this->ruta = './public/frontend/img/';
	}

	public function index(){

		$this->load->view('administrador/layouts/header');
		$this->load->view('administrador/layouts/aside');
		$this->load->view('administrador/referidos/index');
		$this->load->view('administrador/layouts/footer');
	}

	public function listar()
	{
		$data = $this->empresa_model->buscar_comun(1, 'referidos');
		echo json_encode($data);
	}

	public function actualizar()
	{
		$data_reg['titulo'] = $this->input->post('titulo');
		$data_reg['descripcion'] = $this->input->post('descripcion');
		$data_reg['item1'] = $this->input->post('item1');
		$data_reg['item2'] = $this->input->post('item2');
		$data_reg['item3'] = $this->input->post('item3');
		$data_reg['correo_referido'] = $this->input->post('correo_referido');
		$data_temp 			 		= $this->empresa_model->buscar_comun(1, 'referidos');

		# imagen 1
		$config_img_uno['upload_path']   = $this->ruta;
        $config_img_uno['allowed_types'] = 'jpg|png|jpeg';
		$this->load->library('upload', $config_img_uno);
		
		if ($this->upload->do_upload('img1')) {
        	# se guardo
        	if (file_exists($this->ruta . $data_temp->img1)) {
        		unlink($this->ruta . $data_temp->img1);
        	}
        	$data = array('upload_data' => $this->upload->data());
        	$data_reg['img1'] = $data['upload_data']['file_name'];
		}

		# imagen 2
		$config_img_dos['upload_path']   = $this->ruta;
        $config_img_dos['allowed_types'] = 'jpg|png|jpeg';
		$this->load->library('upload', $config_img_dos);
		
		if ($this->upload->do_upload('img2')) {
        	# se guardo
        	if (file_exists($this->ruta . $data_temp->img2)) {
        		unlink($this->ruta . $data_temp->img2);
        	}
        	$data = array('upload_data' => $this->upload->data());
        	$data_reg['img2'] = $data['upload_data']['file_name'];
		}

		# imagen 3
		$config_img_tres['upload_path']   = $this->ruta;
        $config_img_tres['allowed_types'] = 'jpg|png|jpeg';
		$this->load->library('upload', $config_img_tres);
		
		if ($this->upload->do_upload('img3')) {
        	# se guardo
        	if (file_exists($this->ruta . $data_temp->img3)) {
        		unlink($this->ruta . $data_temp->img3);
        	}
        	$data = array('upload_data' => $this->upload->data());
        	$data_reg['img3'] = $data['upload_data']['file_name'];
		}

		# card
		$config_img_card['upload_path']   = $this->ruta;
        $config_img_card['allowed_types'] = 'jpg|png|jpeg';
		$this->load->library('upload', $config_img_card);
		
		if ($this->upload->do_upload('card')) {
        	# se guardo
        	if (file_exists($this->ruta . $data_temp->card)) {
        		unlink($this->ruta . $data_temp->card);
        	}
        	$data = array('upload_data' => $this->upload->data());
        	$data_reg['card'] = $data['upload_data']['file_name'];
		}

		try {
			$this->empresa_model->actualizar_comunes($data_reg, '1', 'referidos');
			$response = array(
				'cabecera'	=> 'M_0',
				'mensaje'	=> 'Los datos se actualizaron correctamente.'
			);
			echo json_encode($response);exit();
		} catch (Exception $e) {
			$response = array(
				'cabecera'	=> 'M_1',
				'mensaje'	=> 'Hubo problemas al eliminar' . $e->getMessage()
			);
			echo json_encode($response);exit();
		}
	}
}
 ?>