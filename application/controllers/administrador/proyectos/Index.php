<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Index extends CI_Controller {

	private $ruta;

	public function __construct(){
		parent::__construct();
		$this->load->model('administrador/proyectos_model');
		$this->ruta = './public/frontend/img/proyecto/';

		date_default_timezone_set('America/Lima');
	}

	public function index()
	{
		$this->load->view('administrador/layouts/header');
		$this->load->view('administrador/layouts/aside');
		$this->load->view('administrador/proyectos/index');
		$this->load->view('administrador/layouts/footer');
	}

	public function lista_proyectos()
	{
		$data = $this->proyectos_model->proyecto();
		echo json_encode($data);
		
	}

	public function proyecto()
	{
		$id_proyecto = $this->input->post('id_proyecto');
		$data = $this->proyectos_model->buscar_proyecto($id_proyecto);
		echo json_encode($data);
	}

	/*public function prueba()
	{
		$nombre = 'Saray Último';
		$nombre = $this->sanear_string($nombre);
		$nombre = strtolower($nombre);

		# controlador
		$nombre_controlador = str_replace(" ", "", $nombre);
		$nombre_clase_controlador = ucfirst($nombre_controlador);
		$nombre_controlador = ucfirst($nombre_controlador) . '.php';

		# vista
		$nombre_vista = str_replace(" ", "", $nombre);
		$nombre_vista = strtolower($nombre_vista) . '.php';

		#ruta
		$nombre_ruta = str_replace(" ", "-", $nombre);

		$texto_controlador = '<?php
			defined("BASEPATH") OR exit("No direct script access allowed");
			class ' . $nombre_clase_controlador . ' extends CI_Controller {
				public function index(){
				$this->load->view("frontend/layout/header");
				$this->load->view("frontend/' . $nombre_vista . '");
				$this->load->view("frontend/layout/footer");
			}

		}';

		if ($controlador = fopen('./application/controllers/frontend/' . $nombre_controlador, "a")) {
			if (fwrite($controlador, $texto_controlador)) {
				echo 'controlador correcto';

				# vista
				if ($vista = fopen('./application/views/frontend/' . $nombre_vista, "a")) {

					$cuerpo_html = '0'

					if (fwrite($vista, '<h1>Hola</h1>')) {
						echo 'vista correcto';
						
						if($archivo_ruta = fopen('./application/config/routes.php', 'a+')){
							
							if (fwrite($archivo_ruta, '\n$route["' . $nombre_ruta . '"] = "frontend/' . strtolower($nombre_clase_controlador) . '";')) {
								echo 'ruta correcto, abrir: ' . base_url() . $nombre_ruta; 

							} else {
								echo 'ruta incorrecto';
							}

							fclose($archivo_ruta);
						}

					} else {
						echo ' vistaincorrecto';
					}
					fclose($vista);
				}
			} else {
				echo ' controlador incorrecto';
			}
			fclose($controlador);
		}

	}*/

	public function sanear_string($string)
	{
	 
	    $string = trim($string);
	 
	    $string = str_replace(
	        array('á', 'à', 'ä', 'â', 'ª', 'Á', 'À', 'Â', 'Ä'),
	        array('a', 'a', 'a', 'a', 'a', 'A', 'A', 'A', 'A'),
	        $string
	    );
	 
	    $string = str_replace(
	        array('é', 'è', 'ë', 'ê', 'É', 'È', 'Ê', 'Ë'),
	        array('e', 'e', 'e', 'e', 'E', 'E', 'E', 'E'),
	        $string
	    );
	 
	    $string = str_replace(
	        array('í', 'ì', 'ï', 'î', 'Í', 'Ì', 'Ï', 'Î'),
	        array('i', 'i', 'i', 'i', 'I', 'I', 'I', 'I'),
	        $string
	    );
	 
	    $string = str_replace(
	        array('ó', 'ò', 'ö', 'ô', 'Ó', 'Ò', 'Ö', 'Ô'),
	        array('o', 'o', 'o', 'o', 'O', 'O', 'O', 'O'),
	        $string
	    );
	 
	    $string = str_replace(
	        array('ú', 'ù', 'ü', 'û', 'Ú', 'Ù', 'Û', 'Ü'),
	        array('u', 'u', 'u', 'u', 'U', 'U', 'U', 'U'),
	        $string
	    );
	 
	    $string = str_replace(
	        array('ñ', 'Ñ', 'ç', 'Ç'),
	        array('n', 'N', 'c', 'C',),
	        $string
	    );
	 
	    //Esta parte se encarga de eliminar cualquier caracter extraño
	   /* $string = str_replace(
	        array("\", "¨", "º", "-", "~",
	             "#", "@", "|", "!", """,
	             "·", "$", "%", "&", "/",
	             "(", ")", "?", "'", "¡",
	             "¿", "[", "^", "<code>", "]",
	             "+", "}", "{", "¨", "´",
	             ">", "< ", ";", ",", ":",
	             ".", " "),
	        '',
	        $string
	    );*/
	 
	 
	    return $string;
	}

	public function nuevo_proyecto() {
		$nombre 		= $this->input->post('nombre');
		$estado 		= $this->input->post('estado');
		#$descripcion 	= $this->input->post('descripcion');
		$descripcion    = $_POST['descripcion'];
		$etapas 		= $this->input->post('etapas');
		$tipo_proyecto 	= $this->input->post('tipo_proyecto');
		$departamento 	= $this->input->post('departamento');
		$correo 		= $this->input->post('correo');
		$comentarios 	= $this->input->post('comentarios');
		$item1			= $this->input->post('item1');
		$item2			= $this->input->post('item2');
		$item3			= $this->input->post('item3');
		
		# imagen banner
		$config_banner['upload_path']   = $this->ruta . 'banners/';
        $config_banner['allowed_types'] = 'jpg|png|jpeg';
        $this->load->library('upload', $config_banner);

        if ($this->upload->do_upload('imagen_principal')) {
        	# se guardo
        	$data = array('upload_data' => $this->upload->data());
        	$imagen_principal = 'banners/' . $data['upload_data']['file_name'];

        	# logo
        	$config_logo['upload_path']   = $this->ruta . 'logos/';
	        $config_logo['allowed_types'] = 'jpg|png|jpeg';
	        $this->upload->initialize($config_logo);

	        if ($this->upload->do_upload('logo')) {
	        	# se guardo logo
	        	$data = array('upload_data' => $this->upload->data());
        		$imagen_logo = 'logos/' . $data['upload_data']['file_name'];

        		#fachada
        		$config_fachada['upload_path']   = $this->ruta . 'fachadas/';
		        $config_fachada['allowed_types'] = 'jpg|png|jpeg';
		        $this->upload->initialize($config_fachada);

		        if ($this->upload->do_upload('imagen_fachada')) {
		        	# se guardo fachada
		        	$data = array('upload_data' => $this->upload->data());
        			$imagen_fachada = 'fachadas/' . $data['upload_data']['file_name'];

        			$nombre = $this->sanear_string($nombre);
					$nombre = strtolower($nombre);
					$nombre_ruta = str_replace(" ", "-", $nombre);

			        $data = array(
				        'nombre' 			=> $nombre,
						'estado' 			=> $estado,
						'descripcion' 		=> $descripcion,
						'etapas' 			=> $etapas,
						'tipo_proyecto' 	=> $tipo_proyecto,
						'departamento' 		=> $departamento,
						'correo' 			=> $correo,
						'comentarios' 		=> $comentarios,
						'imagen_principal' 	=> $imagen_principal,
						'logo' 				=> $imagen_logo,
						'imagen_fachada' 	=> $imagen_fachada,
						'visual'			=> 1,
						'url'				=> $nombre_ruta,
						'item1'				=> $item1,
						'item2'				=> $item2,
						'item3'				=> $item3
					);

        			$respuesta = false;
					$respuesta = $this->proyectos_model->insertar_proyecto($data);

					if ($respuesta) {
						$response = array(
							'cabecera'	=> 'M_0',
							'mensaje'	=> 'El proyecto se registro correctamente',
							'id'		=> $respuesta
						);
						echo json_encode($response);exit();
					} else {
						$response = array(
							'cabecera'	=> 'M_1',
							'mensaje'	=> 'Hubo problemas al registrar, intentelo nuevamente.'
						);
						echo json_encode($response);exit();
					}
			        } else {
		        	$response = array(
						'cabecera'	=> 'M_1',
						'mensaje'	=> 'No se pudo subir la imagen de la fachada.'
					);
					echo json_encode($response);exit();
		        }

	        } else {
	        	$response = array(
					'cabecera'	=> 'M_1',
					'mensaje'	=> 'No se pudo subir la imagen del logo.'
				);
				echo json_encode($response);exit();
	        }

        } else {
        	$response = array(
				'cabecera'	=> 'M_1',
				'mensaje'	=> 'No se pudo subir la imagen principal.'
			);
			echo json_encode($response);exit();
        }       
	}

	public function galerias() {
		$id_proyecto = $this->input->post('id_proyecto');
		$data = $this->proyectos_model->galerias($id_proyecto);
		echo json_encode($data);
	}

	public function planos() {
		$id_proyecto = $this->input->post('id_proyecto');
		$data = $this->proyectos_model->planos($id_proyecto);
		echo json_encode($data);
	}

	public function nueva_galeria()
	{
		$id_proyecto = $this->input->post('id_proyecto');
		$nombre 	 = $this->input->post('nombre');

		$config_banner['upload_path']   = $this->ruta . 'galerias/';
        $config_banner['allowed_types'] = 'jpg|png|jpeg';
        $this->load->library('upload', $config_banner);

        if ($this->upload->do_upload('imagen')) {
        	# se guardo
        	$data = array('upload_data' => $this->upload->data());
        	$imagen = 'galerias/' . $data['upload_data']['file_name'];

        	$data = array(
		        'titulo' 		=> $nombre,
		        'imagen' 		=> $imagen,
		        'id_proyecto'	=> $id_proyecto
			);

			$respuesta = false;
			$respuesta = $this->proyectos_model->insertar_galeria($data);
			if ($respuesta) {
				$response = array(
					'cabecera'	=> 'M_0',
					'mensaje'	=> 'La galería se registro correctamente',
					'id'		=> $respuesta
				);
				echo json_encode($response);exit();
			} else {
				$response = array(
					'cabecera'	=> 'M_1',
					'mensaje'	=> 'Hubo problemas al registrar, intentelo nuevamente.'
				);
				echo json_encode($response);exit();
			}
        } else {
        	$response = array(
				'cabecera'	=> 'M_1',
				'mensaje'	=> 'No se pudo subir la imagen de la galeria.'
			);
			echo json_encode($response);exit();
        }     
	}

	public function nuevo_plano() {
		$id_proyecto = $this->input->post('id_proyecto');
		$nombre 	 = $this->input->post('nombre');
		$descripcion = $this->input->post('descripcion');

		$config['upload_path']   = $this->ruta . 'planos/';
        $config['allowed_types'] = 'jpg|png|jpeg';
        $this->load->library('upload', $config);

        if ($this->upload->do_upload('imagen')) {
        	# se guardo
        	$data = array('upload_data' => $this->upload->data());
        	$imagen = 'planos/' . $data['upload_data']['file_name'];

        	$data = array(
		        'imagen' 		=> $imagen,
		        'titulo' 		=> $nombre,
		        'descripcion'	=> $descripcion,
		        'id_proyecto'	=> $id_proyecto
			);

			$respuesta = false;
			$respuesta = $this->proyectos_model->insertar_planos($data);
			if ($respuesta) {
				$response = array(
					'cabecera'	=> 'M_0',
					'mensaje'	=> 'La galería se registro correctamente',
					'id'		=> $respuesta
				);
				echo json_encode($response);exit();
			} else {
				$response = array(
					'cabecera'	=> 'M_1',
					'mensaje'	=> 'Hubo problemas al registrar, intentelo nuevamente.'
				);
				echo json_encode($response);exit();
			}
        } else {
        	$response = array(
				'cabecera'	=> 'M_1',
				'mensaje'	=> 'No se pudo subir la imagen de la galeria.'
			);
			echo json_encode($response);exit();
        }     
	}

	public function eliminar_galeria()
	{
		$id = $this->input->post('id_galeria');

		// eliminando imagen del server
		$data_temp = $this->proyectos_model->buscar_galeria($id);
		unlink($this->ruta . $data_temp->imagen);

		try {
			$this->proyectos_model->eliminar_galeria($id);
			$response = array(
				'cabecera'	=> 'M_0',
				'mensaje'	=> 'La galería se elimino correctamente'
			);
			echo json_encode($response);exit();
		} catch (Exception $e) {
			$response = array(
				'cabecera'	=> 'M_1',
				'mensaje'	=> 'Hubo problemas al eliminar' . $e->getMessage()
			);
			echo json_encode($response);exit();
		}
	}

	public function eliminar_plano()
	{
		$id = $this->input->post('id_plano');

		// eliminando imagen del server
		$data_temp = $this->proyectos_model->buscar_plano($id);
		unlink($this->ruta . $data_temp->imagen);

		try {
			$this->proyectos_model->eliminar_plano($id);
			$response = array(
				'cabecera'	=> 'M_0',
				'mensaje'	=> 'El plano se elimino correctamente'
			);
			echo json_encode($response);exit();
		} catch (Exception $e) {
			$response = array(
				'cabecera'	=> 'M_1',
				'mensaje'	=> 'Hubo problemas al eliminar' . $e->getMessage()
			);
			echo json_encode($response);exit();
		}
	}

	public function desactivar_proyecto()
	{
		$id_proyecto = $this->input->post('id_proyecto');

		$data_temp = $this->proyectos_model->buscar_proyecto($id_proyecto);

		$visual = ( $data_temp->visual == '1') ? '0' : '1';

		try {
			$data = array(
		        'visual' => $visual
			);

			$this->proyectos_model->actualizar_proyecto($data, $id_proyecto);
			$response = array(
				'cabecera'	=> 'M_0',
				'mensaje'	=> 'El proyecto se desactivo correctamente.'
			);
			echo json_encode($response);exit();
		} catch (Exception $e) {
			$response = array(
				'cabecera'	=> 'M_1',
				'mensaje'	=> 'Hubo problemas al eliminar' . $e->getMessage()
			);
			echo json_encode($response);exit();
		}
	}

	public function listar_galeria()
	{
		$id = $this->input->post('id_galeria');
		$data = $this->proyectos_model->buscar_galeria($id);
		echo json_encode($data);
	}

	public function listar_plano()
	{
		$id = $this->input->post('id_plano');
		$data = $this->proyectos_model->buscar_plano($id);
		echo json_encode($data);
	}

	public function actualizar_galeria()
	{
		$id 	 = $this->input->post('id_galeria');
		$nombre  = $this->input->post('nombre');
		$data 	 = [];

		$config_banner['upload_path']   = $this->ruta . 'galerias/';
        $config_banner['allowed_types'] = 'jpg|png|jpeg';
        $this->load->library('upload', $config_banner);

		# se viene imagen para actualizar
		if ($this->upload->do_upload('imagen')) {
			# eliminando imagen
			$data_temp = $this->proyectos_model->buscar_galeria($id);
			unlink($this->ruta . $data_temp->imagen);

			$data = array('upload_data' => $this->upload->data());
        	$imagen = 'galerias/' . $data['upload_data']['file_name'];

        	$data = array(
		        'titulo' 		=> $nombre,
		        'imagen' 		=> $imagen
			);
		}

		# si solo se actualiza el texto
		else {
			$data = array(
				'titulo' => $nombre
			);
		}

		try {
			$this->proyectos_model->actualizar_galeria($data, $id);
			$response = array(
				'cabecera'	=> 'M_0',
				'mensaje'	=> 'La galería se actualizo correctamente.'
			);
			echo json_encode($response);exit();
		} catch (Exception $e) {
			$response = array(
				'cabecera'	=> 'M_1',
				'mensaje'	=> 'Hubo problemas al eliminar' . $e->getMessage()
			);
			echo json_encode($response);exit();
		}

	}

	public function actualizar_plano()
	{
		$id 	 		= $this->input->post('id_plano');
		$nombre  		= $this->input->post('nombre');
		$descripcion  	= $this->input->post('descripcion');
		$data 	 = [];

		$config_banner['upload_path']   = $this->ruta . 'planos/';
        $config_banner['allowed_types'] = 'jpg|png|jpeg';
        $this->load->library('upload', $config_banner);

		# se viene imagen para actualizar
		if ($this->upload->do_upload('imagen')) {
			# eliminando imagen
			$data_temp = $this->proyectos_model->buscar_plano($id);
			unlink($this->ruta . $data_temp->imagen);

			$data = array('upload_data' => $this->upload->data());
        	$imagen = 'planos/' . $data['upload_data']['file_name'];

        	$data = array(
		        'titulo' 		=> $nombre,
		        'imagen' 		=> $imagen,
		        'descripcion' 	=> $descripcion
			);
		}

		# si solo se actualiza el texto
		else {
			$data = array(
				'titulo' 		=> $nombre,
		        'descripcion'	=> $descripcion
			);
		}

		try {
			$this->proyectos_model->actualizar_plano($data, $id);
			$response = array(
				'cabecera'	=> 'M_0',
				'mensaje'	=> 'El plano se actualizo correctamente.'
			);
			echo json_encode($response);exit();
		} catch (Exception $e) {
			$response = array(
				'cabecera'	=> 'M_1',
				'mensaje'	=> 'Hubo problemas al eliminar' . $e->getMessage()
			);
			echo json_encode($response);exit();
		}

	}

	public function actualizar_proyecto()
	{
		$data_actualizar['id'] 				= $this->input->post('id');
		$data_actualizar['nombre'] 			= $this->input->post('nombre');
		$data_actualizar['estado'] 			= $this->input->post('estado');
		$data_actualizar['tipo_proyecto'] 	= $this->input->post('tipo_proyecto');
		$data_actualizar['departamento'] 	= $this->input->post('departamento');
		$data_actualizar['correo'] 			= $this->input->post('correo');
		$data_actualizar['etapas'] 			= $this->input->post('etapas');
		#$data_actualizar['descripcion'] 	= $this->input->post('descripcion');
		$data_actualizar['descripcion'] 	= $_POST['descripcion'];
		$data_actualizar['comentarios'] 	= $this->input->post('comentarios');
		$data_actualizar['item1']			= $this->input->post('item1');
		$data_actualizar['item2']			= $this->input->post('item2');
		$data_actualizar['item3']			= $this->input->post('item3');

		$data_temp = $this->proyectos_model->buscar_proyecto($this->input->post('id'));

		# imagen banner
		$config_banner['upload_path']   = $this->ruta . 'banners/';
        $config_banner['allowed_types'] = 'jpg|png|jpeg';
        $this->load->library('upload', $config_banner);

        if ($this->upload->do_upload('imagen_principal')) {
        	# se guardo
        	if (file_exists($this->ruta . $data_temp->imagen_principal)) {
        		unlink($this->ruta . $data_temp->imagen_principal);
        	}
        	$data = array('upload_data' => $this->upload->data());
        	$data_actualizar['imagen_principal'] = 'banners/' . $data['upload_data']['file_name'];
		}

    	# logo
    	$config_logo['upload_path']   = $this->ruta . 'logos/';
        $config_logo['allowed_types'] = 'jpg|png|jpeg';
        $this->upload->initialize($config_logo);

		if ($this->upload->do_upload('logo')) {
        	# se guardo logo
        	if (file_exists($this->ruta . $data_temp->logo)) {
        		unlink($this->ruta . $data_temp->logo);
        	}
        	$data = array('upload_data' => $this->upload->data());
    		$data_actualizar['logo'] = 'logos/' . $data['upload_data']['file_name'];
		}

		#fachada
		$config_fachada['upload_path']   = $this->ruta . 'fachadas/';
        $config_fachada['allowed_types'] = 'jpg|png|jpeg';
        $this->upload->initialize($config_fachada);
		if ($this->upload->do_upload('imagen_fachada')) {
        	# se guardo fachada
        	if (file_exists($this->ruta . $data_temp->imagen_fachada)) {
        		unlink($this->ruta . $data_temp->imagen_fachada);
        	}
        	$data = array('upload_data' => $this->upload->data());
			$data_actualizar['imagen_fachada'] = 'fachadas/' . $data['upload_data']['file_name'];
		}

		# ejecutando la actualización
		try {
			$this->proyectos_model->actualizar_proyecto($data_actualizar, $this->input->post('id'));
			$response = array(
				'cabecera'	=> 'M_0',
				'mensaje'	=> 'El proyecto se actualizo correctamente.'
			);
			echo json_encode($response);exit();
		} catch (Exception $e) {
			$response = array(
				'cabecera'	=> 'M_1',
				'mensaje'	=> 'Hubo problemas al eliminar' . $e->getMessage()
			);
			echo json_encode($response);exit();
		}
	}
}

?>