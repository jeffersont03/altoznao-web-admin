<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class inicio extends CI_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->model('frontend/proyectos_model');
		$this->load->model('frontend/empresa_model');
	}

	public function index(){ 
		$data_header = array(
			'proyectos'	=> $this->proyectos_model->proyectos(),
			'telefonos' => $this->empresa_model->telefonos(),
			'correos' => $this->empresa_model->correos(),
		);

		$data = array(
			'sliders' => $this->proyectos_model->slider(),
			'correos' => $this->empresa_model->correos(),	
			'redes' => $this->empresa_model->redes(),	
			'telefonos' => $this->empresa_model->telefonos(),
			'empresa' => $this->empresa_model->getEmpresa(1),	
		);

		$this->load->view('frontend/layout/header', $data_header);
		$this->load->view('frontend/index', $data);
		$this->load->view('frontend/layout/footer');
	}
}