<?php
	defined('BASEPATH') OR exit('No direct script access allowed');

	class Slider_model extends CI_Model {

		public function listar()
		{
			$resultados = $this->db->get('sliders');
			return $resultados->result();
		}

		public function buscar($id)
		{
			$this->db->where('sliders.id', $id);
			$resultados = $this->db->get('sliders');
			return $resultados->row();
		}

		public function nuevo($data)
		{
			$this->db->insert('sliders', $data);
			return $this->db->insert_id();
		}

		public function actualizar($data, $id) {
			$this->db->where('id', $id);
			$this->db->update('sliders', $data);
		}

		public function eliminar($id) {
			$this->db->where('id', $id);
			$this->db->delete('sliders');
		}

	}
?>