<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Empresa_model extends CI_Model {
	public function getEmpresa($id)
	{
		$resultados = $this->db->get('empresa');
		return $resultados->row();
	}

	public function redes() {
		$resultados = $this->db->get('redes_sociales');
		return $resultados->result();
	}

	public function correos() {
		$resultados = $this->db->get('correos');
		return $resultados->result();
	}

	public function telefonos() {
		$resultados = $this->db->get('telefonos');
		return $resultados->result();
	}

	public function actualizar_empresa($data, $id) {
		$this->db->where('id', $id);
		$this->db->update('empresa', $data);
	}

	public function eliminar_comunes($id, $tabla) {
		$this->db->where('id', $id);
		$this->db->delete($tabla);
	}

	public function actualizar_comunes($data, $id, $tabla) {
		$this->db->where('id', $id);
		$this->db->update($tabla, $data);
	}

	public function insertar_comunes($data, $tabla)
	{
		$this->db->insert($tabla, $data);
		return $this->db->insert_id();
	}

	public function buscar_comun($id, $tabla)
	{
		$resultados = $this->db->get_where($tabla, array('id' => $id));
		return $resultados->row();
	}
}