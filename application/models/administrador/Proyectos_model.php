<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Proyectos_model extends CI_Model {

	public function proyecto() {
		$resultados = $this->db->order_by('id DESC')->get("proyecto");
		return $resultados->result();
	}

	public function insertar_proyecto($data)
	{
		$this->db->insert('proyecto', $data);
		return $this->db->insert_id();
	}

	public function insertar_galeria($data)
	{
		$this->db->insert('galeria_proyecto', $data);
		return $this->db->insert_id();
	}

	public function insertar_planos($data)
	{
		$this->db->insert('planos_proyecto', $data);
		return $this->db->insert_id();
	}

	public function buscar_proyecto($id){
		$array = array('id' => $id);
		$this->db->where($array);
		$resultados = $this->db->get("proyecto");
		return $resultados->row();
	}

	public function galerias($id){
		$resultados = $this->db->get_where('galeria_proyecto', array('id_proyecto' => $id));
		return $resultados->result();
	}

	public function planos($id){
		$resultados = $this->db->get_where('planos_proyecto', array('id_proyecto' => $id));
		return $resultados->result();
	}

	public function eliminar_galeria($id) {
		$this->db->where('id', $id);
		$this->db->delete('galeria_proyecto');
	}

	public function eliminar_plano($id) {
		$this->db->where('id', $id);
		$this->db->delete('planos_proyecto');
	}

	public function actualizar_proyecto($data, $id) {
		$this->db->where('id', $id);
		$this->db->update('proyecto', $data);
	}

	public function actualizar_galeria($data, $id) {
		$this->db->where('id', $id);
		$this->db->update('galeria_proyecto', $data);
	}

	public function actualizar_plano($data, $id) {
		$this->db->where('id', $id);
		$this->db->update('planos_proyecto', $data);
	}

	public function buscar_galeria($id){
		$resultados = $this->db->get_where('galeria_proyecto', array('id' => $id));
		return $resultados->row();
	}

	public function buscar_plano($id){
		$resultados = $this->db->get_where('planos_proyecto', array('id' => $id));
		return $resultados->row();
	}
}

?>