<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Politicas_model extends CI_Model {
	public function getPoliticas($id = null)
	{
		if(isset($id)) $this->db->where('id', $id);
		$resultados = $this->db->get('politicas');
		if(isset($id)) return $resultados->row();
		return $resultados->result();
	}

	public function getPdfs($id = null)
	{
		if(isset($id)) $this->db->where('id', $id);
		$this->db->like('titulo', 'PDF:', 'both');
		$resultados = $this->db->get('politicas');
		if(isset($id)) return $resultados->row();
		return $resultados->result();
	}

	public function actualizar_politicas($data, $id) {
		$this->db->where('id', $id);
		return $this->db->update('politicas', $data);
	}

	public function guardar_politicas($data) {
		return $this->db->insert('politicas', $data);
	}
}