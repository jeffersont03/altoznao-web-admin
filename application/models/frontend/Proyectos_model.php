<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Proyectos_model extends CI_Model {

	public function buscar_proyecto($slug){
		$array = array('visual' => 1, 'url' => $slug);
		$this->db->where($array);
		$resultados = $this->db->get("proyecto");
		return $resultados->row();
	}

	public function galerias($id){
		$resultados = $this->db->get_where('galeria_proyecto', array('id_proyecto' => $id));
		return $resultados->result();
	}

	public function planos($id){
		$resultados = $this->db->get_where('planos_proyecto', array('id_proyecto' => $id));
		return $resultados->result();
	}

	public function proyectos() {
		$this->db->where('visual', 1);
		$this->db->order_by('id DESC');
		$resultados = $this->db->get("proyecto");
		return $resultados->result();
	}

	public function proyectos_tipo($tipo_proyecto) {
		$this->db->where('visual', 1);
		$this->db->where('tipo_proyecto', $tipo_proyecto);
		$this->db->order_by('id DESC');
		$resultados = $this->db->get("proyecto");
		return $resultados->result();
	}

	public function slider() {
		$resultados = $this->db->get('sliders');
		return $resultados->result();
	}
}