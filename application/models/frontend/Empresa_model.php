<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Empresa_model extends CI_Model {
	public function getEmpresa($id)
	{
		$resultados = $this->db->get('empresa');
		return $resultados->row();
	}

	public function redes() {
		$resultados = $this->db->get('redes_sociales');
		return $resultados->result();
	}

	public function correos() {
		$resultados = $this->db->get('correos');
		return $resultados->result();
	}

	public function telefonos() {
		$resultados = $this->db->get('telefonos');
		return $resultados->result();
	}

	public function referidos() {
		$resultados = $this->db->get('referidos');
		return $resultados->row();
	}
}