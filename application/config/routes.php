<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'inicio';
$route['nosotros'] = 'frontend/nosotros';
$route['contacto'] = 'frontend/contacto';
$route['proyectos'] = 'frontend/proyectos';
$route['proyectos-en-venta'] = 'frontend/proyectos/venta';
$route['proximos-proyectos'] = 'frontend/proyectos/proximos';
$route['proyectos-entregados'] = 'frontend/proyectos/entregados';
$route['referidos'] = 'frontend/referidos';
$route['administrador'] = 'administrador/auth';
$route['admin'] = 'administrador/auth';
$route['admin/dashboard'] = 'administrador/proyectos/index';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;

# datos de empresa
$route['admin/empresa/datos'] = 'administrador/nosotros/index/empresa_datos';
$route['admin/empresa/redes'] = 'administrador/nosotros/index/redes_empresa';
$route['admin/empresa/correos'] = 'administrador/nosotros/index/correos_empresa';
$route['admin/empresa/telefonos'] = 'administrador/nosotros/index/telefonos_empresas';
$route['admin/empresa/actualizar'] = 'administrador/nosotros/index/actualizar_empresa';
$route['admin/empresa/comuneliminar'] = 'administrador/nosotros/index/eliminar_comun';
$route['admin/empresa/comunbuscar'] = 'administrador/nosotros/index/buscar_comun';
$route['admin/correos/nuevos'] = 'administrador/nosotros/index/insertar_correo';
$route['admin/telefonos/nuevos'] = 'administrador/nosotros/index/insertar_telefono';
$route['admin/redes/nuevos'] = 'administrador/nosotros/index/insertar_redes';
$route['admin/correos/editar'] = 'administrador/nosotros/index/actualizar_correo';
$route['admin/telefonos/editar'] = 'administrador/nosotros/index/actualizar_telefonos';
$route['admin/redes/editar'] = 'administrador/nosotros/index/actualizar_redsocial';

# proyectos
$route['admin/proyectos/lista'] = 'administrador/proyectos/index/lista_proyectos';
$route['admin/proyectos/nuevo'] = 'administrador/proyectos/index/nuevo_proyecto';
$route['admin/proyectos/buscar'] = 'administrador/proyectos/index/proyecto';
$route['admin/proyectos/actualizar'] = 'administrador/proyectos/index/actualizar_proyecto';
$route['admin/proyectos/desactivar'] = 'administrador/proyectos/index/desactivar_proyecto';

# galerias y planos
$route['admin/galerias/listar'] = 'administrador/proyectos/index/galerias';
$route['admin/planos/listar'] 	= 'administrador/proyectos/index/planos';
$route['admin/galeria/nueva']	= 'administrador/proyectos/index/nueva_galeria';
$route['admin/planos/nuevo']	= 'administrador/proyectos/index/nuevo_plano';
$route['admin/galeria/eliminar']	= 'administrador/proyectos/index/eliminar_galeria';
$route['admin/planos/eliminar']	= 'administrador/proyectos/index/eliminar_plano';
$route['admin/galeria/listar']	= 'administrador/proyectos/index/listar_galeria';
$route['admin/plano/listar']	= 'administrador/proyectos/index/listar_plano';
$route['admin/galeria/actualizar']	= 'administrador/proyectos/index/actualizar_galeria';
$route['admin/plano/actualizar']	= 'administrador/proyectos/index/actualizar_plano';

# slider
$route['admin/banners'] = 'administrador/sliders/index';
$route['admin/banners/buscar'] = 'administrador/sliders/index/buscar';
$route['admin/banners/listar'] = 'administrador/sliders/index/listar';
$route['admin/banners/nuevo'] = 'administrador/sliders/index/nuevo';
$route['admin/banners/eliminar'] = 'administrador/sliders/index/eliminar';
$route['admin/banners/editar'] = 'administrador/sliders/index/editar';

# inicio 
$route['admin/inicio'] = 'administrador/inicio/index';
$route['admin/inicio/actualizar'] = 'administrador/inicio/index/actualizar_datos_inicio';

# Políticas
$route['admin/politicas/enlace-1'] = 'administrador/politicas/index/navegacion';
$route['admin/politicas/enlace-2'] = 'administrador/politicas/index/privacidad';
$route['admin/politicas/enlace-3'] = 'administrador/politicas/index/procedimiento_arcos';
$route['admin/politicas/enlace-4'] = 'administrador/politicas/index/formulario_arcos';
$route['admin/politicas/enlace-5'] = 'administrador/politicas/index/proteccion';
$route['admin/politicas/subir-pdf'] = 'administrador/politicas/index/subir_pdf';
$route['admin/politicas/datos'] = 'administrador/politicas/index/politicas_datos';
$route['admin/politicas/actualizar'] = 'administrador/politicas/index/actualizar_politicas';
$route['admin/politicas/subir_documento'] = 'administrador/politicas/index/subir_documento';

# Referidos
$route['admin/referidos'] = 'administrador/referidos/index';
$route['admin/referidos/listar'] = 'administrador/referidos/index/listar';
$route['admin/referidos/actualizar'] = 'administrador/referidos/index/actualizar';

# frontend
$route['proyectos/(:any)'] = 'frontend/proyectos/render/$1';

#gracias
$route['gracias'] = 'frontend/gracias';

#politicas
$route['terminos'] = 'frontend/politicas/navegacion';
$route['terminos/politicas-navegacion'] = 'frontend/politicas/navegacion';
$route['terminos/politicas-privacidad'] = 'frontend/politicas/privacidad';
$route['terminos/proteccion-consumidor'] = 'frontend/politicas/proteccion';