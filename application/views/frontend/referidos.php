<section class="section section-contacto capa-bg" style="background-image: url(<?= base_url(); ?>public/frontend/img/referidos1.jpg);padding-top: 200px;">
  <div class="container-fluid">
    <div class="container1 relative wow fadeInUp">
      <div class="title center-align">
        <h3>Programas referidos</h3>
        <img src="<?= base_url(); ?>public/frontend/img/line.png">

      </div>
      <div class="center-align">
                        <br>
        <?= $referidos->titulo; ?>
        <br>
        <img src="<?= base_url(); ?>public/frontend/img/<?= $referidos->card; ?>">
      </div>
      <div class="center-align">

        <div class="row">

          <div class="col s12 m12 l4">
            <div class="card-referidos">
              <span class="">1</span>
              <p><?= $referidos->item1; ?></p>
              <img src="<?= base_url(); ?>public/frontend/img/<?= $referidos->img1; ?>">
            </div>
          </div>

          <div class="col s12 m12 l4">
            <div class="card-referidos">
              <span class="">2</span>
              <p><?= $referidos->item2; ?></p>
              <img src="<?= base_url(); ?>public/frontend/img/<?= $referidos->img2; ?>">
            </div>
          </div>

          <div class="col s12 m12 l4">
            <div class="card-referidos">
              <span class="">3</span>
              <p><?= $referidos->item3; ?></p>
              <img src="<?= base_url(); ?>public/frontend/img/<?= $referidos->img3; ?>">
            </div>
          </div>

          <div class="col s12" style="text-align: justify;">
            <hr>
            <br>

            <?= $referidos->descripcion; ?>
            <br><br>
          </div>
          
          <div class="col s12 formulario-referidos">
            <h3><strong>Formulario de referidos</strong></h3>
            <form action="<?= base_url() . 'administrador/correo/referidos'; ?>" method="post">
              <div class="row">
                <div class="col s12 m12 l6 left-align">
                  <h5><b style="color: #00934d;">Datos del propietario</b></h5>
                  <div class="input-field">
                    <label>Nombre y apellido</label>
                    <input type="text" class="" name="nombres_propietario" required>
                  </div>
                  <div class="row no-margin">
                    <div class="input-field col s12 m12 l6">
                      <label>DNI</label>
                      <input type="text" class="" name="dni_propietario" required>
                    </div>
                    <div class="input-field col s12 m12 l6">
                      <label>Teléfono</label>
                      <input type="text" class="" name="telefono_propietario" required>
                    </div>
                  </div>
                  <div class="input-field">
                    <label>Proyecto en el que compró</label>
                    <select name="proyecto_propietario" required>
                      <option value="" disabled selected>--</option>
                      <?php foreach ($proyectos as $proyecto): ?>
                        <option value="<?= $proyecto->nombre; ?>"><?= $proyecto->nombre; ?></option>
                      <?php endforeach ?>
                    </select>
                  </div>
                  <div class="row no-margin">
                    <div class="input-field col s12 m12 l6">
                      <label>Torre y n° de departamento</label>
                      <input type="text" class="" name="torre_propietario" required>
                    </div>
                    <div class="input-field col s12 m12 l6">
                      <label>Correo</label>
                      <input type="text" class="" name="correo_propietario" required>
                    </div>
                  </div>
                </div>
                <div class="col s12 m12 l6 left-align">
                  <h5><b style="color: #00934d;">Datos del referido</b></h5>
                  <div class="input-field">
                    <label>Nombre y apellido</label>
                    <input type="text" class="" name="nombres_referido" required>
                  </div>
                  <div class="input-field">
                    <label>Proyecto en el que compró</label>
                    <select name="proyecto_referido" required>
                      <option value="" disabled selected>--</option>
                      <?php foreach ($proyectos as $proyecto): ?>
                        <option value="<?= $proyecto->nombre; ?>"><?= $proyecto->nombre; ?></option>
                      <?php endforeach ?>
                    </select>
                  </div>
                  <div class="input-field">
                    <label>DNI</label>
                    <input type="text" class="" name="dni_referido" required>
                  </div>
                  <div class="input-field">
                    <label>Teléfono</label>
                    <input type="text" class="" name="telefono_referido" required>
                  </div>
                </div>
                
                <div class="col s12">
                  <div class="checkbox center-align">
                    <p>
                      <label>
                        <input type="checkbox" class="filled-in" checked="checked" required/>
                        <span>He leído y acepto la <a href="<?= base_url(); ?>terminos" target="_blank"> Política de Privacidad de Datos</a></span>
                      </label>
                    </p>
                  </div>
                </div>
                <div class="col s12" style="text-align:center;">
                    <?php if($this->session->flashdata('valida')):?>
                        <span style="color: red;">* <?php echo $this->session->flashdata('valida'); ?></span>
                    <?php endif;?>
                    <div class="g-recaptcha" data-sitekey="6LdZ-qYUAAAAAJ5H-cCtpwqhlkfymcZed79aDwt0"></div>
                </div>
                <input type="hidden" name="correo_referido" value="<?= $referidos->correo_referido; ?>">
                <div class="col s12 center-align">
                  <button type="submit" class="btn">Enviar</button>
                </div>

              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>