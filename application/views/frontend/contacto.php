
<section class="section section-contacto capa-bg" style="background-image: url(<?= base_url(); ?>public/frontend/img/contacto1.jpg);padding-top: 200px;">
  <div class="container-fluid">
    <div class="container1 relative wow fadeInUp">
      <div class="title center-align">
        <h3>Contacto</h3>
        <img src="<?= base_url(); ?>public/frontend/img/line.png">
      </div>
      <div class="center-align">
        <p>Puedes escribirnos para comunicarnos tu consulta.</p>
        <p>Nuestros representantes te responderán a la mayor brevedad posible.</p>
      </div>

      <div class="">        
        <div class="form">
          <form action="<?= base_url() . 'administrador/correo/enviar'; ?>" method="post">
            <div class="row">
              <div class="input-field col s12">
                <label>Nombre y apellido</label>
                <input type="text" class="" name="nombres" required>
              </div>
              <div class="input-field col s12">
                <label>Correo electrónico</label>
                <input type="email" class="" name="email" required>
              </div>
              <div class="input-field col s12 m12 l6">
                <label>Celular</label>
                <input type="number" class="" name="telefono" required>
              </div>
              <div class="input-field col s12 m12 l6">
                <label>DNI</label>
                <input type="number" class="" name="dni" required>
              </div>
              <div class="input-field col s12 m12 l12">
                <label>Mensaje</label>
                <textarea name="mensaje"></textarea>
                <?php foreach ($correos as $correo): ?>
                  <input type="hidden" name="destino" value="<?= $correo->correos; ?>">
                <?php endforeach ?>
              </div>
              <div class="col s12">
                <div class="checkbox center-align">
                  <p>
                    <label>
                      <input type="checkbox" class="filled-in" checked="checked" required/>
                      <span>He leído y acepto la <a href="<?= base_url(); ?>terminos" target="_blank"> Política de Privacidad de Datos</a></span>
                    </label>
                  </p>
                </div>
              </div>
              <div class="col s12" style="text-align:center;">
                  <?php if($this->session->flashdata('valida')):?>
                      <span style="color: red;">* <?php echo $this->session->flashdata('valida'); ?></span>
                  <?php endif;?>
                  <div class="g-recaptcha" data-sitekey="6LdZ-qYUAAAAAJ5H-cCtpwqhlkfymcZed79aDwt0"></div>
              </div>
              <div class="col s12 center-align">
                <button class="btn" type="submit">ENVIAR</button>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>