<section class="section bg-politicas" style="background-image: url(<?= base_url(); ?>public/frontend/img/slide/2.jpg);">
  <!-- <div class="container"> -->

    <!-- <div class="row no-margin">
      
      <div class="col s12 m12 l8">

        <div class="ley politicas bg">
          <h3><strong>Información requerida por la ley 29571 art. 76.1</strong></h3>

          <br><br>

          <h5>1. Antecedentes</h5>
          <a target="_blank" href="<?= base_url(); ?>public/frontend/img/pdf/antecedentes.pdf">Antecedentes</a>
          
          <br><br>

          <h5>2. Condiciones Sismorresistentes</h5>
          <a target="_blank" href="<?= base_url(); ?>public/frontend/img/pdf/condiciones-sismorresistentes.pdf">Condiciones Sismorresistentes</a>
          
          <br><br>
          
          <h5>3. Conformidad de obra</h5>
          <a target="_blank" href="<?= base_url(); ?>public/frontend/img/pdf/conformidad-de-obra.pdf">Proceso Conformidad de Obra</a>
          
          <br><br>
          
          <h5>4. Declaratoria de Independización</h5>
          <a target="_blank" href="<?= base_url(); ?>public/frontend/img/pdf/declaratoria-de-independizacion.pdf">Proceso de Declaratoria de Fabrica independización</a>
          
          <br><br>
          
          <h5>5. Representantes legales</h5>
          <a target="_blank" href="<?= base_url(); ?>public/frontend/img/pdf/vigencia-ahv-enero-2019.pdf">Vigencia AHV Enero 2019</a>
          <a target="_blank" href="<?= base_url(); ?>public/frontend/img/pdf/vigencia-apm-enero-2019.pdf">Vigencia APM Enero 2019</a>
          <a target="_blank" href="<?= base_url(); ?>public/frontend/img/pdf/vigencia-mma-enero-2019.pdf">Vigencia MMA Enero 2019</a>
          <a target="_blank" href="<?= base_url(); ?>public/frontend/img/pdf/vigencia-mmb-enero-2019.pdf">Vigencia MMB Enero 2019</a>
          <a target="_blank" href="<?= base_url(); ?>public/frontend/img/pdf/vigencia-nof-enero-2019.pdf">Vigencia NOF Enero 2019</a>
          
          <br><br>
          
          <h5>6. Reglamento interno</h5>
          <a target="_blank" href="<?= base_url(); ?>public/frontend/img/pdf/reglamento-interno.pdf">Reglamento interno</a>
          
          <br><br>
          
          <h5>7. Características del inmueble</h5>
          <a target="_blank" href="<?= base_url(); ?>public/frontend/img/pdf/memoria-descriptiva.pdf">Memoria descriptiva</a>
          
          <br><br>
          
          <h5>8. Proceso de inscripción</h5>
          <a target="_blank" href="<?= base_url(); ?>public/frontend/img/pdf/proceso-de-inscripcion.pdf">Proceso de inscripción</a>

          

        </div>
        

      </div>

      <div class="col s12 m12 l4 ">
        
      </div>


    </div> -->

    <div class="row no-margin">
      <div class="col s12 no-padding">
        <ul class="tabs" style="margin-top:50px">
          <li class="tab col s4"><a class="active" href="#"><?= $politicas[0]->titulo ?></a></li>
          <li class="tab col s4"><a href="<?= base_url(); ?>politicas/politicas-navegacion"><?= $politicas[1]->titulo ?></a></li>
          <li class="tab col s4"><a href="<?= base_url(); ?>politicas/politicas-privacidad"><?= $politicas[2]->titulo ?></a></li>
        </ul>
        <div class="container-fluid">
          <div class="container1" style="border-top-left-radius:0;border-top-right-radius:0">
            <div class="col s12" style="float:none"><?= $politicas[0]->contenido ?></div>
          </div>
        </div>
      </div>
    </div>
  <!-- </div> -->
</section>
<script>
$(document).ready(function(){
  $('.tabs').tabs();
});
</script>