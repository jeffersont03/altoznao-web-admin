<section class="banner relative" style="background-image: url(<?= base_url() . 'public/frontend/img/proyecto/' . $proyecto->imagen_principal;?>);">
  <div class="container">
    <div class="row no-margin blanco">
      <div class="col s12">
        <div class="text-banner left relative">
          <h1 class="no-margin mayuscula"><?= $proyecto->nombre; ?></h1>
        </div>
        <div class="enlaces right relative">
          <div class="flex"><a href="<?= base_url(); ?>" class="enlace">Inicio</a> / <a href="<?= base_url() . 'proyectos'; ?>">Proyectos</a></div>
        </div>
      </div>
    </div>
  </div>
</section>

<section id="detail" class="section content-single">
  <div class="container">
    <div class="row">
      <div class="col s12 m12 l8">
        <div class="galeria">
          <!-- Product Images & Alternates -->
          <div class="product-images demo-gallery">
            <!-- Begin Product Images Slider -->
            <div class="main-img-slider">
              <?php foreach ($galerias as $galeria): ?>
                <a data-fancybox="gallery" href="<?= base_url() . 'public/frontend/img/proyecto/' . $galeria->imagen; ?>">
                  <img src="<?= base_url() . 'public/frontend/img/proyecto/' . $galeria->imagen; ?>" class="img-fluid">
                </a>                
              <?php endforeach ?>
             <!--  <a data-fancybox="gallery" href="img/single/1.jpg">
                <img src="img/single/1.jpg" class="img-fluid">
              </a>
              <a data-fancybox="gallery" href="img/single/3.jpg">
                <img src="img/single/3.jpg" class="img-fluid">
              </a>
              <a data-fancybox="gallery" href="img/single/1.jpg">
                <img src="img/single/1.jpg" class="img-fluid">
              </a>
              <a data-fancybox="gallery" href="img/single/2.jpg">
                <img src="img/single/2.jpg" class="img-fluid">
              </a>
              <a data-fancybox="gallery" href="img/single/3.jpg">
                <img src="img/single/3.jpg" class="img-fluid">
              </a> -->
            </div>
            <!-- End Product Images Slider -->
          
            <!-- Begin product thumb nav -->
            <ul class="thumb-nav">
              <?php foreach ($galerias as $galeria): ?>
                <li><img src="<?= base_url() . 'public/frontend/img/proyecto/' . $galeria->imagen; ?>"></li>               
              <?php endforeach ?>
              <!-- <li><img src="img/single/1.jpg"></li>
              
              <li><img src="img/single/3.jpg"></li>
              <li><img src="img/single/1.jpg"></li>
              <li><img src="img/single/2.jpg"></li>
              <li><img src="img/single/3.jpg"></li> -->
            </ul>
            <!-- End product thumb nav -->
          </div>
          <!-- End Product Images & Alternates -->
        </div>

        <div class="descripcion">
          <div class="card-estado">
            <div class="row no-margin">
              <div class="col s12 m12 l4 center-align">
                <div class="logo-proyecto">
                  <img src="<?= base_url() . 'public/frontend/img/proyecto/' . $proyecto->logo; ?>">
                  <h6><strong><?= $proyecto->departamento;?></strong></h6>
                </div>
              </div>
              <div class="col s12 m12 l8">
                <p><b><i class="az-next_week"> </i><?= $proyecto->estado; ?></b></p>
                <hr>
                <p><b><i class="az-business"> </i><?= $proyecto->etapas; ?></b></p>
                <hr>
                <p><b><i class="az-textsms"> </i><?= $proyecto->comentarios; ?></b></p>
              </div>
            </div>
          
          </div>
          <?= $proyecto->descripcion; ?>
          <div class="row">
            <div class="col s12 m6 l6">
              <div class="text-li">
                <?= $proyecto->item1; ?>
              </div>
            </div>
            <div class="col s12 m6 l6">
              <div class="text-li">
                <?= $proyecto->item2; ?>
              </div>
            </div>
            <div class="col s12 m12 l12">
              <div class="text-li">
                <?= $proyecto->item3; ?>
              </div>
            </div>
          </div>
        </div>
        
        <div class="plano">
          <div class="row">
            <div class="col s12">
              <div class="">
                  <?php if($planos): ?>
                    <h2><strong>Planos</strong></h2>
                  <?php endif;?>
                
              </div>
            </div>
            
            <?php foreach ($planos as $plano): ?>
              
            <div class="col s12 m6 l6">
              <div class="card card-plano">
                <div class="img-card">
                  <div class="material-placeholder"><img class="materialboxed zoom-cursor" src="<?= base_url() . 'public/frontend/img/proyecto/' . $plano->imagen; ?>"></div>
                </div>
                <div class="text-plano paddin center-align">
                  <h5><?= $plano->titulo?></h5>
                  <?= $plano->descripcion; ?>
                </div>
              </div>
            </div>
            <?php endforeach ?>
          </div>
        </div>

      </div>
    
      <div class="col s12 m12 l4">
        <aside>
          <div class="card-aside">
            <div class="card-title">
              <h4>Contacto</h4>
            </div>
            <div class="content-aside">
              <div class="form">
                <form action="<?= base_url() . 'administrador/correo/enviar_proyecto'; ?>" method="post">
                  <div class="row">
                    <div class="input-field col s12">
                      <label>Nombre y apellido</label>
                      <input type="text" class="" name="nombres" required>
                    </div>
                    <div class="input-field col s12">
                      <label>Correo electrónico</label>
                      <input type="email" class="" name="email" required>
                    </div>
                    <div class="input-field col s12 m12 l12">
                      <label>Celular</label>
                      <input type="number" class="" name="telefono">
                    </div>
                    <div class="input-field col s12 m12 l12">
                      <label>Mensaje</label>
                      <textarea name="mensaje"></textarea>
                        <input type="hidden" name="destino" value="<?= $proyecto->correo; ?>">
                        <input type="hidden" name="proyecto_env" value="<?= $proyecto->nombre; ?>">
                    </div>
                    <div class="col s12">
                      <div class="checkbox center-align">
                        <p>
                          <label>
                            <input type="checkbox" class="filled-in" checked="checked" required/>
                            <span>He leído y acepto la <a href="<?= base_url(); ?>terminos" target="_blank"> Política de Privacidad de Datos</a></span>
                          </label>
                        </p>
                      </div>
                    </div>
                    <div class="col s12">
                        <?php if($this->session->flashdata('valida')):?>
                          <span style="color: red;">* <?php echo $this->session->flashdata('valida'); ?></span>
                          <?php endif;?>
                        <div class="g-recaptcha" data-sitekey="6LdZ-qYUAAAAAJ5H-cCtpwqhlkfymcZed79aDwt0"></div>
                    </div>
                    <br>
                    <div class="col s12">
                      <button class="btn" type="submit">ENVIAR</button>
                    </div>
                  </form>
                </div>
              </div>
            </div>
          </div>

          <div class="aside-blog">
            <div class="title">
              <h3>Otros proyectos</h3>
            </div>
            <?php $cantidad = 0; ?>
            <?php foreach ($proyectos as $proyecto): ?>

              <?php if ($cantidad <= 4): ?>
                <div class="card-aside">
                  <div class="card-image">
                    <a href="<?= base_url() . 'proyectos/' . $proyecto->url; ?>"><img src="<?= base_url() . 'public/frontend/img/proyecto/' . $proyecto->imagen_fachada; ?>"></a>
                  </div>
                  <div class="card-content valign-wrapper">
                    <a href="<?= base_url() . 'proyectos/' . $proyecto->url; ?>"><h6><?= $proyecto->nombre; ?></h6><p><strong><?= $proyecto->departamento; ?></strong></p></a>
                  </div>
                </div>                
              <?php endif ?>
              <?php $cantidad = $cantidad + 1;?>
            <?php endforeach ?>

            <div>
              <a href="blog.php" class="btn">Ver todos</a>
            </div>
          </div>

        </aside>
      </div>
    </div>
  </div>
</section>

<style type="text/css">
  .slick-slider .slick-prev, .slick-slider .slick-next {
  z-index: 100;
  font-size: 2.5em;
  height: 40px;
  width: 40px;
  margin-top: -20px;
  color: #B7B7B7;
  position: absolute;
  top: 50%;
  text-align: center;
  color: #000;
  opacity: .3;
  transition: opacity .25s;
  cursor: pointer;
}
.slick-slider .slick-prev:hover, .slick-slider .slick-next:hover {
  opacity: .65;
}
.slick-slider .slick-prev {
  left: 0;
}
.slick-slider .slick-next {
  right: 0;
}

#detail .product-images {
  width: 100%;
  margin: 0 auto;
  border:1px solid #eee;
}
#detail .product-images li, #detail .product-images figure, #detail .product-images a, #detail .product-images img {
    display: block;
    outline: none;
    border: none;
    width: 100%;
}
#detail .product-images .main-img-slider figure {
  margin: 0 auto;
  padding: 0 2em;
}
#detail .product-images .main-img-slider figure a {
  cursor: pointer;
  cursor: -webkit-zoom-in;
  cursor: -moz-zoom-in;
  cursor: zoom-in;
}
#detail .product-images .main-img-slider figure a img {
  width: 100%;
  max-width: 400px;
  margin: 0 auto;
}
#detail .product-images .thumb-nav {
  margin: 0 auto;
  padding:20px 10px;
}
#detail .product-images .thumb-nav.slick-slider .slick-prev, #detail .product-images .thumb-nav.slick-slider .slick-next {
  font-size: 1.2em;
  height: 20px;
  width: 26px;
  margin-top: -10px;
}
#detail .product-images .thumb-nav.slick-slider .slick-prev {
  margin-left: -30px;
}
#detail .product-images .thumb-nav.slick-slider .slick-next {
  margin-right: -30px;
}
#detail .product-images .thumb-nav li {
  display: block;
  margin: 0 auto;
  cursor: pointer;
}
#detail .product-images .thumb-nav li img {
  display: block;
  width: 100%;
  margin: 0 auto;
  border: 2px solid transparent;
  -webkit-transition: border-color .25s;
  -ms-transition: border-color .25s;
  -moz-transition: border-color .25s;
  transition: border-color .25s;
}
#detail .product-images .thumb-nav li:hover, #detail .product-images .thumb-nav li:focus {
  border-color: #999;
}
#detail .product-images .thumb-nav li.slick-current img {
  border-color: #d12f81;
}



</style>

<script type="text/javascript">
  
// Main/Product image slider for product page
$('#detail .main-img-slider').slick({
  slidesToShow: 1,
  slidesToScroll: 1,
  infinite: true,
  arrows: true,
  fade:true,
  autoplay: true,
  autoplaySpeed: 4000,
  speed: 300,
  lazyLoad: 'ondemand',
  asNavFor: '.thumb-nav',
  prevArrow: '<div class="slick-prev"><i class="az-chevron_left"></i></div>',
  nextArrow: '<div class="slick-next"><i class="az-chevron_right"></i></div>'
});
// Thumbnail/alternates slider for product page
$('.thumb-nav').slick({
  slidesToShow: 4,
  slidesToScroll: 1,
  infinite: true,
  centerPadding: '0px',
  asNavFor: '.main-img-slider',
  dots: false,
  centerMode: false,
  draggable: true,
  speed:200,
  focusOnSelect: true,
  prevArrow: '<div class="slick-prev"><i class="az-chevron_left"></i></div>',
  nextArrow: '<div class="slick-next"><i class="az-chevron_right"></i></div>'
});


//keeps thumbnails active when changing main image, via mouse/touch drag/swipe
$('.main-img-slider').on('afterChange', function(event, slick, currentSlide, nextSlide){
  //remove all active class
  $('.thumb-nav .slick-slide').removeClass('slick-current');
  //set active class for current slide
  $('.thumb-nav .slick-slide:not(.slick-cloned)').eq(currentSlide).addClass('slick-current');  
});
</script>