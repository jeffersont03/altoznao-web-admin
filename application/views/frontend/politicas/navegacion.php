<section class="banner relative" style="background-image: url(http://altozano.pe/public/frontend/img/slide/2.jpg);">
  <div class="container">
    <div class="row no-margin blanco">
      <div class="col s12">
        <div class="text-banner left relative">
          <h1 class="no-margin">TÉRMINOS Y CONDICIONES</h1>
        </div>
        <div class="enlaces right relative">
          <div class="flex"><a href="" class="enlace">Inicio</a> / <a href="">Términos y Condiciones</a></div>
        </div>
      </div>
    </div>
  </div>
</section>
<section>
  <div class="container">
    <div class="row no-margin">
      <div class="col s12 no-padding">
        <ul class="tabs">
          <li class="tab col s12 m4"><a class="active" href="#"><?= $politicas[0]->titulo ?></a></li>
          <li class="tab col s12 m4"><a href="<?= base_url(); ?>terminos/politicas-privacidad"><?= $politicas[1]->titulo ?></a></li>
          <li class="tab col s12 m4"><a href="<?= base_url(); ?>terminos/proteccion-consumidor"><?= $politicas[2]->titulo ?></a></li>
        </ul>
      </div>
    </div>
  </div>
</section>
<section class="section histori cover" style="background-image: url(http://altozano.pe/public/frontend/img/nosotros1.jpg);">
  <div class="container">
    <div class="row no-margin">
      <div class="section-historia center-align" style="text-align:left">
        <h3><strong><?= $politicas[0]->titulo ?></strong></h3>
        <img src="http://altozano.pe/public/frontend/img/line.png">
        <br>
        <br>
        <?= $politicas[0]->contenido ?>
      </div>
    </div>
  </div>
</section>
<style>
.tabs .tab a {
  color: #002d5d;
  font-weight: 800;
} 
.tabs .tab a.active {
  border-bottom: 3px solid #f18a06;
}
.tabs .tab a.active, .tabs .tab a:hover {
  color: #f18a06;
}
@media only screen and (max-width: 992px) {
  .tabs {
    display: block;
    height: 100%;
  }
}
</style>