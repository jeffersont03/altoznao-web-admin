  <div class="messenger tooltipped wow fadeInRight" data-wow-delay="1s" data-position="top" data-tooltip="Mensaje">
    <a href="#messenger" class="modal-trigger"><img src="<?= base_url(); ?>public/frontend/img/messenger.png"></a>
  </div>

<!-- Modal Structure -->
  <div id="messenger" class="modal modal-cita">
    <div class="modal-content">
      <div class="center-align">
        <div class="title">
          <h3><strong>Mensaje de Facebook</strong></h3>
          
        </div>
        <div>
          <div class="fb-page" data-href="https://www.facebook.com/altozanoinmobiliaria/" data-tabs="messages,timeline" data-width="" data-height="" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true"><blockquote cite="https://www.facebook.com/altozanoinmobiliaria/" class="fb-xfbml-parse-ignore"><a href="https://www.facebook.com/altozanoinmobiliaria/">Altozano</a></blockquote></div>
        </div>
        
      </div>
    </div>
    <a href="#!" class="modal-close">X</a>
  </div>

<footer class="page-footer" style="background-image: url(<?= base_url(); ?>public/frontend/img/footer-bg.jpg);">
    <div class="container">
      <div class="row">

        <div class="col s12 m12 l4 blanco center-align">
          <img src="<?= base_url(); ?>public/frontend/img/logo-blanco.png" style="max-width: 250px;">
          <div class="redes-footer center-align">
            <ul>
              <?php foreach ($redes as $red): ?>
                <li><a href="<?= $red->link; ?>" target="_blank"><img src="<?= base_url() . 'public/frontend/img/' . $red->icono; ?>"></a></li>                
              <?php endforeach ?>
            </ul>
          </div>
          <div class="borde-rad">
            <img src="<?= base_url(); ?>public/frontend/img/certificado1.jpg">
            <img src="<?= base_url(); ?>public/frontend/img/certificado2.jpg">
          </div>
          <br><br>
        </div>

        <div class="col s12 m12 l3 color-1 blanco offset-l1">
          <div class="subtitle relative"><h4>Menu</h4></div>
          <ul>
            <li><a href="<?= base_url(); ?>">Inicio</a></li>
            <li><a href="<?= base_url() . 'nosotros'; ?>">Nosotros</a></li>
            <li><a href="<?= base_url() . 'proyectos'; ?>">Proyectos</a></li>
            <li><a href="<?= base_url() . 'referidos'; ?>">Referidos</a></li>
            <li><a href="<?= base_url() . 'contacto'; ?>">Contacto</a></li>
            <li><a href="#cotiza" class="modal-trigger">Agendar una cita</a></li>
          </ul>
          <p>&nbsp;</p>
          <div class="subtitle relative"><h4>Legales</h4></div>
          <?php
            $resultados = $this->db->get("politicas");
            if($resultados->num_rows() > 0 ) {
            ?>
            <ul class="legales">
              <?php $array = $resultados->result(); ?>
              <li><a target="_blank" href="<?= base_url() . 'terminos/politicas-navegacion'; ?>"><?= $array[0]->titulo ?></a></li>
              <li><a target="_blank" href="<?= base_url() . 'terminos/politicas-privacidad'; ?>"><?= $array[1]->titulo ?></a></li>
              <li><a target="_blank" href="<?= base_url() . 'public/frontend/img/pdf/' . $array[3]->contenido; ?>"><?= $array[3]->titulo ?></a></li>
              <li><a target="_blank" href="<?= base_url() . 'public/frontend/img/pdf/' . $array[4]->contenido; ?>"><?= $array[4]->titulo ?></a></li>
              <li><a target="_blank" href="<?= base_url() . 'terminos/proteccion-consumidor'; ?>"><?= $array[2]->titulo ?></a></li>
            </ul>
          <?php
            }
          ?>
        </div>

        <div class="col s12 m12 l4">
          <div class="color-1 blanco">
            <div class="subtitle relative"><h4>Contacto</h4></div>
            <?= $empresa->contacto; ?>
          </div>
        </div>

      </div>
    </div>
    <div class="footer-copyright">
      <div class="container">
      © <?= date('Y'); ?> Todos los derechos reservados
      <a class="grey-text text-lighten-4 right" target="_blank" href="https://playgroup.pe/">Desarrollador por PLAY Group <img src="<?= base_url(); ?>public/frontend/img/icon-play.png"></a>
      </div>
    </div>
</footer>

 

  <script type="text/javascript">
      wow = new WOW(
          {
          boxClass:     'wow',      // default
          animateClass: 'animated', // default
          offset:       0,          // default
          mobile:       true,       // default
          live:         true        // default
          }
        )
        wow.init();
  </script>

  <script type="text/javascript">
     $(document).on("scroll",function(){
        if($(document).scrollTop()>0){ 
            $("header").addClass("menu2");
            $("header nav .brand-logo").addClass("logo-menu");   
        } else{
            $("header").removeClass("menu2"); 
            $("header nav .brand-logo").removeClass("logo-menu");
        }
    });
  </script>

  <script type="text/javascript">
    $(document).ready(function(){
      
      $(".Modern-Slider").slick({
        autoplay:true,
        autoplaySpeed:10000,
        speed:900,
        slidesToShow:1,
        slidesToScroll:1,
        pauseOnHover:false,
        dots:true,
        pauseOnDotsHover:true,
        cssEase:'linear',
        fade:true,
        draggable:false,
        prevArrow:'<button class="PrevArrow"><span class="az-chevron_left"></span></button>',
        nextArrow:'<button class="NextArrow"><span class="az-chevron_right"></span></button>', 
      });
      
    });
  </script>


  <script type="text/javascript">
    $(document).ready(function(){
      $('.scrollspy').scrollSpy();
      $('select').formSelect();
      $('.tooltipped').tooltip();
      $('.collapsible').collapsible();
      $('.modal').modal();
      $('.sidenav').sidenav();
      $('.dropdown-trigger').dropdown();
      $('.datepicker').datepicker();
      $('.materialboxed').materialbox();
    });
  </script>
  
  <script type="text/javascript">
    $(document).ready(function() {
      var owl = $(".proyectos-section #owl-demo");
      owl.owlCarousel({
          autoPlay: 4000,
          items : 3, //10 items above 1000px browser width
          itemsDesktop : [1180,2], //5 items between 1000px and 901px
          itemsDesktopSmall : [900,2], // betweem 900px and 601px
          itemsTablet: [600,2], //2 items between 600 and 0
          itemsMobile : [500,1] // itemsMobile disabled - inherit from itemsTablet option
      });
     
      // Custom Navigation Events
      $(".next").click(function(){
        owl.trigger('owl.next');
        owl.trigger('owl.stop');
      })
      $(".prev").click(function(){
        owl.trigger('owl.prev');
        owl.trigger('owl.stop');
      })
     
    });
  </script>

  <script>
    function ver_proyectos(tipo)
    {
      /*var form = document.createElement('form');
      form.action = '<?= base_url() . 'proyectos'?>';
      form.method = 'POST';

      var input = document.createElement('input');
      input.type = 'text';
      input.name = 'tipo_proyecto';
      input.value = tipo;
      document.body.appendChild(form);
      document.body.appendChild(input);
      form.appendChild(input);  
      form.submit(); */
      window.location.href = '<?= base_url(); ?>' + tipo;
    }
  </script>
  <style>
  .g-recaptcha {
    margin-bottom: 20px;
  }
  .g-recaptcha div {
    margin: auto;
  }
  .legales a {
    line-height: 18px !important;
    padding: 8px 0;
  }
  </style>



  </body>
</html>