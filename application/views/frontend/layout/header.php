<!DOCTYPE html>
<html lang="es">
<head>

  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0"/>
  <link rel="shortcut icon" type="image/x-icon" href="<?= base_url();?>public/frontend/img/icon-altozano.png" />
  <title>Altozano</title>
    <script src="https://www.google.com/recaptcha/api.js" async defer></script>
  <!-- CSS  -->
  <link href="<?= base_url(); ?>public/frontend/css/materialize.css" type="text/css" rel="stylesheet" media="screen,projection"/>
  <link href="https://fonts.googleapis.com/css?family=Nunito:400,600,700,800" rel="stylesheet">
  <link rel="stylesheet" type="text/css" href="<?= base_url(); ?>public/frontend/css/style.css"/>
  <link rel="stylesheet" type="text/css" href="<?= base_url(); ?>public/frontend/css/icono.css"/>
  <link rel="stylesheet" type="text/css" href="<?= base_url(); ?>public/frontend/css/animate.css"/>
  <link rel="stylesheet" type="text/css" href="<?= base_url(); ?>public/frontend/css/carusel-themes.css"/>
  <link rel="stylesheet" type="text/css" href="<?= base_url(); ?>public/frontend/css/carusel-transitions.css"/>
  <link rel="stylesheet" type="text/css" href="<?= base_url(); ?>public/frontend/css/carusel.css"/>

  <link rel="stylesheet" type="text/css" href="<?= base_url(); ?>public/frontend/css/slick.css"/>
  <link rel="stylesheet" type="text/css" href="<?= base_url(); ?>public/frontend/css/jquery.fancybox.min.css"/>

   <!--  Scripts-->
  <script src="<?= base_url(); ?>public/frontend/js/jquery.js"></script>
  <script src="<?= base_url(); ?>public/frontend/js/materialize.js"></script>
  <script src="<?= base_url(); ?>public/frontend/js/carusel.js"></script>
  <script src="<?= base_url(); ?>public/frontend/js/wow.min.js"></script>
  <script src='https://kenwheeler.github.io/slick/slick/slick.js'></script>

  <script src='https://cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js'></script>
  <script src='https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.3.5/jquery.fancybox.min.js'></script>

<div id="fb-root"></div>
<script async defer crossorigin="anonymous" src="https://connect.facebook.net/es_LA/sdk.js#xfbml=1&version=v3.3"></script>

<!-- Facebook Pixel Code -->
<script>
!function(f,b,e,v,n,t,s)
{if(f.fbq)return;n=f.fbq=function(){n.callMethod?
n.callMethod.apply(n,arguments):n.queue.push(arguments)};
if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
n.queue=[];t=b.createElement(e);t.async=!0;
t.src=v;s=b.getElementsByTagName(e)[0];
s.parentNode.insertBefore(t,s)}(window,document,'script',
'https://connect.facebook.net/en_US/fbevents.js');
fbq('init', '402540433695993'); 
fbq('track', 'PageView');
</script>
<noscript>
<img height="1" width="1" 
src="https://www.facebook.com/tr?id=402540433695993&ev=PageView
&noscript=1"/>
</noscript>
<!-- End Facebook Pixel Code -->


</head>
<body>

<ul id="proy" class="dropdown-content">
  <li><a onclick="ver_proyectos('proyectos-en-venta')">Proyectos en Ventas</a></li>
  <li class="divider"></li>
  <li><a onclick="ver_proyectos('proximos-proyectos')">Próximos Proyectos</a></li>
  <li class="divider"></li>
  <li><a onclick="ver_proyectos('proyectos-entregados')">Proyectos Entregados</a></li>
</ul>

<header>
  <nav>
    <div class="container">
    <div class="nav-wrapper">
      <div class="menu">
          <div class="left hide-on-med-and-down animated fadeInLeft delay-0.2s">
            <ul>
              <li><a href="<?= base_url(); ?>">Inicio</a></li>
              <li><a href="<?= base_url(); ?>nosotros">Nosotros</a></li>
              <li><a href="<?= base_url(); ?>proyectos" class="dropdown-trigger" data-target="proy">Proyectos<i class="az-expand_more right"></i></a></li>
            </ul>
          </div>
          <div><a href="<?= base_url(); ?>" class="brand-logo center"><img class="logo1" src="<?= base_url(); ?>public/frontend/img/logo.png"><img class="logo2" style="display: none;" src="<?= base_url(); ?>public/frontend/img/logo1.png"></a></div>
          <div class="right hide-on-med-and-down animated fadeInRight delay-0.2s">
            <ul>
              <li><a href="<?= base_url(); ?>referidos">Referidos</a></li>
              <li><a href="<?= base_url(); ?>contacto">Contacto</a></li>
              <li><a href="#cotiza" class="btn modal-trigger"><i class="az-email2"> </i><span>Agenda una cita</span></a></li>
            </ul>
          </div>
      </div>
      <a href="#" data-target="mobile-demo" class="sidenav-trigger"><i class="az-format_list_bulleted"></i></a>
    </div>
    </div>
  </nav>

  <div class="sidenav" id="mobile-demo">
    <div class="movil-logo">
      <img class="logo1" src="<?= base_url(); ?>public/frontend/img/logo.png">
    </div>
    <ul>
      <li><a href="<?= base_url(); ?>">Inicio</a></li>
      <li><a href="<?= base_url(); ?>nosotros">Nosotros</a></li>
      <li><a href="<?= base_url(); ?>proyectos">Proyectos</a></li>
      <li><a href="<?= base_url(); ?>referidos">Referidos</a></li>
      <li><a href="<?= base_url(); ?>contacto">Contacto</a></li>
      <li><a href="#cotiza" class="modal-trigger">Agenda una cita</a></li>
    </ul>
  </div>
</header>


<!-- Modal Structure -->
  <div id="cotiza" class="modal modal-cita">
    <div class="modal-content">
      <div class="center-align">
        <div class="title">
          <h2><b>Agendar cita</b></h2>
          <hr>
        </div>
        <p>Puedes escribirnos para comunicarnos tu consulta.</p><p>Nuestros representantes te responderán a la mayor brevedad posible.</p>
      </div>
      <form action="<?= base_url() . 'administrador/correo/cita'; ?>" method="post">
        <div class="input-field col s12">
          <label>Proyecto</label>
          <select required name="proyecto">
            <?php foreach ($proyectos as $proyecto): ?>
              <option value="<?= $proyecto->nombre; ?>"><?= $proyecto->nombre; ?></option>
            <?php endforeach ?>
          </select>
        </div>
        <div class="input-field col s12">
          <label>Nombre y Apellido</label>
          <input name="nombres" type="text" required="">
        </div>
        <div class="input-field col s12">
          <label>Teléfono</label>
          <input name="telefono" type="number" required="">
        </div>
        <div class="input-field col s12">
          <label>Correo electronico</label>
          <input name="correo" type="email" required="">
        </div>
        <div class="input-field col s12">
          <label>Fecha de la cita</label>
          <input name="fecha" type="date" required="">
        </div>
        <div class="input-field col s12">
          <label>Mensaje</label>
          <textarea name="mensaje"></textarea>
          <?php foreach ($correos as $correo): ?>
            <input type="hidden" name="destino" value="<?= $correo->correos; ?>">
          <?php endforeach ?>
        </div>
        <div class="col s12">
          <div class="checkbox center-align">
            <p>
              <label>
                <input type="checkbox" class="filled-in" checked="checked" required/>
                <span>He leído y acepto la <a href="<?= base_url(); ?>terminos" target="_blank"> Política de Privacidad de Datos</a></span>
              </label>
            </p>
          </div>
        </div>
        <div class="input-field col s12" style="text-align:center;">
          <?php if($this->session->flashdata('valida_cita')):?>
              <script>
              document.addEventListener('DOMContentLoaded', function () {
                  var Modalelem = document.querySelector('.modal-cita');
                  var instance = M.Modal.init(Modalelem);
                  instance.open();
              });
              </script>
              <span style="color: red;">* <?php echo $this->session->flashdata('valida_cita'); ?></span>
          <?php endif;?>
          <div 
            class="g-recaptcha" 
            data-sitekey="6LdZ-qYUAAAAAJ5H-cCtpwqhlkfymcZed79aDwt0">
          </div>
        </div>
        <div class="col s12">
          <button class="btn" type="submit">Enviar</button>
        </div>
      </form>
    </div>
  </div>