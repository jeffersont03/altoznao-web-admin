<section class="banner relative" style="background-image: url(<?= base_url(); ?>public/frontend/img/slide/2.jpg);">
  <div class="container">
    <div class="row no-margin blanco">
      <div class="col s12">
        <div class="text-banner left relative">
          <h1 class="no-margin">Proyectos</h1>
        </div>
        <div class="enlaces right relative">
          <div class="flex"><a href="<?= base_url(); ?>" class="enlace">Inicio</a> / <a href="">Proyectos</a></div>
        </div>
      </div>
    </div>
  </div>
</section>


<section class="top-botones bg">
  <div class="container">
    <div class="row">
      <div class="col s12 m12 l4">
        <?php if ($activado == 'venta'): ?>
          <a onclick="ver_proyectos('proyectos-en-venta')" class="active proyect">
        <?php else: ?>
          <a onclick="ver_proyectos('proyectos-en-venta')" style="cursor: pointer;">
        <?php endif ?>
            <div class="btn-proy">
              <strong>Proyectos en Ventas</strong>
            </div>
          </a>
      </div>
      <div class="col s12 m12 l4">
        <?php if ($activado == 'proximos'): ?>
          <a onclick="ver_proyectos('proximos-proyectos')" class="active proyect">
        <?php else: ?>
          <a onclick="ver_proyectos('proximos-proyectos')" style="cursor: pointer;">
        <?php endif ?>
            <div class="btn-proy">
              <strong>Próximos Proyectos</strong>
            </div>
          </a>
      </div>
      <div class="col s12 m12 l4">
        <?php if ($activado == 'entregados'): ?>
          <a onclick="ver_proyectos('proyectos-entregados')" class="active proyect">
        <?php else: ?>
          <a onclick="ver_proyectos('proyectos-entregados')" style="cursor: pointer;">
        <?php endif ?>
            <div class="btn-proy">
              <strong>Proyectos Entregados</strong>
            </div>
          </a>
      </div>
    </div>
  </div>  
</section>


<section class="section bg">
  <div class="container">
    <div class="row">
      <div class="col s12 m12 l12">
        <div class="center-align">
          <p><strong></strong></p>
          <img src="<?= base_url(); ?>public/frontend/img/line.png">
        </div>
      </div>
    </div>

    <div class="row">
      <?php foreach ($proyectos as $proyecto): ?>
      <div class="col s12 m6 l4">
        <div class="card-proyecto center-align">
          <img src="<?= base_url() . 'public/frontend/img/proyecto/' . $proyecto->imagen_fachada; ?>">
          <div class="card-title flex">
            <div class="logo-proyecto">
              <img src="<?= base_url() . 'public/frontend/img/proyecto/' . $proyecto->logo; ?>">
              <h6><strong><?= $proyecto->departamento; ?></strong></h6>
            </div>
            <div class="content-card-proyecto">
              <p><?= $proyecto->estado;?></p>
              <hr>
              <p><?= $proyecto->etapas;?></p>
              <hr>
              <p><?= $proyecto->comentarios; ?></p>
            </div>
          </div>
          <div>
            <a href="<?= base_url() . 'proyectos/' . $proyecto->url; ?>" class="btn">Ver proyecto</a>
          </div>
        </div>
      </div>
      <?php endforeach ?>

    </div>

  </div>
</section>