

<section>
  

  <div class="redes wow fadeInLeft" data-wow-delay="1s">
    <ul>
      <?php foreach ($redes as $red): ?>
        <li><a href="<?= $red->link; ?>" target="_blank"><img src="<?= base_url() . 'public/frontend/img/' . $red->icono; ?>" ></a></li>
        
      <?php endforeach ?>
      
    </ul>
  </div>

  <div class="Modern-Slider">
    <!-- Item -->
  
    <?php foreach ($sliders as $slide): ?>
    <div class="item">
      <div class="img-fill">
        <img class="slide" src="<?= base_url() . 'public/frontend/img/' . $slide->imagen; ?>" alt="">
        <div class="info valign-wrapper">
          <div class="text-slide blanco">
            <?= $slide->texto; ?>
          </div>
        </div>
      </div>
    </div>
    <?php endforeach ?>
    
   </div>
</section>


<section class="section bg1">
  <div class="container-fluid">
    <div class="title center-align">
      <h2>Nuestros Proyectos</h2>
      <img src="<?= base_url(); ?>public/frontend/img/line.png">
    </div>
    
    <div class="proyectos-section">
      <div id="owl-demo" class="owl-carousel owl-theme">
        
        <?php foreach ($proyectos as $proyecto): ?>
          
        <div class="item">
          <a href="<?= base_url() . 'proyectos/' . $proyecto->url; ?>">
          <div class="card-proyectos">
            <div class="card-logo">
              <img src="<?= base_url() . 'public/frontend/img/proyecto/' .  $proyecto->logo; ?>">
            </div>
            <div class="content-proyect">
              <h5><?= $proyecto->estado; ?></h5>
              <hr>
              <h5><?= $proyecto->etapas; ?></h5>
              <hr>
              <h5><?= $proyecto->comentarios; ?></h5>
            </div>
            <img src="<?= base_url() . 'public/frontend/img/proyecto/' . $proyecto->imagen_fachada; ?>">
            <div class="footer blanco">
              <h5 class="title-proyect"><strong><?= $proyecto->nombre; ?></strong></h5>
              <h5><strong><?= $proyecto->departamento; ?></strong></h5>
            </div>
          </div>
          </a>
        </div>
        <?php endforeach ?>
      </div>
       
      <div class="customNavigation">
        <a class="btn prev"><</a>
        <a class="btn next">></a>
      </div>
    </div>

  </div>
</section>


<section class="section section-contacto capa-bg" style="background-image: url(<?= base_url(); ?>public/frontend/img/slide/2.jpg)" id="formulario_cotiza">
  <div class="container-fluid">
    <div class="container-form relative wow fadeInUp" data-wow-delay="1s">
      <div class="title center-align">
        <h3>Cotiza con nosotros</h3>
        <img src="<?= base_url(); ?>public/frontend/img/line.png">
      </div>
      <div class="form">
         <form action="<?= base_url() . 'administrador/correo/cotiza'; ?>" method="post">
        <div class="row">
          <div class="input-field col s12">
            <label>Proyecto</label>
            <select name="proyecto" required>
              <option value="" disabled selected>--</option>
              <?php foreach ($proyectos as $proyecto): ?>
                <option value="<?= base_url() . $proyecto->url; ?>"><?= $proyecto->nombre; ?></option>
              <?php endforeach ?>
            </select>
          </div>
          <div class="input-field col s12">
            <label>Nombre y apellido</label>
            <input type="text" class="" name="nombres" required>
          </div>
          <div class="input-field col s12">
            <label>Correo electrónico</label>
            <input type="email" class="" name="correo" required>
          </div>
          <div class="input-field col s12 m12 l6">
            <label>Celular</label>
            <input type="number" class="" name="telefono" required>
          </div>
          <div class="input-field col s12 m12 l6">
            <label>DNI</label>
            <input type="number" class="" name="dni" required>
            <?php foreach ($correos as $correo): ?>
                <input type="hidden" name="destino" value="<?= $correo->correos; ?>">
              <?php endforeach ?>
          </div>
          <div class="col s12">
            <div class="checkbox center-align">
              <p>
                <label>
                  <input type="checkbox" class="filled-in" checked="checked" required/>
                  <span>He leído y acepto la <a href="<?= base_url(); ?>terminos" target="_blank"> Política de Privacidad de Datos</a></span>
                </label>
              </p>
            </div>
          </div>
          <div class="col s12" style="text-align:center;">
            <?php if($this->session->flashdata('valida')):?>
                <script>
                    $(document).scrollTop( $("#formulario_cotiza").offset().top );  
                </script>
                <span style="color: red;">* <?php echo $this->session->flashdata('valida'); ?></span>
            <?php endif;?>
          <div class="g-recaptcha" data-sitekey="6LdZ-qYUAAAAAJ5H-cCtpwqhlkfymcZed79aDwt0"></div>
        </div>
          <div class="col s12 center-align">
            <button class="btn" type="submit">ENVIAR</button>
          </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</section>

<section class="section-l">
  <div class="container">
    <div class="row">
      <div class="col s12 m12 l6">
        <div class="wow fadeInLeft" data-wow-delay="0.5s">
          <div class="title">
            <h3><?= $empresa->tituloConfia; ?></h3>
            <img src="<?= base_url(); ?>public/frontend/img/line2.png">
          </div>
          <div style="width: 100%;">
            <div class="left">
              <img src="<?= base_url() . 'public/frontend/img/' . $empresa->iconoConfia; ?>">
            </div>
            <div class="left resaltar">
              <?= $empresa->subtituloConfia; ?>
            </div>
          </div>
          <div style="display: inline-block;">
            <?= $empresa->textoConfia; ?>
          </div>
          <br><br>
        </div>
      </div>
      <div class="col s12 m12 l6">
        <div class="content-img relative wow fadeInRight" data-wow-delay="0.5s">
          <img src="<?= base_url() . 'public/frontend/img/' . $empresa->imagenConfia; ?>">
        </div>
      </div>
    </div>
  </div>
</section>