
<section class="banner relative" style="background-image: url(<?= base_url(); ?>public/frontend/img/slide/2.jpg);">
  <div class="container">
    <div class="row no-margin blanco">
      <div class="col s12">
        <div class="text-banner left relative">
          <h1 class="no-margin">NOSOTROS</h1>
        </div>
        <div class="enlaces right relative">
          <div class="flex"><a href="" class="enlace">Inicio</a> / <a href="">Nosotros</a></div>
        </div>
      </div>
    </div>
  </div>
</section>


<section class="section1">
  <div class="container">
    <div class="row no-margin">
      <div class="col s12 m12 l6">
        <?= $empresa->nosotros; ?>
      </div>
      <div class="col s12 m12 l6">
        <img src="<?= base_url() . 'public/frontend/img/' . $empresa->imagen; ?>">
      </div>
    </div>
  </div>
</section>


<section class="section1 histori cover" style="background-image: url(<?= base_url(); ?>public/frontend/img/nosotros1.jpg);">
  <div class="container-short">
    <div class="row no-margin">
      <div class="section-historia center-align">
          <h3><strong><?= $empresa->titulo_historia; ?></strong></h3>
          <img src="<?= base_url(); ?>public/frontend/img/line.png">
          <br><br>                
          <?= $empresa->historia; ?>   
          
      </div>
    </div>
  </div>
</section>


<section class="section bg">
  <div class="container">
    <div class="row no-margin">

      <div class="col s12 m12 l6">
        <div class="valores">
          <h3><strong><?= $empresa->titulo_valores; ?></strong></h3>
          <img src="<?= base_url(); ?>public/frontend/img/line2.png">
          <br><br>   
          <?= $empresa->valores; ?>
        </div>
      </div>

      <div class="col s12 m12 l6">
        <div class="">
          <h3><strong><?= $empresa->titulo_valores_colaboradores; ?></strong></h3>
          <img src="<?= base_url(); ?>public/frontend/img/line2.png">
          <br><br>   
          <?= $empresa->valores_colaboradores; ?>
        </div>
      </div>
      
    </div>
  </div>
</section>

<section class="myv">
  <div class="container-fluid smp">
    <div class="row no-margin">
      <div class="col s12 m12 l6 smp" style="background-image: url(<?= base_url(); ?>public/frontend/img/mision.jpg)">
        <div class="text-mision blanco valign-wrapper">
          <div>
            <h3>Misión</h3>
            <br>
            <?= $empresa->mision; ?>
          </div>
        </div>
      </div>
      <div class="col s12 m12 l6 smp2" style="background-image: url(<?= base_url(); ?>public/frontend/img/vision.jpg)">
        <div class="text-mision blanco valign-wrapper">
          <div>
            <h3>Visión</h3>
            <br>
            <?= $empresa->vision; ?>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
