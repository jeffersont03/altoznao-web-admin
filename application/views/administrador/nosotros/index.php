<style>
    .cabecera_prueba {
        background: #f5f5f5;
        padding: 1% 0%;
    }
</style>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <ol class="breadcrumb breadcum">
      <li><a href="<?php echo base_url() ?>">Inicio</a></li>
      <li class="active">Empresa</li>
    </ol>
    <section class="content-header">
        <h1>
        Empresa
        <small>Datos de Empresa</small>
        </h1>
    </section>
    <!-- Main content -->
    <section class="content">
        <!-- Default box -->
        <div class="box box-solid">
            <div class="box-body">
                <div class="row">
                    <div class="col-md-12">
                      <form method="post" enctype="multipart/form-data" id="form_cursos" autocomplete="nope">
                        <div class="modal-body">
                            <div class="alert alert-warning" id="errores" role="alert" style="display: none;"></div>
                            <div class="col-xs-12 col-md-12 col-lg-6" style="padding:0">
                                <div class="form-group col-xs-12">
                                    <label for="txt_descripcion"><span style="color: red">*</span> Nosotros:</label>
                                    <textarea style="height:80px;width:100%;resize:none;" class="form-control" name="txt_descripcion_nos" id="txt_descripcion_nos"></textarea>
                                </div>                               
                            </div>
                            <div class="col-xs-12 col-md-12 col-lg-6" style="padding:0">
                                <div class="form-group col-xs-12">
                                    <div class="relative">
                                        <label><span style="color: red">*</span> Imagen de Nosotros:<br><small>(Tamaño recomendado 455 x 560px)</small></label>
                                        <div class="bg-form-imagen" style="text-align: center;background: #f5f5f5;">
                                            <img width="auto" height="auto" id="imagenmuestra_nosotros" style="max-width: 100%;height: 300px;">
                                        </div>
                                        <input type="file" class="form-control" name="imagen" id="imagen_nosotros">
                                    </div>
                                </div>
                            </div>

                            <div class="col-xs-12 col-md-12 col-lg-6" style="padding:0">
                                <div class="form-group col-xs-12">
                                    <label for=""><span style="color: red">*</span> Titulo de Historia</label>
                                    <input type="text" class="form-control" id="titulo_historia" placeholder="Historia">
                                </div>
                                <div class="form-group col-xs-12">
                                    <label for="txt_historia"><span style="color: red">*</span> Historia:</label>
                                    <textarea class="form-control" name="txt_historia" id="txt_historia"></textarea>
                                </div>  
                            </div>
                            <div class="col-xs-12 col-md-12 col-lg-6" style="padding:0">
                                <div class="form-group col-xs-12">
                                    <label for=""><span style="color: red">*</span> Titulo de Nuestros Valores</label>
                                    <input type="text" class="form-control" id="titulo_valores" placeholder="Nuestros Valores">
                                </div>
                                <div class="form-group col-xs-12">
                                    <label for="txt_valores"><span style="color: red">*</span> Nuestros valores:</label>
                                    <textarea class="form-control" name="txt_valores" id="txt_valores"></textarea>
                                </div>
                                
                            </div>
                                
                            <div class="col-xs-12 col-md-12 col-lg-6" style="padding:0">
                                <div class="form-group col-xs-12">
                                    <label for=""><span style="color: red">*</span> Titulo de Valores de Nuestros Colaboradores</label>
                                    <input type="text" class="form-control" id="titulo_colaboradores" placeholder="Valores de Nuestros Colaboradores">
                                </div>

                                <div class="form-group col-xs-12">
                                    <label for="txt_colaboradores"><span style="color: red">*</span> Valores de Nuestros Colaboradores:</label>
                                    <textarea class="form-control" name="txt_colaboradores" id="txt_colaboradores"></textarea>
                                </div> 
                                <div class="form-group col-xs-12">
                                    <label for="txt_contacto"><span style="color: red">*</span> Contactanos:</label>
                                    <textarea class="form-control" name="txt_contacto" id="txt_contacto"></textarea>
                                </div>  
                            </div>
                            <div class="col-xs-12 col-md-12 col-lg-6" style="padding:0">
                                <div class="form-group col-lg-6 col-md-6 col-xs-12">
                                    <label for="txt_mision"><span style="color: red">*</span> Mision:</label>
                                    <textarea class="form-control" name="txt_mision" id="txt_mision"></textarea>
                                </div> 
                                <div class="form-group col-lg-6 col-md-6 col-xs-12">
                                    <label for="txt_vision"><span style="color: red">*</span> Vision:</label>
                                    <textarea class="form-control" name="txt_vision" id="txt_vision"></textarea>
                                </div> 
                            </div>

                            <div class="col-xs-12 text-center" style="margin-top: 2%;">
                                <button type="button" class="btn btn-success" onclick="actualizar_datos_empresa()"><i class="fa fa-save"></i>&nbsp;Actualizar</button>
                            </div>
                            <hr>
                            <div class="form-group col-md-12">
                                <h4>Información General:</h4>
                            </div>

                            <div class="form-group col-md-12 col-xs-12" style="padding:0">
                                <div class="col-md-6 col-xs-12">
                                    <div class="text-center cabecera_prueba">
                                        <h4><i class="fa fa-envelope"></i>&nbsp;Correo Formulario&nbsp;&nbsp;<!-- <button type="button" class="btn btn-primary" onclick="mostrar_agregar_correo()"><i class="fa fa-plus-circle"></i>&nbsp;Agregar</button> --></h4>
                                    </div>
                                    <br>
                                    <div class="table-responsive">
                                        <table class="table table-bordered" id="tabla_correos">
                                            <thead>
                                                <th>Correo</th>
                                                <th><i class="fa fa-cog"></i></th>
                                            </thead>
                                        </table>
                                    </div>
                                </div>
                                <!-- <div class="col-md-4 col-xs-12">
                                    <div class="text-center cabecera_prueba">
                                        <h4><i class="fa fa-mobile"></i>&nbsp;Telefonos&nbsp;&nbsp;<button type="button" class="btn btn-primary" onclick="mostrar_agregar_telefono()"><i class="fa fa-plus-circle"></i>&nbsp;Agregar</button></h4>
                                        <h4><i class="fa fa-mobile"></i>&nbsp; Contacto</h4>
                                    </div>
                                    <br>
                                    <div class="table-responsive">
                                        <table class="table table-bordered" id="tabla_telefonos">
                                            <thead>
                                                <th>Telefonos</th>
                                                <th><i class="fa fa-cog"></i></th>
                                            </thead>
                                        </table>
                                    </div>
                                </div> -->
                                <div class="col-md-6 col-xs-12">
                                    <div class="text-center cabecera_prueba">
                                        <h4><i class="fa fa-users"></i>&nbsp;Redes Sociales&nbsp;&nbsp;<!-- <button type="button" class="btn btn-primary"  onclick="mostrar_agregar_red()"><i class="fa fa-plus-circle"></i>&nbsp;Agregar</button> --></h4>
                                    </div>
                                    <br>
                                    <div class="table-responsive">
                                        <table class="table table-bordered" id="tabla_redes">
                                            <thead>
                                                <th>Red Social</th>
                                                <th>Icono</th>
                                                <th><i class="fa fa-cog"></i></th>
                                            </thead>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <input type="hidden" name="txt_id" id="txt_id" value="">
                        </div>
                    </form>
                    </div>
                </div>
  
            </div>
        </div>
    </section>
</div>

<div class="modal fade" id="modal_agregar_correo">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">JVG Inmobiliaria - Agregar Correo</h4>
            </div>
            <div class="modal-body">
                <div class="input-group">
                    <span class="input-group-addon"><i class="fa fa-envelope"></i></span>
                    <input type="text" class="form-control" placeholder="Nuevo Correo" id="nuevo_correo">
                    
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" onclick="nuevo_correo()"><i class="fa fa-save"></i>&nbsp; Guardar</button>
                <button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-times"></i>&nbsp;Cerrar</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modal_editar_correo">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">JVG Inmobiliaria - Editar Correo</h4>
            </div>
            <div class="modal-body">
                <div class="input-group">
                    <span class="input-group-addon"><i class="fa fa-envelope"></i></span>
                    <input type="text" class="form-control" placeholder="Correo" id="editar_correo">
                    <input type="hidden" name="" id="id_correo_editar">
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" onclick="editar_correo()"><i class="fa fa-save"></i>&nbsp; Guardar</button>
                <button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-times"></i>&nbsp;Cerrar</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modal_agregar_telefono">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">JVG Inmobiliaria - Agregar Telefono</h4>
            </div>
            <div class="modal-body">
                <div class="input-group">
                    <span class="input-group-addon"><i class="fa fa-mobile"></i></span>
                    <input type="text" class="form-control" placeholder="Nuevo Telefono" id="nuevo_telefono">
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" onclick="nuevo_telefono()"><i class="fa fa-save"></i>&nbsp; Guardar</button>
                <button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-times"></i>&nbsp;Cerrar</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modal_editar_telefono">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">JVG Inmobiliaria - Editar Telefono</h4>
            </div>
            <div class="modal-body">
                <div class="input-group">
                    <span class="input-group-addon"><i class="fa fa-mobile"></i></span>
                    <input type="text" class="form-control" placeholder="Telefono" id="editar_telefono">
                    <input type="hidden" name="" id="id_telefono_editar">
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" onclick="editar_telefono()"><i class="fa fa-save"></i>&nbsp; Guardar</button>
                <button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-times"></i>&nbsp;Cerrar</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modal_agregar_red">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">JVG Inmobiliaria - Agregar Red Social</h4>
            </div>
            <div class="modal-body">
                <!-- <div class="form-group">
                    <select name="" id="id_red_nueva" class="form-control">
                        <option value="0">Seleccione</option>  
                        <option value="az-facebook2_facebook">Facebook</option>
                        <option value="az-instagram_instagram">Instagram</option>
                        <option value="az-youtube_youtube">YouTube</option>
                        <option value="az-twitter_twitter">Twitter</option>
                    </select>
                </div> -->
                <div class="input-group">
                    <span class="input-group-addon"><i class="fa fa-link"></i></span>
                    <input type="text" class="form-control" placeholder="Link red social: https://www.facebook.com/" id="nuevo_link">
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" onclick="nuevo_red()"><i class="fa fa-save"></i>&nbsp; Guardar</button>
                <button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-times"></i>&nbsp;Cerrar</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modal_editar_red">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">JVG Inmobiliaria - Editar Red Social</h4>
            </div>
            <div class="modal-body">
                <!-- <div class="form-group">
                    <select name="" id="id_red_editar_cmb" class="form-control">
                        <option value="0">Seleccione</option>  
                        <option value="az-facebook2_facebook">Facebook</option>
                        <option value="az-instagram_instagram">Instagram</option>
                        <option value="az-youtube_youtube">YouTube</option>
                        <option value="az-twitter_twitter">Twitter</option>
                    </select>
                </div> -->
                <div class="input-group">
                    <span class="input-group-addon"><i class="fa fa-link"></i></span>
                    <input type="text" class="form-control" placeholder="Link red social: https://www.facebook.com/" id="editar_link">
                    <input type="hidden" name="" id="id_red_editar">
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" onclick="editar_red()"><i class="fa fa-save"></i>&nbsp; Guardar</button>
                <button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-times"></i>&nbsp;Cerrar</button>
            </div>
        </div>
    </div>
</div>



<script src="<?php echo base_url();?>public/administrador/js/nosotros.js"></script>