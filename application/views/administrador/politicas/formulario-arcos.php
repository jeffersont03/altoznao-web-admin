<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <ol class="breadcrumb breadcum">
      <li><a href="<?php echo base_url() ?>">Inicio</a></li>
      <li>Contenido Legal</a></li>
      <li class="active">Enlace 4</li>
    </ol>
    <!-- Main content -->
    <section class="content">
        <!-- Default box -->
        <ul class="nav nav-tabs">
            <li role="presentation"><a href="<?php echo base_url() ?>admin/politicas/enlace-1">Enlace 1</a></li>
            <li role="presentation"><a href="<?php echo base_url() ?>admin/politicas/enlace-2">Enlace 2</a></li>
            <li role="presentation"><a href="<?php echo base_url() ?>admin/politicas/enlace-3">Enlace 3</a></li>
            <li role="presentation" class="active"><a href="">Enlace 4</a></li>
            <li role="presentation"><a href="<?php echo base_url() ?>admin/politicas/enlace-5">Enlace 5</a></li>
            <li role="presentation"><a href="<?php echo base_url() ?>admin/politicas/subir-pdf">PDF</a></li>
        </ul>
        <div class="box box-solid">
            <div class="box-body">
                <div class="row">
                    <div class="col-md-12">
                        <form method="post" enctype="multipart/form-data" id="form_privacidad" autocomplete="nope">
                            <div class="modal-body">
                                <div class="col-xs-12 col-md-12 col-lg-12" style="padding:0">
                                    <div class="form-group col-xs-12">
                                        <label for=""><span style="color: red">*</span> Titulo de Enlace 4:</label>
                                        <input type="text" class="form-control" id="titulo_politica" placeholder="Enlace 4">
                                    </div>
                                    <div class="form-group col-xs-12">
                                        <label><span style="color: red">*</span> Archivo:<br><small>(Formato PDF/DOC/DOCX)</small></label>
                                        <input type="file"  accept="application/pdf, application/msword, application/vnd.openxmlformats-officedocument.wordprocessingml.document" class="form-control" name="file" id="file_politicas">
                                        <div style="clear:both">
                                            <iframe id="viewer" frameborder="0" scrolling="no" width="400" height="600"></iframe>
                                        </div>
                                    </div>
                                    <div class="col-xs-12 text-center" style="margin-top: 2%;">
                                        <input type="hidden" id="p_id" value="5">
                                        <button type="button" class="btn btn-success" onclick="actualizar_datos_politicas()"><i class="fa fa-save"></i>&nbsp;Actualizar</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>


<script src="<?php echo base_url();?>public/administrador/js/politicas-arcos.js"></script>