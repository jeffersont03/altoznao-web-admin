<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <ol class="breadcrumb breadcum">
      <li><a href="<?php echo base_url() ?>">Inicio</a></li>
      <li>Contenido Legal</a></li>
      <li class="active">Subida de PDF</li>
    </ol>
    <!-- Main content -->
    <section class="content">
        <!-- Default box -->
        <ul class="nav nav-tabs">
            <li role="presentation"><a href="<?php echo base_url() ?>admin/politicas/enlace-1">Enlace 1</a></li>
            <li role="presentation"><a href="<?php echo base_url() ?>admin/politicas/enlace-2">Enlace 2</a></li>
            <li role="presentation"><a href="<?php echo base_url() ?>admin/politicas/enlace-3">Enlace 3</a></li>
            <li role="presentation"><a href="<?php echo base_url() ?>admin/politicas/enlace-4">Enlace 4</a></li>
            <li role="presentation"><a href="<?php echo base_url() ?>admin/politicas/enlace-5">Enlace 5</a></li>
            <li role="presentation" class="active"><a href="#">PDF</a></li>
        </ul>
        <div class="box box-solid">
            <div class="box-body">
                <div class="row">
                    <div class="col-md-12">
                        <form method="post" enctype="multipart/form-data" id="form_privacidad" autocomplete="nope">
                            <?php if(isset($pdfs) && count($pdfs) > 0): ?>
                            <div class="panel panel-default">
                                <?php foreach($pdfs as $pdf) { 
                                    $pdf->titulo = explode(": ",$pdf->titulo)[1];
                                ?>
                                <div class="panel-heading">PDF <?= $pdf->id - 5; ?></div>
                                <div class="panel-body" id="<?= 'pdf-' . $pdf->id; ?>">
                                    <div class="col-xs-12 col-md-12 col-lg-12" style="padding:0">
                                        <div class="form-group col-xs-6">
                                            <label><span style="color: red">*</span> Archivo:<br><small>(Formato PDF/DOC/DOCX)</small></label>
                                            <input type="file"  accept="application/pdf, application/msword, application/vnd.openxmlformats-officedocument.wordprocessingml.document" class="form-control file_pdf" name="file" id="file_pdf">
                                        </div>
                                        <div class="form-group col-xs-6">
                                            <label for=""><span style="color: red">*</span> Nombre Archivo:</label>
                                            <br>&nbsp;
                                            <input type="text" class="form-control titulo_pdf" value="<?= $pdf->titulo ?>">
                                        </div>
                                        <div class="form-group col-xs-12">
                                            <label for="">Ruta:</label>
                                            <input type="text" class="form-control ruta_pdf" value="<?= base_url() . 'public/frontend/img/pdf/' . $pdf->contenido?>" readonly>
                                        </div>
                                        <div class="col-xs-12 text-center" style="margin-top: 2%;">
                                            <button type="button" class="btn btn-success" onclick="actualizar_datos_pdf(<?= $pdf->id ?>)"><i class="fa fa-save"></i>&nbsp;Actualizar</button>
                                        </div>
                                    </div>
                                </div>
                                <?php } ?>
                            </div>
                            <?php endif; ?>

                            <div class="panel panel-default">
                                <div class="panel-heading">Nuevo</div>
                                <div class="panel-body" id="pdf-nuevo">
                                    <div class="col-xs-12 col-md-12 col-lg-12" style="padding:0">
                                        <div class="form-group col-xs-6">
                                            <label><span style="color: red">*</span> Archivo:<br><small>(Formato PDF)</small></label>
                                            <input type="file"  accept="application/pdf" class="form-control file_pdf" name="file" id="file_pdf">
                                        </div>
                                        <div class="form-group col-xs-6">
                                            <label for=""><span style="color: red">*</span> Nombre Archivo:</label>
                                            <br>&nbsp;
                                            <input type="text" class="form-control titulo_pdf" placeholder="Nombre de mi archivo PDF">
                                        </div>
                                        <div class="form-group col-xs-12">
                                            <label for="">Ruta:</label>                                            
                                            <input type="text" class="form-control ruta_pdf" readonly>
                                        </div>
                                        <div class="col-xs-12 text-center" style="margin-top: 2%;">
                                            <button type="button" class="btn btn-success" onclick="actualizar_datos_pdf()"><i class="fa fa-save"></i>&nbsp;Guardar</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>


<script src="<?php echo base_url();?>public/administrador/js/pdfs.js"></script>