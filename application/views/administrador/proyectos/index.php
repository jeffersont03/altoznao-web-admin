<style>
    .cabecera_prueba {
        background: #f5f5f5;
        padding: 1% 0%;
    }
</style>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <ol class="breadcrumb breadcum">
      <li><a href="<?php echo base_url() ?>">Inicio</a></li>
      <li class="active">Proyectos</li>
    </ol>
    <section class="content-header">
        <h1>
        Proyectos
        <small></small>
        </h1>
    </section>
    <!-- Main content -->
    <section class="content">
        <!-- Default box -->
        <div class="box box-solid">
            <div class="box-body">
                <div class="row" id="lista_proyectos" style="display: block;">
                    <div class="col-md-12">
                        <div class="row">
                            <div class="col-xs-12 text-right">
                                <button type="button" class="btn btn-info" onclick="retornar_nuevo()"> <i class="fa fa-plus"></i>&nbsp;Agregar Nuevo Proyecto</button>
                            </div>
                        </div>
                        <hr>
                        <div class="row">
                            <div class="col-xs-12">
                                 <div class="table-responsive">
                                    <table class="table table-bordered" id="tabla_proyectos">
                                        <thead>
                                            <th>Opciones</th>
                                            <th>Nombre</th>
                                            <th>Ubicación</th>
                                            <th>Estado</th>
                                            <th>Visual</th>
                                            <th>URL</th>
                                            <th>Imagen</th>
                                        </thead>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>


                <!-- nuevos proyectos -->
                <div class="row" id="nuevo_proyecto" style="display: none;">
                    <div class="col-lg-6 col-md-6 col-xs-12">
                        <div class="form-group col-lg-6 col-md-6 col-sm-12 col-xs-12">
                            <label for=""><span style="color: red">*</span> Nombre</label>
                            <input type="text" class="form-control" placeholder="Nombre del Proyecto" id="nuevo_nombre">
                        </div>
                        <div class="form-group col-lg-6 col-md-6 col-sm-12 col-xs-12">
                            <label>Estado del Proyecto:</label>
                            <input type="text" class="form-control" placeholder="Pre Venta, En Venta, ..." id="nuevo_estado">
                        </div>
                        <div class="form-group col-xs-12">
                            <label for="txt_descripcion">Descripcion:</label>
                            <textarea style="height:80px;width:100%;resize:none;" class="form-control" name="txt_descripcion" id="txt_descripcion"></textarea>
                        </div>
                        <div class="form-group col-lg-6 col-md-6 col-sm-12 col-xs-12">
                            <label>Etapas:</label>
                            <div class="input-group">                               
                                <span class="input-group-addon"><i class="fa fa-commenting"></i></span>
                                <input type="text" class="form-control" id="nueva_etapas" maxlength="200">
                            </div>
                        </div>
                        <div class="form-group col-lg-6 col-md-6 col-sm-12 col-xs-12">
                            <label><span style="color: red">*</span> Tipo de Proyecto:</label>
                            <select name="" id="nuevo_tipo_proyecto" class="form-control">
                                <option value="0">Seleccione</option>
                                <option value="p_venta">Proyectos en Venta</option>
                                <option value="p_proximo">Próximos Proyectos</option>
                                <option value="p_entregado">Proyectos Entregados</option>
                            </select>
                        </div>
                        <div class="form-group col-lg-6 col-md-6 col-sm-12 col-xs-12">
                            <label><span style="color: red">*</span> Departamento:</label>
                            <div class="input-group">                               
                                <span class="input-group-addon"><i class="fa fa-map-marker"></i></span>
                                <input type="text" class="form-control" id="nuevo_departamento" maxlength="200">
                            </div>
                        </div>
                        <div class="form-group col-lg-6 col-md-6 col-sm-12 col-xs-12">
                            <label><span style="color: red">*</span> Correo:</label>
                            <div class="input-group">                               
                                <span class="input-group-addon"><i class="fa fa-envelope"></i></span>
                                <input type="text" class="form-control" id="nuevo_correo" maxlength="200">
                            </div>
                        </div>
                        <div class="form-group col-xs-12">
                            <label for="txt_comentarios">Comentarios:</label>
                            <input type="text" class="form-control" id="txt_comentarios">
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-6 col-xs-12">
                        <div class="form-group col-xs-12">
                            <div class="relative">
                                <label><span style="color: red">*</span> Imagen de fondo:<br><small>(Tamaño recomendado menos 1980 x 800px)</small></label>
                                <div class="bg-form-imagen" style="height: 300px; text-align: center;background: #f5f5f5;">
                                    <img width="auto" height="auto" id="imagenmuestra_principal" style="max-width: 100%;max-height: 100%;">
                                </div>
                                <input type="file" class="form-control" name="imagen" id="imagen_principal">
                            </div>
                        </div>
                        <div class="form-group col-lg-6 col-md-6 col-sm-12 col-xs-12">
                            <div class="relative">
                                <label><span style="color: red">*</span> Logo:<br><small>(Tamaño recomendado máximo de 300 x 300px en formato png)</small></label>
                                <div class="bg-form-imagen" style="text-align: center;background: #f5f5f5;">
                                    <img width="auto" height="auto" id="imagenmuestra_logo" style="max-width: 100%;max-height: 100%;">
                                </div>
                                <input type="file" class="form-control" name="imagen" id="imagen_logo">
                            </div>
                        </div>
                        <div class="form-group col-lg-6 col-md-6 col-sm-12 col-xs-12">
                            <div class="relative">
                                <label><span style="color: red">*</span> Imagen de fachada:<br><small>(Tamaño recomendado 455 x 560px)</small></label>
                                <div class="bg-form-imagen" style="text-align: center;background: #f5f5f5;">
                                    <img width="auto" height="auto" id="imagenmuestra_fachada" style="max-width: 100%;max-height: 100%;">
                                </div>
                                <input type="file" class="form-control" name="imagen" id="imagen_fachada">
                            </div>
                        </div>
                        
                    </div>
                    <div class="col-xs-12">
                        <div class="col-lg-4 col-md-4 col-xs-12">
                            <div class="form-group col-xs-12">
                                <label for="">Lista 1</label>
                                <textarea id="txt_lista_uno"></textarea>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-4 col-xs-12">
                            <div class="form-group col-xs-12">
                                <label for="">Lista 2</label>
                                <textarea id="txt_lista_dos"></textarea>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-4 col-xs-12">
                            <div class="form-group col-xs-12">
                                <label for="">Lista 3</label>
                                <textarea id="txt_lista_tres"></textarea>
                            </div>
                        </div>
                    </div>
                    <div class="row" style="margin-top: 2%;">
                        <div class="col-xs-12 text-center">
                            <button type="button" class="btn btn-primary" onclick="nuevo_proyecto()"><i class="fa fa-save"></i>&nbsp;Guardar</button>
                            <button type="button" class="btn btn-default" onclick="retornar_principal()"><i class="fa fa-arrow-left" aria-hidden="true"></i>&nbsp;Regresar</button>
                        </div>
                    </div>
                </div>
                

                <!-- edicion de proyecto -->
                <div class="row" id="edicion_proyecto" style="display: none;padding: 2%;">
                    <div class="row">
                        <div class="col-xs-12 text-center cabecera_prueba">
                            <h3>Proyecto <label id="npam"></label> </h3>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-6 col-md-6 col-xs-12">
                            <div class="form-group col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                <label for=""><span style="color: red">*</span> Nombre</label>
                                <input type="text" class="form-control" placeholder="Nombre del Proyecto" id="editar_nuevo_nombre">
                                <input type="hidden" id="editar_id_proyecto">
                            </div>
                            <div class="form-group col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                <label><span style="color: red">*</span> Estado del Proyecto:</label>
                                <input type="text" class="form-control" placeholder="Pre Venta, En Venta, ..." id="editar_nuevo_estado">
                            </div>
                            <div class="form-group col-xs-12">
                                <label for="txt_descripcion">Descripcion:</label>
                                <textarea id="editar_descripcion"></textarea>
                            </div>
                           <div class="form-group col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                <label><span style="color: red">*</span> Etapas:</label>
                                <div class="input-group">                               
                                    <span class="input-group-addon"><i class="fa fa-commenting"></i></span>
                                    <input type="text" class="form-control" id="nueva_etapas_editar" maxlength="200">
                                </div>
                            </div>
                            <div class="form-group col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                <label><span style="color: red">*</span> Tipo de Proyecto:</label>
                                <select name="" id="nuevo_tipo_proyecto_editar" class="form-control">
                                    <option value="0">Seleccione</option>
                                    <option value="p_venta">Proyectos en Venta</option>
                                    <option value="p_proximo">Próximos Proyectos</option>
                                    <option value="p_entregado">Proyectos Entregados</option>
                                </select>
                            </div>
                            <div class="form-group col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                <label><span style="color: red">*</span> Departamento:</label>
                                <div class="input-group">                               
                                    <span class="input-group-addon"><i class="fa fa-map-marker"></i></span>
                                    <input type="text" class="form-control" id="nuevo_departamento_editar" maxlength="200">
                                </div>
                            </div>
                            <div class="form-group col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                <label><span style="color: red">*</span> Correo:</label>
                                <div class="input-group">                               
                                    <span class="input-group-addon"><i class="fa fa-envelope"></i></span>
                                    <input type="text" class="form-control" id="editar_nuevo_correo" maxlength="200">
                                </div>
                            </div>
                             <div class="form-group col-xs-12">
                                <label for="editar_txt_comentarios">Comentarios:</label>
                                <input type="text" class="form-control" id="editar_txt_comentarios">
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6 col-xs-12">
                            <div class="form-group col-xs-12">
                                <div class="relative">
                                    <label><span style="color: red">*</span> Imagen de fondo:<br><small>(Tamaño recomendado menos 1980 x 800px)</small></label>
                                    <div class="bg-form-imagen" style="height: 300px; text-align: center;background: #f5f5f5;">
                                        <img width="auto" height="auto" id="editar_imagenmuestra_principal" style="max-width: 100%;max-height: 100%;">
                                    </div>
                                    <input type="file" class="form-control" name="imagen" id="editar_imagen_principal">
                                </div>
                            </div>
                            <div class="form-group col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                <div class="relative">
                                    <label><span style="color: red">*</span> Logo:<br><small>(Tamaño recomendado máximo de 300 x 300px en formato png)</small></label>
                                    <div class="bg-form-imagen" style="text-align: center;background: #f5f5f5;">
                                        <img width="auto" height="auto" id="editar_imagenmuestra_logo" style="max-width: 100%;max-height: 100%;">
                                    </div>
                                    <input type="file" class="form-control" name="imagen" id="editar_imagen_logo">
                                </div>
                            </div>
                            <div class="form-group col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                <div class="relative">
                                    <label><span style="color: red">*</span> Imagen de fachada:<br><small>(Tamaño recomendado 455 x 560px)</small></label>
                                    <div class="bg-form-imagen" style="text-align: center;background: #f5f5f5;">
                                        <img width="auto" height="auto" id="editar_imagenmuestra_fachada" style="max-width: 100%;max-height: 100%;">
                                    </div>
                                    <input type="file" class="form-control" name="imagen" id="editar_imagen_fachada">
                                </div>
                            </div>                      
                        </div>
                        
                    </div>
                    <div class="row">
                        <div class="col-xs-12 col-md-4 col-lg-4">
                            <div class="form-group col-xs-12">
                                <label for=""><span style="color: red;">*</span> Lista 1</label>
                                <textarea id="txt_item_uno_editar"></textarea>
                            </div> 
                        </div>
                        <div class="col-xs-12 col-md-4 col-lg-4">
                            <div class="form-group col-xs-12">
                                <label for=""><span style="color: red;">*</span> Lista 2</label>
                                <textarea id="txt_item_dos_editar"></textarea>
                            </div> 
                        </div>
                        <div class="col-xs-12 col-md-4 col-lg-4">
                            <div class="form-group col-xs-12">
                                <label for=""><span style="color: red;">*</span> Lista 3</label>
                                <textarea id="txt_item_tres_editar"></textarea>
                            </div> 
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12 text-center"><button class="btn btn-success" onclick="editar_proyecto()"><i class="fa fa-floppy-o"></i>&nbsp;&nbsp;Actualizar</button>&nbsp;<button type="button" class="btn btn-default" onclick="retornar_principal()"><i class="fa fa-arrow-left" aria-hidden="true"></i>&nbsp;Regresar</button></div>
                    </div>
                    <hr>
                    <div class="row">                        
                        <div class="col-lg-6 col-md-6 col-xs-12" style="padding: 2%;">
                            <div class="form-group col-xs-12">
                                <div class="row">
                                    <div class="col-xs-12 text-center cabecera_prueba">
                                        <h4><i class="fa fa-camera-retro"></i>&nbsp;Galerias&nbsp;&nbsp;<button class="btn btn-primary" onclick="mostrar_modal_galeria()"><i class="fa fa-plus-circle"></i>&nbsp;Agregar</button></h4>
                                    </div>
                                </div>
                                <table class="table table-bordered" id="tabla_galerias">
                                    <thead>
                                        <th>Imagen</th>
                                        <th>Título</th>
                                        <th>Operaciones</th>
                                    </thead>
                                </table>
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6 col-xs-12" style="padding: 2%;">
                            <div class="form-group col-xs-12">
                                <div class="row">
                                    <div class="col-xs-12 text-center cabecera_prueba">
                                        <h4><i class="fa fa-object-ungroup"></i>&nbsp;Planos&nbsp;&nbsp;<button class="btn btn-primary" onclick="mostrar_modal_plano()"><i class="fa fa-plus-circle"></i>&nbsp;Agregar</button></h4>
                                    </div>
                                </div>
                                <table class="table table-bordered" id="tabla_planos">
                                    <thead>
                                        <th>Imagen</th>
                                        <th>Título</th>
                                        <th>Descripción</th>
                                        <th>Operaciones</th>
                                    </thead>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>

<div class="modal fade" id="modal_registro_exitoso">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Altozano</h4>
            </div>
            <div class="modal-body">
                <!-- <div class="alert alert-success">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    <strong>Registro correcto!</strong> El proyecto se registro con éxito.
                </div> -->
                <div class="row">
                    <div class="col-xs-12 text-center">
                        <h4><b>Proyecto registrado correctamente!!</b></h4>
                        <p>Ustede puede registrar ahora los planos o galerías del nuevo proyecto, o despues puede hacerlo. </p>
                        <input type="hidden" id="id_proyecto_temporal">
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-6">
                        <div class="panel panel-default">
                            <div class="panel-heading">Galerías</div>
                            <div class="panel-body">
                                <img class="img-thumbnail" id="img_proto_galeria" style="height: 180px; object-fit: cover;width: 100%;">
                                <br>
                                <div class="row">
                                    <div class="col-xs-12 text-center"><button class="btn btn-default" onclick="retornar_editar_especial()">Registrar!!</button></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-6">
                       <div class="panel panel-default">
                            <div class="panel-heading">Planos</div>
                            <div class="panel-body">
                                <img class="img-thumbnail" id="img_proto_planos" style="height: 180px; object-fit: cover;width: 100%;">
                                <br>
                                <div class="row">
                                    <div class="col-xs-12 text-center"><button class="btn btn-default" onclick="retornar_editar_especial()">Registrar!!</button></div>
                                </div>
                            </div>
                        </div> 
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <!-- <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary">Save changes</button> -->
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modal_nueva_galeria">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Altozano - Nueva Galería</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="form-group">
                            <label for="">Nombre</label>
                            <input type="text" class="form-control" id="nombre_galeria">
                        </div>
                        <div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="relative">
                                <label><span style="color: red">*</span> Imagen:<br><small>(Tamaño recomendado máximo de 1000 x 800px)</small></label>
                                <div class="bg-form-imagen" style="text-align: center;background: #f5f5f5;">
                                    <img width="auto" height="auto" id="imagengaleria_nueva" style="max-width: 100%;max-height: 100%;">
                                </div>
                                <input type="file" class="form-control" name="imagen" id="imagen_galeria">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer text-center">
                <div class="text-center">
                    <button type="button" class="btn btn-primary" onclick="nueva_galeria()">Guardar</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cerar</button>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modal_editar_galeria">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Altozano - Editar Galería</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="form-group">
                            <label for="">Nombre</label>
                            <input type="text" class="form-control" id="nombre_galeria_editar">
                            <input type="hidden" id="id_galeria_editar">
                        </div>
                        <div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="relative">
                                <label><span style="color: red">*</span> Imagen:<br><small>(Tamaño recomendado máximo de 1000 x 800px)</small></label>
                                <div class="bg-form-imagen" style="text-align: center;background: #f5f5f5;">
                                    <img width="auto" height="auto" id="imagengaleria_editar" style="max-width: 100%;max-height: 100%;">
                                </div>
                                <input type="file" class="form-control" name="imagen" id="imagen_galeria_editar">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer text-center">
                <div class="text-center">
                    <button type="button" class="btn btn-primary" onclick="editar_galeria()">Guardar</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cerar</button>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modal_nuevo_plano">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Altozano - Nuevo Plano</h4>
            </div>
            <div class="modal-body">
                <div class="col-xs-12">
                        <div class="form-group">
                            <label for="">Nombre</label>
                            <input type="text" class="form-control" id="nombre_plano">
                        </div>
                        <div class="form-group col-lg-6 col-md-6 col-sm-12 col-xs-12">
                            <div class="relative">
                                <label><span style="color: red">*</span> Imagen:<br><small>(Tamaño recomendado máximo de 1000 x 700px)</small></label>
                                <div class="bg-form-imagen" style="text-align: center;background: #f5f5f5;">
                                    <img width="auto" height="auto" id="imagenplano_nueva" style="max-width: 100%;max-height: 100%;">
                                </div>
                                <input type="file" class="form-control" name="imagen" id="imagen_plano">
                            </div>
                        </div>
                        <div class="form-group col-lg-6 col-md-6 col-sm-12 col-xs-12">
                            <label for="txt_descripcion">Descripcion:</label>
                            <textarea style="height:80px;width:100%;resize:none;" class="form-control" name="txt_descripcion" id="galeria_txt_descripcion"></textarea>
                        </div>
                    </div>
            </div>
            <div class="modal-footer">
                <div class="text-center">
                    <button type="button" class="btn btn-primary" onclick="nuevo_plano()">Guardar</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cerar</button>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modal_editar_plano">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Altozano - Editar Plano</h4>
            </div>
            <div class="modal-body">
                <div class="col-xs-12">
                        <div class="form-group">
                            <label for="">Nombre</label>
                            <input type="text" class="form-control" id="nombre_plano_editar">
                            <input type="hidden" name="" id="id_plano_editar">
                        </div>
                        <div class="form-group col-lg-6 col-md-6 col-sm-12 col-xs-12">
                            <div class="relative">
                                <label><span style="color: red">*</span> Imagen:<br><small>(Tamaño recomendado máximo de 1000 x 700px)</small></label>
                                <div class="bg-form-imagen" style="text-align: center;background: #f5f5f5;">
                                    <img width="auto" height="auto" id="imagenplano_editar" style="max-width: 100%;max-height: 100%;">
                                </div>
                                <input type="file" class="form-control" name="imagen" id="imagen_plano_editar">
                            </div>
                        </div>
                        <div class="form-group col-lg-6 col-md-6 col-sm-12 col-xs-12">
                            <label for="txt_descripcion">Descripcion:</label>
                            <textarea style="height:80px;width:100%;resize:none;" class="form-control" name="txt_descripcion" id="plano_txt_descripcion_editar"></textarea>
                        </div>
                    </div>
            </div>
            <div class="modal-footer">
                <div class="text-center">
                    <button type="button" class="btn btn-primary" onclick="editar_plano()">Guardar</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cerar</button>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="<?php echo base_url();?>public/administrador/js/proyectos.js"></script>