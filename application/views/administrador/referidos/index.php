<style>
    .cabecera_prueba {
        background: #f5f5f5;
        padding: 2px 0%;
    }

    .card-referidos {
        background: #f18a06;
        width: 64px;
        height: 64px;
        line-height: 64px;
        display: inline-block;
        border-radius: 50%;
        margin: 13px 0;
        color: white;
        font-size: 21px;
    }
</style>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <!-- <ol class="breadcrumb breadcum">
      <li><a href="<?php echo base_url() ?>">Inicio</a></li>
      <li class="active">Página de Inicio</li>
    </ol>
    <section class="content-header">
        <h1>
        Página de Inicio
        <small></small>
        </h1>
    </section> -->
    <!-- Main content -->
    <section class="content">
        <!-- Default box -->
        <div class="box box-solid">
            <div class="box-body">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="text-center cabecera_prueba">
                            <h3>REFERIDOS</h3>
                        </div>
                    </div>
                </div>
                <br>
                <div class="row">
                    <div class="col-lg-6 col-md-6 col-xs-12 col-lg-offset-3 col-md-offset-3">
                        <div class="form-group col-xs-12">
                            <label for=""><span style="color: red;">*</span> Título</label>
                            <textarea style="height:40px;width:100%;resize:none;" class="form-control" name="txt_titulo_referido" id="txt_titulo_referido"></textarea>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-6 col-md-6 col-xs-12 col-lg-offset-3 col-md-offset-3">
                        <div class="form-group col-xs-12">
                            <label for="imagen_card"><span style="color: red;">*</span> Tarjeta</label>
                            <div class="bg-form-imagen" style="text-align: center;background: #f5f5f5;">
                                <img width="auto" height="auto" id="imagen_card" style="max-width: 100%;height: 300px;">
                            </div>
                            <input type="file" class="form-control" name="imagen" id="img_card">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-4 col-md-4 col-xs-12 text-center">
                        <span class="card-referidos">1</span>
                        <div class="form-group col-xs-12">
                            <label for="txt_item_uno">Texto:</label>
                            <textarea rows="5" class="form-control" name="txt_item_uno" id="txt_item_uno"></textarea>
                        </div>
                        <div class="form-group col-xs-12">
                            <label for="imagen_item_uno">Imagen:</label>
                            <div class="bg-form-imagen" style="text-align: center;background: #f5f5f5;">
                                <img width="auto" height="auto" id="imagen_item_uno" style="max-width: 100%;height: 300px;">
                            </div>
                            <input type="file" class="form-control" name="imagen" id="img_item_uno">
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-4 col-xs-12 text-center">
                        <span class="card-referidos">2</span>
                        <div class="form-group col-xs-12">
                            <label for="txt_item_dos">Texto:</label>
                            <textarea rows="5" class="form-control" name="txt_item_dos" id="txt_item_dos"></textarea>
                        </div>
                        <div class="form-group col-xs-12">
                            <label for="imagen_item_dos">Imagen:</label>
                            <div class="bg-form-imagen" style="text-align: center;background: #f5f5f5;">
                                <img width="auto" height="auto" id="imagen_item_dos" style="max-width: 100%;height: 300px;">
                            </div>
                            <input type="file" class="form-control" name="imagen" id="img_item_dos">
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-4 col-xs-12 text-center">
                        <span class="card-referidos">3</span>
                        <div class="form-group col-xs-12">
                            <label for="txt_item_tres">Texto:</label>
                            <textarea rows="5" class="form-control" name="txt_item_tres" id="txt_item_tres"></textarea>
                        </div>
                        <div class="form-group col-xs-12">
                            <label for="imagen_item_tres">Imagen:</label>
                            <div class="bg-form-imagen" style="text-align: center;background: #f5f5f5;">
                                <img width="auto" height="auto" id="imagen_item_tres" style="max-width: 100%;height: 300px;">
                            </div>
                            <input type="file" class="form-control" name="imagen" id="img_item_tres">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-6 col-md-6 col-xs-12 col-lg-offset-3 col-md-offset-3">
                        <div class="form-group col-xs-12">
                            <label for=""><span style="color: red;">*</span> Descripción</label>
                            <textarea style="height:40px;width:100%;resize:none;" class="form-control" name="txt_descripcion_referido" id="txt_descripcion_referido"></textarea>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-6 col-md-6 col-xs-12 col-lg-offset-3 col-md-offset-3">
                        <div class="form-group col-xs-12">
                            <label for=""><span style="color: red;">*</span> Correo de destino para diferidos</label>
                            <input type="text" class="form-control" id="correo_referido">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12 text-center"><button class="btn btn-success" type="button" onclick="actualizar_referidos()"><i class="fa fa-save"></i>&nbsp;Actualizar</button></div>
                </div>
            </div>
        </div>
    </section>
</div>

<script src="<?php echo base_url();?>public/administrador/js/referidos.js"></script>