<style>
    .cabecera_prueba {
        background: #f5f5f5;
    }
</style>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <ol class="breadcrumb breadcum">
      <li><a href="<?php echo base_url() ?>">Inicio</a></li>
      <li class="active">Banners</li>
    </ol>
    <section class="content-header">
        <h1>
        Banners
        <small></small>
        </h1>
    </section>
    <!-- Main content -->
    <section class="content">
        <!-- Default box -->
        <div class="box box-solid">
            <div class="box-body">
                <div class="row">
                    <div class="col-lg-6 col-md-6 col-xs-12 col-lg-offset-3">
                        <div class="row">
                            <div class="col-xs-12 text-center cabecera_prueba">
                                <h4><i class="fa fa-picture-o"></i>&nbsp;Banners&nbsp;&nbsp;<button class="btn btn-primary" data-toggle="modal" data-target="#modal_nuevo_banner"><i class="fa fa-plus-circle"></i>&nbsp;Agregar</button></h4>
                            </div>
                        </div>
                        <div class="table-responsive">
                            <table class="table table-bordered" id="tabla_sliders">
                                <thead>
                                    <th>Operaciones</th>
                                    <th>Imagen</th>
                                    <th>Texto</th>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>


<div class="modal fade" id="modal_nuevo_banner">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Altozano - Agregar Banner</h4>
            </div>
            <div class="modal-body">
                <div class="form-group col-lg-6 col-md-6 col-sm-12 col-xs-12">
                    <div class="relative">
                        <label><span style="color: red">*</span> Imagen:<br><small>Tamaño recomendado: 1980 x 970px</small></label>
                        <div class="bg-form-imagen" style="text-align: center;background: #f5f5f5;">
                            <img width="auto" height="auto" id="imagenbanner_nueva" style="max-width: 100%;max-height: 100%;">
                        </div>
                        <input type="file" class="form-control" name="imagen" id="imagen_banners">
                    </div>
                </div>
                <div class="form-group col-lg-6 col-md-6 col-sm-12 col-xs-12">
                    <label for="txt_slider">Descripcion:</label>
                    <textarea style="height:80px;width:100%;resize:none;" class="form-control" name="txt_slider" id="txt_slider"></textarea>
                </div>
            </div>
            <div class="modal-footer">
                <hr>
                <div class="text center">
                    <button type="button" class="btn btn-primary" onclick="nuevo_banner()"><i class="fa fa-save"></i>&nbsp;Guardar</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-times"></i>&nbsp;Cerrar</button>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modal_editar_banner">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Altozano - Editar Banner</h4>
            </div>
            <div class="modal-body">
                <div class="form-group col-lg-6 col-md-6 col-sm-12 col-xs-12">
                    <input type="hidden" id="id_banner_editar">
                    <div class="relative">
                        <label><span style="color: red">*</span> Imagen:<br><small>Tamaño recomendado: 1980 x 970px</small></label>
                        <div class="bg-form-imagen" style="text-align: center;background: #f5f5f5;">
                            <img width="auto" height="auto" id="imagenbanner_editar" style="max-width: 100%;max-height: 100%;">
                        </div>
                        <input type="file" class="form-control" name="imagen" id="imagen_banners_editar">
                    </div>
                </div>
                <div class="form-group col-lg-6 col-md-6 col-sm-12 col-xs-12">
                    <label for="txt_slider_editar">Descripcion:</label>
                    <textarea style="height:80px;width:100%;resize:none;" class="form-control" name="txt_slider_editar" id="txt_slider_editar"></textarea>
                </div>
            </div>
            <div class="modal-footer">
                <div class="text center">
                    <button type="button" class="btn btn-primary" onclick="editar_banner()">Editar</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                </div>
            </div>
        </div>
    </div>
</div>

<script src="<?php echo base_url();?>public/administrador/js/slider.js"></script>