<style>
    .cabecera_prueba {
        background: #f5f5f5;
        padding: 2px 0%;
    }
</style>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <!-- <ol class="breadcrumb breadcum">
      <li><a href="<?php echo base_url() ?>">Inicio</a></li>
      <li class="active">Página de Inicio</li>
    </ol>
    <section class="content-header">
        <h1>
        Página de Inicio
        <small></small>
        </h1>
    </section> -->
    <!-- Main content -->
    <section class="content">
        <!-- Default box -->
        <div class="box box-solid">
            <div class="box-body">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="text-center cabecera_prueba">
                            <h3>SECCIÓN: <label id="titulo_confia_inf"></label></h3>
                        </div>
                    </div>
                </div>
                <br>
                <div class="row">
                    <div class="col-xs-12 col-md-6 col-lg-6">
                        <div class="form-group col-xs-12">
                            <label for=""><span style="color: red;">*</span> Título</label>
                            <input type="text" class="form-control" id="titulo_confia">
                        </div>
                        <div class="form-group col-xs-4">
                            <div class="relative">
                                <label><span style="color: red">*</span> Icono:<br><small></small></label>
                                <div class="bg-form-imagen" style="text-align: center;background: #f5f5f5;">
                                    <img width="auto" height="auto" id="imagenicono_nueva" style="max-width: 100%;max-height: 100%;">
                                </div>
                                <input type="file" class="form-control" name="imagen" id="imagen_icono">
                            </div>
                        </div>
                        <div class="form-group col-xs-8">
                            <label for="txt_subtitulo"><span style="color: red;">*</span> Subtitulo:</label>
                            <textarea style="height:80px;width:100%;resize:none;" class="form-control" name="txt_subtitulo" id="txt_subtitulo"></textarea>
                        </div>
                        <div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <label for="txt_confio_nosotros"><span style="color: red;">*</span> Descripcion:</label>
                            <textarea style="height:80px;width:100%;resize:none;" class="form-control" name="txt_confio_nosotros" id="txt_confio_nosotros"></textarea>
                        </div>
                    </div>
                    <div class="col-xs-12 col-md-6 col-lg-6">
                        <div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="relative">
                                <label><span style="color: red">*</span> Imagen:<br><small></small></label>
                                <div class="bg-form-imagen" style="text-align: center;background: #f5f5f5;">
                                    <img width="auto" height="auto" id="imagenconfia_nueva" style="max-width: 100%;max-height: 100%;">
                                </div>
                                <input type="file" class="form-control" name="imagen" id="imagen_confia">
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12 text-center">
                        <button class="btn btn-success" type="button" onclick="actualizar_datos()"><i class="fa fa-save"></i>&nbsp;Actualizar</button>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>

<script src="<?php echo base_url();?>public/administrador/js/inicio.js"></script>