// inicio de funciones
CKEDITOR.replace('txt_contenido');
var p_id = $('#p_id').val();
datos_politicas(p_id);

function actualizar_datos_politicas() {
    var titulo = $('#titulo_politica').val();
    var contenido = CKEDITOR.instances.txt_contenido.getData();

    var formdata = new FormData();

    // iniciando variables
    formdata.append('titulo', titulo);
    formdata.append('contenido', contenido);
    formdata.append('p_id', p_id);

    var url_modulo = base_url3 + 'admin/politicas/actualizar';

    $.ajax({
      url: url_modulo,
      type: 'POST',
      dataType: 'json',
      cache: false,
      contentType: false,
      processData: false,
      data: formdata,
      beforeSend: function()
      { 
        $("#capa_loading").css('display', 'block');
        $('html, body').animate({scrollTop:0}, 'slow');
      },
      success: function(data)
      {
        if (data.cabecera == 'M_1') {
          alertify.set('notifier','position', 'top-right');
          alertify.error(data.mensaje);
        } else if(data.cabecera == 'M_0') {
          datos_politicas(p_id);
          $("#capa_loading").css('display', 'none');
          alertify.set('notifier','position', 'top-right');
          alertify.success(data.mensaje);
        }
        console.log(data);
      }
    });
}

function datos_politicas(id) 
{
  $.ajax({
    url: base_url3 + 'admin/politicas/datos',
    type: 'POST',
    dataType: 'json',
    data: {id: id},
    success: function(data)
    {
      $('#titulo_politica').val(data.titulo);
      CKEDITOR.instances.txt_contenido.setData(data.contenido);
      return false;
    },
    error: function(e) {
      console.log("ERROR", e);
    }
  });
}