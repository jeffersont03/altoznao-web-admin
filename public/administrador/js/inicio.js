CKEDITOR.replace('txt_confio_nosotros');
CKEDITOR.replace('txt_subtitulo');

datos_empresa();

// Eventos
$("#imagen_confia").change(function(event) {
  cambio_imagen(this,'imagenconfia_nueva');
});

$("#imagen_icono").change(function(event) {
  cambio_imagen(this,'imagenicono_nueva');
});

function datos_empresa() 
{
  // e.preventDefault();
 	$.ajax({
    	url: base_url3 + 'admin/empresa/datos',
    	type: 'POST',
    	dataType: 'json',
    	data: {id: '1'},
    	success: function(data)
    	{
    		  if (data.imagenConfia == null) {
        		$('#imagenconfia_nueva').attr('src', base_url3 + 'public/frontend/img/admin_files_imagen.jpg');
      		} else {
        		$('#imagenconfia_nueva').attr('src', base_url3 + 'public/frontend/img/' + data.imagenConfia);
      		}

          if (data.iconoConfia == null) {
            $('#imagenicono_nueva').attr('src', base_url3 + 'public/frontend/img/admin_files_imagen.jpg');
          } else {
            $('#imagenicono_nueva').attr('src', base_url3 + 'public/frontend/img/' + data.iconoConfia);
          }
        CKEDITOR.instances.txt_confio_nosotros.setData(data.textoConfia);
    		CKEDITOR.instances.txt_subtitulo.setData(data.subtituloConfia);
        $('#titulo_confia').val(data.tituloConfia);
        $('#titulo_confia_inf').text(data.tituloConfia.toUpperCase());
        
    	}
  	});

}

function actualizar_datos()
{
   var textoConfia = CKEDITOR.instances.txt_confio_nosotros.getData();
	 var subtituloConfia = CKEDITOR.instances.txt_subtitulo.getData();
    var imagen = $("#imagen_confia").prop("files")[0];
    var icono = $("#imagen_icono").prop("files")[0];
    var tituloConfia = $('#titulo_confia').val();

    if (textoConfia.length <= 0) {
        alertify.set('notifier','position', 'top-right');
        alertify.error('Debes ingresar una descripcion para la "Confia en Nosotros"');
      } else {
            var formdata = new FormData();

    		// iniciando variables
    		formdata.append('textoConfia', textoConfia);
        formdata.append('imagen', imagen);
        formdata.append('subtituloConfia', subtituloConfia);
        formdata.append('icono', icono);
    		formdata.append('tituloConfia', tituloConfia);

        	var url_modulo = base_url3 + 'admin/inicio/actualizar';

        	$.ajax({
         url: url_modulo,
         type: 'POST',
         dataType: 'json',
         cache: false,
         contentType: false,
         processData: false,
         data: formdata,
          beforeSend: function()
          { 
              $("#capa_loading").css('display', 'block');
              $('html, body').animate({scrollTop:0}, 'slow');
          },
         success: function(data)
         {
            $("#capa_loading").css('display', 'none');
        	 if (data.cabecera == 'M_1') {
        		alertify.set('notifier','position', 'top-right');
    			     alertify.error(data.mensaje);
        	 } else if(data.cabecera == 'M_0') {
        		document.getElementById("imagen_confia").value = "";
        		datos_empresa();
        		alertify.set('notifier','position', 'top-right');
    			   alertify.success(data.mensaje);

        	 }
          }
        });
    }
}

function cambio_imagen(input,elemento, alto, ancho) {
  if (input.files && input.files[0]) {
    var reader = new FileReader();


    reader.onload = function(e) {
      var image = new Image();
      image.src = e.target.result;

      image.onload = function() {
       $('#' + elemento).attr('src', this.src);
     }
   }
   reader.readAsDataURL(input.files[0]);
 }
}