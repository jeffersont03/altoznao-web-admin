CKEDITOR.replace('txt_titulo_referido');
CKEDITOR.replace('txt_descripcion_referido');

datos_referido();


// Eventos
$("#img_item_uno").change(function(event) {
	cambio_imagen(this,'imagen_item_uno');
});

$("#img_item_dos").change(function(event) {
	cambio_imagen(this,'imagen_item_dos');
});

$("#img_item_tres").change(function(event) {
	cambio_imagen(this,'imagen_item_tres');
});

$("#img_card").change(function(event) {
	cambio_imagen(this,'imagen_card');
});

function datos_referido() 
{
  // e.preventDefault();
  	$.ajax({
	    url: base_url3 + 'admin/referidos/listar',
	    type: 'GET',
	    dataType: 'json',
	    success: function(data)
    	{
				console.log("DATOS",data);
				
				if (data.img1 == null) {
					$('#imagen_item_uno').attr('src', base_url3 + 'public/frontend/img/admin_files_imagen.jpg');
				} else {
					$('#imagen_item_uno').attr('src', base_url3 + 'public/frontend/img/' + data.img1);
				}
				if (data.img2 == null) {
					$('#imagen_item_dos').attr('src', base_url3 + 'public/frontend/img/admin_files_imagen.jpg');
				} else {
					$('#imagen_item_dos').attr('src', base_url3 + 'public/frontend/img/' + data.img2);
				}
				if (data.img3 == null) {
					$('#imagen_item_tres').attr('src', base_url3 + 'public/frontend/img/admin_files_imagen.jpg');
				} else {
					$('#imagen_item_tres').attr('src', base_url3 + 'public/frontend/img/' + data.img3);
				}
				if (data.card == null) {
					$('#imagen_card').attr('src', base_url3 + 'public/frontend/img/admin_files_imagen.jpg');
				} else {
					$('#imagen_card').attr('src', base_url3 + 'public/frontend/img/' + data.card);
				}
    		CKEDITOR.instances.txt_titulo_referido.setData(data.titulo);
    		CKEDITOR.instances.txt_descripcion_referido.setData(data.descripcion);
    		$('#txt_item_uno').val(data.item1);
				$('#txt_item_dos').val(data.item2);
				$('#txt_item_tres').val(data.item3);
    		$('#correo_referido').val(data.correo_referido);
    	}
 	});

}

function actualizar_referidos() {
  var titulo 			= CKEDITOR.instances.txt_titulo_referido.getData();
	var descripcion 	= CKEDITOR.instances.txt_descripcion_referido.getData();
	var item1			= $('#txt_item_uno').val();
	var item2			= $('#txt_item_dos').val();
	var item3			= $('#txt_item_tres').val();
	var imagen1 	= $("#img_item_uno").prop("files")[0];
	var imagen2 	= $("#img_item_dos").prop("files")[0];
	var imagen3 	= $("#img_item_tres").prop("files")[0];
	var card 			= $("#img_card").prop("files")[0];
	var correo_referido	= $('#correo_referido').val();

	if (titulo.length <= 0) {
		alertify.set('notifier','position', 'top-right');
		alertify.error('Debes ingresar una titulo para la sección de referidos');
	} else if (item1.length <= 0) {
		alertify.set('notifier','position', 'top-right');
		alertify.error('Debes ingresar un texto Item1 para la sección de referidos');
	} else if (item2.length <= 0) {
		alertify.set('notifier','position', 'top-right');
		alertify.error('Debes ingresar un texto Item2 para la sección de referidos');
	} else if (item3.length <= 0) {
		alertify.set('notifier','position', 'top-right');
		alertify.error('Debes ingresar un texto Item3 para la sección de referidos');
	} else if (descripcion.length <= 0) {
		alertify.set('notifier','position', 'top-right');
		alertify.error('Debes ingresar una descripción para la sección de referidos');
	} else if (correo_referido.length <= 0) {
		alertify.set('notifier','position', 'top-right');
		alertify.error('Debes ingresar un correo para la sección de referidos');
	} else {
		var formdata = new FormData();

		// iniciando variables
		formdata.append('titulo', titulo);
		formdata.append('descripcion', descripcion);
		formdata.append('item1', item1);
		formdata.append('item2', item2);
		formdata.append('item3', item3);
		formdata.append('img1', imagen1);
		formdata.append('img2', imagen2);
		formdata.append('img3', imagen3);
		formdata.append('card', card);
		formdata.append('correo_referido', correo_referido);
		
		var url_modulo = base_url3 + 'admin/referidos/actualizar';

		$.ajax({
			url: url_modulo,
			type: 'POST',
			dataType: 'json',
			cache: false,
			contentType: false,
			processData: false,
			data: formdata,
			beforeSend: function()
			{ 
				$("#capa_loading").css('display', 'block');
				$('html, body').animate({scrollTop:0}, 'slow');
			},
			success: function(data)
			{
				console.log(data);
				
				$("#capa_loading").css('display', 'none');
				if (data.cabecera == 'M_1') {
					alertify.set('notifier','position', 'top-right');
					alertify.error(data.mensaje);
				} else if(data.cabecera == 'M_0') {
					datos_referido();
					alertify.set('notifier','position', 'top-right');
					alertify.success(data.mensaje);
				}
			}
		});
  }
}

function cambio_imagen(input,elemento, alto, ancho) {
  if (input.files && input.files[0]) {
    var reader = new FileReader();


    reader.onload = function(e) {
      var image = new Image();
      image.src = e.target.result;

      image.onload = function() {
       $('#' + elemento).attr('src', this.src);
     }
   }
   reader.readAsDataURL(input.files[0]);
 }
}