$('.file_pdf').change(function(event) {
	cambio_file(this, '.titulo_pdf', event);
});

function actualizar_datos_pdf(id = null)
{
  if(id) {
    let query = `#pdf-${id}`;
    var file = $(query + ' .file_pdf').prop("files")[0];
    var titulo = $(query + ' .titulo_pdf').val();
  } else {
    var file = $("#pdf-nuevo .file_pdf").prop("files")[0];
    var titulo = $("#pdf-nuevo .titulo_pdf").val();
  }

  var formdata = new FormData();

  // iniciando variables
  formdata.append('file', file);
  formdata.append('titulo', titulo);
  formdata.append('p_id', id);
  formdata.append('type', 'doc');

  var url_modulo = base_url3 + 'admin/politicas/subir_documento';

  $.ajax({
      url: url_modulo,
      type: 'POST',
      dataType: 'json',
      cache: false,
      contentType: false,
      processData: false,
      data: formdata,
      beforeSend: function()
      {	
          $("#capa_loading").css('display', 'block');
          $('html, body').animate({scrollTop:0}, 'slow');
      },
      success: function(data)
      {
          $('#capa_loading').css('display','none');
          if (data.cabecera == 'M_1') {
            alertify.set('notifier','position', 'top-right');
            alertify.error(data.mensaje);
          } else if(data.cabecera == 'M_0') {
            $("#capa_loading").css('display', 'none');
            alertify.set('notifier','position', 'top-right');
            alertify.success(data.mensaje);

            window.location.reload();
          }
      }
  });
}

function cambio_file(input, element, event) {
  let a = event.target.parentNode.parentNode.querySelector(element);
  let pdffile = input.files[0];

  let name = pdffile.name.split('.pdf')[0];
  a.value = name;
}