// Configuraciones iniciales

var p_id = $('#p_id').val();
datos_politicas(p_id);

$('#file_politicas').change(function(event) {
	cambio_file(this, 'viewer');
});

function actualizar_datos_politicas()
{
  let titulo = $('#titulo_politica').val();
  let file = $("#file_politicas").prop("files")[0];

  let formdata = new FormData();

    // iniciando variables
    formdata.append('titulo', titulo);
    formdata.append('file', file);
    formdata.append('p_id', p_id);
    formdata.append('type', 'page');

		var url_modulo = base_url3 + 'admin/politicas/subir_documento';

		$.ajax({
        url: url_modulo,
        type: 'POST',
        dataType: 'json',
        cache: false,
        contentType: false,
        processData: false,
        data: formdata,
        beforeSend: function()
        {	
            $("#capa_loading").css('display', 'block');
            $('html, body').animate({scrollTop:0}, 'slow');
        },
        success: function(data)
        {
            $('#capa_loading').css('display','none');
            if (data.cabecera == 'M_1') {
              alertify.set('notifier','position', 'top-right');
              alertify.error(data.mensaje);
            } else if(data.cabecera == 'M_0') {
              datos_politicas(p_id);
              $("#capa_loading").css('display', 'none');
              alertify.set('notifier','position', 'top-right');
              alertify.success(data.mensaje);
            }
        }
    });
}

function datos_politicas(id) 
{
  $.ajax({
    url: base_url3 + 'admin/politicas/datos',
    type: 'POST',
    dataType: 'json',
    data: {id: id},
    success: function(data)
    {
      $('#titulo_politica').val(data.titulo);
      let split = data.contenido.split('.');
      let typefile = split[split.length-1];
      let url = base_url3 + "public/frontend/img/pdf/" + data.contenido;
      if(typefile.toLowerCase() == 'pdf') {
        $("#viewer").attr('src', url);
        $('#viewer').style.display = 'block';
      }
      else {
        $('#viewer').attr('src', '');
        $('#viewer')[0].style.display = 'none';
      }
      return false;
    },
    error: function(e) {
      console.log("ERROR", e);
    }
  });
}


function cambio_file(input, element) {
  let pdffile = input.files[0];
  if(pdffile) {
    let pdffile_url = URL.createObjectURL(pdffile);
    if(pdffile.type == 'application/pdf') {
      $('#' + element).attr('src',pdffile_url);
      $('#' + element)[0].style.display = 'block';
    }
    else {
      $('#' + element).attr('src', '');
      $('#' + element)[0].style.display = 'none';
    }
  } else {
    $('#' + element).attr('src', '');
    $('#' + element)[0].style.display = 'none';
  }
}