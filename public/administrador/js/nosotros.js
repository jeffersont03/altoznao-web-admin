// inicio de funciones
CKEDITOR.replace('txt_descripcion_nos');
CKEDITOR.replace('txt_historia');
CKEDITOR.replace('txt_valores');
CKEDITOR.replace('txt_colaboradores');
CKEDITOR.replace('txt_mision');
CKEDITOR.replace('txt_vision');
CKEDITOR.replace('txt_contacto');
datos_empresa();
correos();
redes();
telefonos();



// Eventos
$("#imagen_nosotros").change(function(event) {
	cambio_imagen(this,'imagenver_nosotros');
});

$("#imagen_uno").change(function(event) {
  cambio_imagen(this,'imagenver_uno');
});

$("#imagen_dos").change(function(event) {
  cambio_imagen(this,'imagenver_dos');
});

$("#imagen_tres").change(function(event) {
  cambio_imagen(this,'imagenver_tres');
});

$("#imagen_mapa").change(function(event) {
  cambio_imagen(this,'imagenver_mapa');
});

function actualizar_datos_empresa() {
    var nombre = 'Altozano';
    var nosotros = CKEDITOR.instances.txt_descripcion_nos.getData();
    var vision = CKEDITOR.instances.txt_vision.getData();
    var mision = CKEDITOR.instances.txt_mision.getData();
    var imagen = $("#imagen_nosotros").prop("files")[0];
    var titulo_historia = $('#titulo_historia').val();
    var titulo_valores = $('#titulo_valores').val();
    var titulo_valores_colaboradores = $('#titulo_colaboradores').val();
    var historia = CKEDITOR.instances.txt_historia.getData();
    var valores = CKEDITOR.instances.txt_valores.getData();
    var valores_colaboradores = CKEDITOR.instances.txt_colaboradores.getData();
    var contacto = CKEDITOR.instances.txt_contacto.getData();

      if (nosotros.length <= 0) {
        alertify.set('notifier','position', 'top-right');
        alertify.error('Debes ingresar una descripcion en nosotros para la empresa');
      } else {
            var formdata = new FormData();

    		// iniciando variables
    		formdata.append('nombre', nombre);
    		formdata.append('nosotros', nosotros);
    		formdata.append('vision', vision);
        formdata.append('mision', mision);
        formdata.append('imagen', imagen);
        formdata.append('titulo_historia', titulo_historia);
        formdata.append('titulo_valores', titulo_valores);
        formdata.append('titulo_valores_colaboradores', titulo_valores_colaboradores);
        formdata.append('historia', historia);
        formdata.append('valores', valores);
        formdata.append('valores_colaboradores', valores_colaboradores);
        formdata.append('contacto', contacto);

        var url_modulo = base_url3 + 'admin/empresa/actualizar';

        $.ajax({
         url: url_modulo,
         type: 'POST',
         dataType: 'json',
         cache: false,
         contentType: false,
         processData: false,
         data: formdata,
          beforeSend: function()
          { 
              $("#capa_loading").css('display', 'block');
              $('html, body').animate({scrollTop:0}, 'slow');
          },
         success: function(data)
         {
        	//alertify.dismissAll();
        	if (data.cabecera == 'M_1') {
        		alertify.set('notifier','position', 'top-right');
    			     alertify.error(data.mensaje);
        	} else if(data.cabecera == 'M_0') {
        		document.getElementById("imagen_nosotros").value = "";
        		datos_empresa();
            $("#capa_loading").css('display', 'none');
        		alertify.set('notifier','position', 'top-right');
    			   alertify.success(data.mensaje);

        	}
            console.log(data);
          }
        });
    }
}

function datos_empresa() 
{
  // e.preventDefault();
  $.ajax({
    url: base_url3 + 'admin/empresa/datos',
    type: 'POST',
    dataType: 'json',
    data: {id: '1'},
    success: function(data)
    {

      if (data.imagen == null) {
        $('#imagenmuestra_nosotros').attr('src', base_url3 + 'public/frontend/img/admin_files_imagen.jpg');
      } else {
        $('#imagenmuestra_nosotros').attr('src', base_url3 + 'public/frontend/img/' + data.imagen);
      }
      $('#txt_nombre_empresa').val(data.nombre);
      CKEDITOR.instances.txt_descripcion_nos.setData(data.nosotros);
      $('#titulo_historia').val(data.titulo_historia);
      CKEDITOR.instances.txt_historia.setData(data.historia);
      $('#titulo_valores').val(data.titulo_valores);
      CKEDITOR.instances.txt_valores.setData(data.valores);
      $('#titulo_colaboradores').val(data.titulo_valores_colaboradores);
      CKEDITOR.instances.txt_colaboradores.setData(data.valores_colaboradores);
      CKEDITOR.instances.txt_mision.setData(data.mision);
      CKEDITOR.instances.txt_vision.setData(data.vision);
      CKEDITOR.instances.txt_contacto.setData(data.contacto);
      return false;
    }
  });

}

function correos() {
	var url_modulo = base_url3+'admin/empresa/correos';

  $('#tabla_correos').dataTable().fnDestroy();
  var tablass = $('#tabla_correos').DataTable({
    'ajax': {
     "type"	: "POST",
     "url"	: url_modulo,
     "data"	: function(d) {
      d.id = '1';
    },
    "dataSrc": ""
  },
  'language': {
    "search": 'Buscar:',
    "emptyTable": "No se encuentran datos para mostrar.",
    "paginate": {
      "first": "Primero",
      "previous": "Atr&aacute;s",
      "next": "Adelante",
      "last": "&Uacute;ltimo"
    },
    "infoEmpty": "Mostrando 0 al 0 de 0 entradas",
    "lengthMenu": "Mostrar _MENU_ entradas",
    "info": "Mostrando _START_ al _END_ de _TOTAL_ entradas"
  },
  'lengthMenu': [5, 10, 25, 50, 75, 100],

  'columns': [
  {"data": "correos"},
  {
    "data": "id",
    "render" : function(data, type, row) {
      return '<button type="button" class="btn btn-info" onclick="mostrar_editar_correo(' + data + ')"><i class="fa fa-pencil"></i></button>'
    } 
  },
  ],
  'columnDefs': [
  {
    "targets": [1],
    "className": "text-center",
       			//"width": "10%"
       		}
          ],
          'order': [[ 0, "desc" ]]
        });

  tablass.ajax.reload();
}

function eliminar_comun(id,tabla)
{	
	alertify.confirm('JVG Inmobiliaria', '¿Seguro desea eliminar este item?', 
		function(){ 
			$.ajax({
				url: base_url3 + 'admin/empresa/comuneliminar',
				type: 'POST',
				dataType: 'json',
				data: {
					id: id,
					tabla: tabla
				},
				success: function(data)
				{
					if (data.cabecera == 'M_1') {
            alertify.set('notifier','position', 'top-right');
            alertify.error(data.mensaje);
          } else if(data.cabecera == 'M_0') {
            correos();
            redes();
            telefonos();
            alertify.set('notifier','position', 'top-right');
            alertify.success(data.mensaje);

          }
        }
      });
		}, 
    function(){ 
        	// alertify.error('Cancel')
        });	
	
}

function redes() {
	var url_modulo = base_url3+'admin/empresa/redes';

  $('#tabla_redes').dataTable().fnDestroy();
  var tablass = $('#tabla_redes').DataTable({
    'ajax': {
     "type"	: "POST",
     "url"	: url_modulo,
     "data"	: function(d) {
      d.id = '1';
    },
    "dataSrc": ""
  },
  'language': {
    "search": 'Buscar:',
    "emptyTable": "No se encuentran datos para mostrar.",
    "paginate": {
      "first": "Primero",
      "previous": "Atr&aacute;s",
      "next": "Adelante",
      "last": "&Uacute;ltimo"
    },
    "infoEmpty": "Mostrando 0 al 0 de 0 entradas",
    "lengthMenu": "Mostrar _MENU_ entradas",
    "info": "Mostrando _START_ al _END_ de _TOTAL_ entradas"
  },
  'lengthMenu': [5, 10, 25, 50, 75, 100],

  'columns': [
  {"data": "redes"},
  {
    "data": "icono",
    "render" : function(data, type, row) {
      return '<img src="' + base_url3 + 'public/frontend/img/' + data + '">'
    } 
  },
  {
    "data": "id",
    "render" : function(data, type, row) {
      return '<button type="button" class="btn btn-info" onclick="mostrar_editar_red(' + data + ')"><i class="fa fa-pencil"></i></button>&nbsp;&nbsp;<button type="button" class="btn btn-danger" onclick="eliminar_comun('+data+',\'redes_sociales\')"><i class="fa fa-trash-o"></i></button>'
    } 
  }
  ],
  'columnDefs': [
  {
    "targets": [1,2],
    "className": "text-center",
       			//"width": "10%"
       		}
          ],
          'order': [[ 0, "desc" ]]
        });

  tablass.ajax.reload();
}

function telefonos() {
	
	var url_modulo = base_url3+'admin/empresa/telefonos';

  $('#tabla_telefonos').dataTable().fnDestroy();
  var tablass = $('#tabla_telefonos').DataTable({
    'ajax': {
     "type"	: "POST",
     "url"	: url_modulo,
     "data"	: function(d) {
      d.id = '1';
    },
    "dataSrc": ""
  },
  'language': {
    "search": 'Buscar:',
    "emptyTable": "No se encuentran datos para mostrar.",
    "paginate": {
      "first": "Primero",
      "previous": "Atr&aacute;s",
      "next": "Adelante",
      "last": "&Uacute;ltimo"
    },
    "infoEmpty": "Mostrando 0 al 0 de 0 entradas",
    "lengthMenu": "Mostrar _MENU_ entradas",
    "info": "Mostrando _START_ al _END_ de _TOTAL_ entradas"
  },
  'lengthMenu': [5, 10, 25, 50, 75, 100],

  'columns': [
  {"data": "telefono"},
  {
    "data": "id",
    "render" : function(data, type, row) {
      return '<button type="button" class="btn btn-info" onclick="mostrar_editar_telefonos(' + data + ')"><i class="fa fa-pencil"></i></button>&nbsp;&nbsp;<button type="button" class="btn btn-danger" onclick="eliminar_comun('+data+',\'telefonos\')"><i class="fa fa-trash-o"></i></button>'
    } 
  },
  ],
  'columnDefs': [
  {
    "targets": [1],
    "className": "text-center",
       			//"width": "10%"
       		}
          ],
          'order': [[ 0, "desc" ]]
        });

  tablass.ajax.reload();
}

function mostrar_agregar_correo()
{
  $('#modal_agregar_correo').modal('show');
  $('#modal_agregar_correo').on('shown.bs.modal', function () {
    $('#nuevo_correo').focus();
  });
}

function mostrar_agregar_telefono()
{
  $('#modal_agregar_telefono').modal('show');
  $('#modal_agregar_telefono').on('shown.bs.modal', function () {
    $('#nuevo_telefono').focus();
  });
}

function mostrar_agregar_red()
{
  $('#modal_agregar_red').modal('show');
}

function nuevo_correo()
{
  var nuevo_correo  = $('#nuevo_correo').val();

  if (nuevo_correo.length <= 0) {
    alertify.set('notifier','position', 'top-right');
    alertify.error('Debes ingresar un nombre para el correo.');
  } else {
    $.ajax({
      url: base_url3 + 'admin/correos/nuevos',
      type: 'POST',
      dataType: 'json',
      data: {
        correo: nuevo_correo
      },
      success: function(data)
      {
            // alertify.dismissAll();
            if (data.cabecera == 'M_1') {
              alertify.set('notifier','position', 'top-right');
              alertify.error(data.mensaje);
            } else if(data.cabecera == 'M_0') {
              $('#nuevo_correo').val('');
              $('#modal_agregar_correo').modal('hide');
              correos();
              alertify.set('notifier','position', 'top-right');
              alertify.success(data.mensaje);
            }
          }
        });
  }
}

function nuevo_telefono()
{
  var nuevo_telefono  = $('#nuevo_telefono').val();

  if (nuevo_telefono.length <= 0) {
    alertify.set('notifier','position', 'top-right');
    alertify.error('Debes ingresar un número para el teléfono.');
  } else {
    $.ajax({
      url: base_url3 + 'admin/telefonos/nuevos',
      type: 'POST',
      dataType: 'json',
      data: {
        telefono: nuevo_telefono
      },
      success: function(data)
      {
            // alertify.dismissAll();
            if (data.cabecera == 'M_1') {
              alertify.set('notifier','position', 'top-right');
              alertify.error(data.mensaje);
            } else if(data.cabecera == 'M_0') {
              $('#nuevo_telefono').val('');
              $('#modal_agregar_telefono').modal('hide');
              telefonos();
              alertify.set('notifier','position', 'top-right');
              alertify.success(data.mensaje);
            }
          }
        });
  }
}

function nuevo_red()
{
  var nuevo_link  = $('#nuevo_link').val();
  var id_red_nueva  = $('#id_red_nueva').val();

  if (nuevo_link.length <= 0) {
    alertify.set('notifier','position', 'top-right');
    alertify.error('Debes ingresar un link para la red social.');
  } else if (id_red_nueva == 0) {
    alertify.set('notifier','position', 'top-right');
    alertify.error('Debes seleccionar una red social.');
  } else {
    $.ajax({
      url: base_url3 + 'admin/redes/nuevos',
      type: 'POST',
      dataType: 'json',
      data: {
        link: nuevo_link,
        redes_temporal: id_red_nueva
      },
      success: function(data)
      {
            // alertify.dismissAll();
            if (data.cabecera == 'M_1') {
              alertify.set('notifier','position', 'top-right');
              alertify.error(data.mensaje);
            } else if(data.cabecera == 'M_0') {
              $('#nuevo_link').val('');
              $("#id_red_nueva").val(0);
              $('#modal_agregar_red').modal('hide');
              redes();
              alertify.set('notifier','position', 'top-right');
              alertify.success(data.mensaje);
            }
          }
        });
  }
}

function mostrar_editar_correo(id)
{

  $.ajax({
    url: base_url3 + 'admin/empresa/comunbuscar',
    type: 'POST',
    dataType: 'json',
    data: {
      id: id,
      editar: 'correos'
    },
    success: function(data)
    {
      $('#id_correo_editar').val(data.id);
      $('#editar_correo').val(data.correos);
      $('#modal_editar_correo').modal('show');
      $('#modal_editar_correo').on('shown.bs.modal', function () {
        $('#editar_correo').focus();
      });
    }
  });

}

function mostrar_editar_telefonos(id)
{
  $.ajax({
    url: base_url3 + 'admin/empresa/comunbuscar',
    type: 'POST',
    dataType: 'json',
    data: {
      id: id,
      editar: 'telefonos'
    },
    success: function(data)
    {
      console.log(data);
      $('#id_telefono_editar').val(data.id);
      $('#editar_telefono').val(data.telefono);
      $('#modal_editar_telefono').modal('show');
      $('#modal_editar_telefono').on('shown.bs.modal', function () {
        $('#modal_editar_telefono').focus();
      });
    }
  });
}

function mostrar_editar_red(id)
{
   $.ajax({
    url: base_url3 + 'admin/empresa/comunbuscar',
    type: 'POST',
    dataType: 'json',
    data: {
      id: id,
      editar: 'redes_sociales'
    },
    success: function(data)
    {
      console.log(data);
      $('#id_red_editar').val(data.id);
      $('#editar_link').val(data.link);
      $('#id_red_editar_cmb').val(data.icono + '_' +data.redes.toLowerCase());
      $('#modal_editar_red').modal('show');
      $('#modal_editar_red').on('shown.bs.modal', function () {
        $('#editar_link').focus();
      });
    }
  });
}

function editar_correo()
{
  var id  = $('#id_correo_editar').val();
  var correo  = $('#editar_correo').val();

  if (correo.length <= 0) {
    alertify.set('notifier','position', 'top-right');
    alertify.error('Debes ingresar un correo.');
  } else {
    $.ajax({
      url: base_url3 + 'admin/correos/editar',
      type: 'POST',
      dataType: 'json',
      data: {
        id: id,
        correos: correo
      },
      success: function(data)
      {
            // alertify.dismissAll();
            if (data.cabecera == 'M_1') {
              alertify.set('notifier','position', 'top-right');
              alertify.error(data.mensaje);
            } else if(data.cabecera == 'M_0') {
              $('#modal_editar_correo').modal('hide');
              correos();
              alertify.set('notifier','position', 'top-right');
              alertify.success(data.mensaje);
            }
          }
        });
  }
}

function editar_telefono()
{
  var id  = $('#id_telefono_editar').val();
  var telefono  = $('#editar_telefono').val();

  if (telefono.length <= 0) {
    alertify.set('notifier','position', 'top-right');
    alertify.error('Debes ingresar un telefono.');
  } else {
    $.ajax({
      url: base_url3 + 'admin/telefonos/editar',
      type: 'POST',
      dataType: 'json',
      data: {
        id: id,
        telefonos: telefono
      },
      success: function(data)
      {
            // alertify.dismissAll();
            if (data.cabecera == 'M_1') {
              alertify.set('notifier','position', 'top-right');
              alertify.error(data.mensaje);
            } else if(data.cabecera == 'M_0') {
              $('#modal_editar_telefono').modal('hide');
              telefonos();
              alertify.set('notifier','position', 'top-right');
              alertify.success(data.mensaje);
            }
          }
        });
  }
}

function editar_red()
{
  var id  = $('#id_red_editar').val();
  var link  = $('#editar_link').val();
  // var redes_temporal  = $('#id_red_editar_cmb').val();

  if (link.length <= 0) {
    alertify.set('notifier','position', 'top-right');
    alertify.error('Debes ingresar un link.');
  } else {
    $.ajax({
      url: base_url3 + 'admin/redes/editar',
      type: 'POST',
      dataType: 'json',
      data: {
        id: id,
        link: link/*,
        redes_temporal: redes_temporal*/
      },
      success: function(data)
      {
            // alertify.dismissAll();
            if (data.cabecera == 'M_1') {
              alertify.set('notifier','position', 'top-right');
              alertify.error(data.mensaje);
            } else if(data.cabecera == 'M_0') {
              $('#modal_editar_red').modal('hide');
              redes();
              alertify.set('notifier','position', 'top-right');
              alertify.success(data.mensaje);
            }
          }
        });
  }
}

function cambio_imagen(input,elemento, alto, ancho) {
  if (input.files && input.files[0]) {
    var reader = new FileReader();


    reader.onload = function(e) {
      var image = new Image();
      image.src = e.target.result;

      image.onload = function() {
       $('#' + elemento).attr('src', this.src);
     }
   }
   reader.readAsDataURL(input.files[0]);
 }
}