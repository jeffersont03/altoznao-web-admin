// Configuraciones iniciales

get_proyectos();
CKEDITOR.replace('txt_descripcion');
CKEDITOR.replace('editar_descripcion');
CKEDITOR.replace('galeria_txt_descripcion');
CKEDITOR.replace('plano_txt_descripcion_editar');
CKEDITOR.replace('txt_lista_uno');
CKEDITOR.replace('txt_lista_dos');
CKEDITOR.replace('txt_lista_tres');
CKEDITOR.replace('txt_item_uno_editar');
CKEDITOR.replace('txt_item_dos_editar');
CKEDITOR.replace('txt_item_tres_editar');

$('#imagenmuestra_principal').attr('src', base_url3 + 'public/frontend/img/admin_files_imagen.jpg');
$('#imagenmuestra_logo').attr('src', base_url3 + 'public/frontend/img/admin_files_imagen.jpg');
$('#imagenmuestra_fachada').attr('src', base_url3 + 'public/frontend/img/admin_files_imagen.jpg');
$('#imagenmuestra_maps').attr('src', base_url3 + 'public/frontend/img/admin_files_imagen.jpg');
$('#img_proto_galeria').attr('src', base_url3 + 'public/frontend/img/galeria_proto.jpg');
$('#img_proto_planos').attr('src', base_url3 + 'public/frontend/img/plano_proto.jpg');

$('#editar_imagenmuestra_logo').attr('src', base_url3 + 'public/frontend/img/admin_files_imagen.jpg');
$('#editar_imagenmuestra_fachada').attr('src', base_url3 + 'public/frontend/img/admin_files_imagen.jpg');
$('#editar_imagenmuestra_principal').attr('src', base_url3 + 'public/frontend/img/admin_files_imagen.jpg');
$('#editar_imagenmuestra_maps').attr('src', base_url3 + 'public/frontend/img/admin_files_imagen.jpg');
$('#imagengaleria_nueva').attr('src', base_url3 + 'public/frontend/img/admin_files_imagen.jpg');
$('#imagenplano_nueva').attr('src', base_url3 + 'public/frontend/img/admin_files_imagen.jpg');
$('#imagengaleria_editar').attr('src', base_url3 + 'public/frontend/img/admin_files_imagen.jpg');

// Eventos
$("#imagen_logo").change(function(event) {
	cambio_imagen(this,'imagenmuestra_logo');
});

$("#imagen_fachada").change(function(event) {
	cambio_imagen(this,'imagenmuestra_fachada');
});

$("#imagen_principal").change(function(event) {
	cambio_imagen(this,'imagenmuestra_principal');
});

$('#imagen_maps').change(function(event) {
	cambio_imagen(this,'imagenmuestra_maps');
});

$('#imagen_galeria').change(function(event) {
	cambio_imagen(this,'imagengaleria_nueva');
});

$('#imagen_plano').change(function(event) {
	cambio_imagen(this,'imagenplano_nueva');
});

$('#imagen_galeria_editar').change(function(event) {
	cambio_imagen(this,'imagengaleria_editar');
});

$('#imagen_plano_editar').change(function(event) {
	cambio_imagen(this,'imagenplano_editar');
});

$('#editar_imagen_logo').change(function(event) {
	cambio_imagen(this,'editar_imagenmuestra_logo');
});

$('#editar_imagen_fachada').change(function(event) {
	cambio_imagen(this,'editar_imagenmuestra_fachada');
});

$('#editar_imagen_principal').change(function(event) {
	cambio_imagen(this,'editar_imagenmuestra_principal');
});

$('#editar_imagen_maps').change(function(event) {
	cambio_imagen(this,'editar_imagenmuestra_maps');
});

// Funciones

function limpiar()
{
	// limpiando
	$('#nuevo_nombre').val(null);
	$('#nuevo_estado').val(null);
	$('#nueva_etapas').val(null);
	$('#nuevo_departamento').val(null);
	$('#nuevo_correo').val(null);
	$('#nuevo_tipo_proyecto').val(0);
	$('#imagenmuestra_principal').attr('src', base_url3 + 'public/frontend/img/admin_files_imagen.jpg');
	$('#imagenmuestra_logo').attr('src', base_url3 + 'public/frontend/img/admin_files_imagen.jpg');
	$('#imagenmuestra_fachada').attr('src', base_url3 + 'public/frontend/img/admin_files_imagen.jpg');
	document.getElementById("imagen_logo").value = "";
	document.getElementById("imagen_fachada").value = "";
	document.getElementById("imagen_principal").value = "";
	CKEDITOR.instances.txt_descripcion.setData('');
	$('#txt_comentarios').val(null);
}

function nuevo_plano() {
	var id_proyecto = $('#editar_id_proyecto').val();
	var nombre 		= $('#nombre_plano').val();
	var imagen 		= $("#imagen_plano").prop("files")[0];
	var descripcion = CKEDITOR.instances.galeria_txt_descripcion.getData();

	if (nombre.length <= 0) {
		alertify.set('notifier','position', 'top-right');
		alertify.error('Debes ingresar un Nombre para la galeria');
	/*} else if (descripcion.length <= 0) {
		alertify.set('notifier','position', 'top-right');
		alertify.error('Debes ingresar una descripción para la galeria');*/
	} else if(document.getElementById("imagen_plano").files.length == 0) {
		alertify.set('notifier','position', 'top-right');
		alertify.error('Debes ingresar una imagen para la galeria');
	} else {
		var formdata = new FormData();

		// iniciando variables
		formdata.append('nombre', nombre);
		formdata.append('id_proyecto', id_proyecto);
		formdata.append('imagen', imagen);
		formdata.append('descripcion', descripcion);
		var url_modulo = base_url3+'admin/planos/nuevo';

		$.ajax({
			url: url_modulo,
			type: 'POST',
			dataType: 'json',
	        cache: false,
	        contentType: false,
	        processData: false,
	        data: formdata,
	        beforeSend: function()
	        {	
	           	$("#capa_loading").css('display', 'block');
              	$('html, body').animate({scrollTop:0}, 'slow');
	        },
	        success: function(data)
	        {
	        	$('#capa_loading').css('display','none');
	        	if (data.cabecera == 'M_1') {
	        		alertify.set('notifier','position', 'top-right');
					alertify.error(data.mensaje);
	        	} else if(data.cabecera == 'M_0') {
	        		$('#modal_nuevo_plano').modal('hide');
	        		$('#nombre_plano').val(null);
	        		$('#imagenplano_nueva').attr('src', base_url3 + 'public/frontend/img/admin_files_imagen.jpg');
	        		CKEDITOR.instances.galeria_txt_descripcion.setData();
	        		document.getElementById("imagen_plano").value = "";
	        		get_planos(id_proyecto);
	        		$('html, body').animate({scrollBottom:0}, 'slow');
	        	}
	        }
		});
	}
}

function editar_galeria()
{
	var id_galeria 	= $('#id_galeria_editar').val();
	var nombre 		= $('#nombre_galeria_editar').val();
	var imagen 		= $("#imagen_galeria_editar").prop("files")[0];

	if (nombre.length <= 0) {
		alertify.set('notifier','position', 'top-right');
		alertify.error('Debes ingresar un Nombre para la galeria');
	} else {
		var formdata = new FormData();

		// iniciando variables
		formdata.append('nombre', nombre);
		formdata.append('id_galeria', id_galeria);
		formdata.append('imagen', imagen);

		var url_modulo = base_url3 + 'admin/galeria/actualizar';

		$.ajax({
			url: url_modulo,
			type: 'POST',
			dataType: 'json',
	        cache: false,
	        contentType: false,
	        processData: false,
	        data: formdata,
	         beforeSend: function()
	        {	
	           	$("#capa_loading").css('display', 'block');
              $('html, body').animate({scrollTop:0}, 'slow');
	        },
	        success: function(data)
	        {
	        	$('#capa_loading').css('display','none');
	        	if (data.cabecera == 'M_1') {
	        		alertify.set('notifier','position', 'top-right');
					alertify.error(data.mensaje);
	        	} else if(data.cabecera == 'M_0') {
	        		$('#modal_editar_galeria').modal('hide');
	        		document.getElementById("imagen_galeria_editar").value = "";
	        		get_galerias($('#editar_id_proyecto').val());
	        		$('html, body').animate({scrollBottom:0}, 'slow');
	        		alertify.set('notifier','position', 'top-right');
					alertify.success(data.mensaje);
	        	}
	        }
		});
	}
}

function editar_plano()
{
	var id_plano 	= $('#id_plano_editar').val();
	var nombre 		= $('#nombre_plano_editar').val();
	var imagen 		= $("#imagen_plano_editar").prop("files")[0];
	var descripcion = CKEDITOR.instances.plano_txt_descripcion_editar.getData();

	if (nombre.length <= 0) {
		alertify.set('notifier','position', 'top-right');
		alertify.error('Debes ingresar un nombre para el plano.');
	/*} else if (descripcion.length <= 0) {
		alertify.set('notifier','position', 'top-right');
		alertify.error('Debes ingresar una descripción para el plano.');*/
	} else {
		var formdata = new FormData();

		// iniciando variables
		formdata.append('nombre', nombre);
		formdata.append('id_plano', id_plano);
		formdata.append('imagen', imagen);
		formdata.append('descripcion', descripcion);

		var url_modulo = base_url3 + 'admin/plano/actualizar';

		$.ajax({
			url: url_modulo,
			type: 'POST',
			dataType: 'json',
	        cache: false,
	        contentType: false,
	        processData: false,
	        data: formdata,
	        beforeSend: function()
	        {	
	           	$("#capa_loading").css('display', 'block');
              	$('html, body').animate({scrollTop:0}, 'slow');
	        },
	        success: function(data)
	        {
	        	$('#capa_loading').css('display','none');
	        	if (data.cabecera == 'M_1') {
	        		alertify.set('notifier','position', 'top-right');
					alertify.error(data.mensaje);
	        	} else if(data.cabecera == 'M_0') {
	        		$('#modal_editar_plano').modal('hide');
	        		document.getElementById("imagen_plano_editar").value = "";
	        		get_planos($('#editar_id_proyecto').val());
	        		$('html, body').animate({scrollBottom:0}, 'slow');
	        		alertify.set('notifier','position', 'top-right');
					alertify.success(data.mensaje);
	        	}
	        }
		});
	}
}

function mostrar_editar_galeria(id)
{
	var url_modulo = base_url3+'admin/galeria/listar';

	$.ajax({
		url: url_modulo,
		type: 'POST',
		dataType: 'json',
		data: {id_galeria: id},
		beforeSend: function()
        {	
           	$("#capa_loading").css('display', 'block');
            $('html, body').animate({scrollTop:0}, 'slow');
        },
        success: function(data)
        {
	        $('#capa_loading').css('display','none');
			$('#id_galeria_editar').val(data.id);
			$('#nombre_galeria_editar').val(data.titulo);
			$('#imagengaleria_editar').attr('src', base_url3 + 'public/frontend/img/proyecto/' + data.imagen);
		}
	});

	$('#modal_editar_galeria').modal('show');
	$('#modal_editar_galeria').on('shown.bs.modal', function () {
	    $('#nombre_galeria_editar').focus();
	});
}

function modal_editar_plano(id)
{
	var url_modulo = base_url3+'admin/plano/listar';

	$.ajax({
		url: url_modulo,
		type: 'POST',
		dataType: 'json',
		data: {id_plano: id},
		beforeSend: function()
        {	
           	$("#capa_loading").css('display', 'block');
              	$('html, body').animate({scrollTop:0}, 'slow');
        },
        success: function(data)
        {
	        $('#capa_loading').css('display','none');
			$('#id_plano_editar').val(data.id);
			$('#nombre_plano_editar').val(data.titulo);
			$('#imagenplano_editar').attr('src', base_url3 + 'public/frontend/img/proyecto/' + data.imagen);
			CKEDITOR.instances.plano_txt_descripcion_editar.setData(data.descripcion);
		}
	});

	$('#modal_editar_plano').modal('show');
	$('#modal_editar_plano').on('shown.bs.modal', function () {
	    $('#nombre_plano_editar').focus();
	});
}

function eliminar_galeria(id) {

	alertify.confirm('JVG Inmobiliaria', '¿Seguro desea eliminar esta galería?', 
		function(){ 
			var url_modulo = base_url3+'admin/galeria/eliminar';
			$.ajax({
				url: url_modulo,
				type: 'POST',
				dataType: 'json',
				data: {id_galeria: id},
				beforeSend: function()
		        {	
		           	$("#capa_loading").css('display', 'block');
              	$('html, body').animate({scrollTop:0}, 'slow');
		        },
		        success: function(data)
		        {
			        $('#capa_loading').css('display','none');
					if (data.cabecera == 'M_1') {
	        		alertify.set('notifier','position', 'top-right');
					alertify.error(data.mensaje);
		        	} else if(data.cabecera == 'M_0') {
		        		alertify.set('notifier','position', 'top-right');
						alertify.success(data.mensaje);
						get_galerias($('#editar_id_proyecto').val());
		        		$('html, body').animate({scrollBottom:0}, 'slow');
		        	}
				}
			});
		}, 
        function(){ 
        	// alertify.error('Cancel')
        });	
}

function eliminar_plano(id) {

	alertify.confirm('JVG Inmobiliaria', '¿Seguro desea eliminar este plano?', 
		function(){ 
			var url_modulo = base_url3+'admin/planos/eliminar';
			$.ajax({
				url: url_modulo,
				type: 'POST',
				dataType: 'json',
				data: {id_plano: id},
				beforeSend: function()
		        {	
		           	$("#capa_loading").css('display', 'block');
              	$('html, body').animate({scrollTop:0}, 'slow');
		        },
		        success: function(data)
		        {
			        $('#capa_loading').css('display','none');
					if (data.cabecera == 'M_1') {
	        		alertify.set('notifier','position', 'top-right');
					alertify.error(data.mensaje);
		        	} else if(data.cabecera == 'M_0') {
		        		alertify.set('notifier','position', 'top-right');
						alertify.success(data.mensaje);
						get_planos($('#editar_id_proyecto').val());
		        		$('html, body').animate({scrollBottom:0}, 'slow');
		        	}
				}
			});
		}, 
        function(){ 
        	// alertify.error('Cancel')
        });	
}

function desactivar_proyecto(id) {

	alertify.confirm('JVG Inmobiliaria', '¿Seguro desea cambiar el estado visual del proyecto?', 
		function(){ 
			var url_modulo = base_url3+'admin/proyectos/desactivar';
			$.ajax({
				url: url_modulo,
				type: 'POST',
				dataType: 'json',
				data: {id_proyecto: id},
				beforeSend: function()
		        {	
		           	$("#capa_loading").css('display', 'block');
              	$('html, body').animate({scrollTop:0}, 'slow');
		        },
		        success: function(data)
		        {
			        $('#capa_loading').css('display','none');
					if (data.cabecera == 'M_1') {
	        		alertify.set('notifier','position', 'top-right');
					alertify.error(data.mensaje);
		        	} else if(data.cabecera == 'M_0') {
		        		alertify.set('notifier','position', 'top-right');
						alertify.success(data.mensaje);
						get_proyectos();
		        	}
				}
			});
		}, 
        function(){ 
        	// alertify.error('Cancel')
        });	
}

function nueva_galeria() {
	var id_proyecto = $('#editar_id_proyecto').val();
	var imagen 		= $("#imagen_galeria").prop("files")[0];
	var nombre 		= $('#nombre_galeria').val();

	if (nombre.length <= 0) {
		alertify.set('notifier','position', 'top-right');
		alertify.error('Debes ingresar un Nombre para la galeria');
	} else if(document.getElementById("imagen_galeria").files.length == 0) {
		alertify.set('notifier','position', 'top-right');
		alertify.error('Debes ingresar una imagen para la galeria');
	} else {
		var formdata = new FormData();

		// iniciando variables
		formdata.append('nombre', nombre);
		formdata.append('id_proyecto', id_proyecto);
		formdata.append('imagen', imagen);
		var url_modulo = base_url3+'admin/galeria/nueva';

		$.ajax({
			url: url_modulo,
			type: 'POST',
			dataType: 'json',
	        cache: false,
	        contentType: false,
	        processData: false,
	        data: formdata,
	        beforeSend: function()
	        {	
	           	$("#capa_loading").css('display', 'block');
              $('html, body').animate({scrollTop:0}, 'slow');
	        },
	        success: function(data)
	        {
		        $('#capa_loading').css('display','none');
	        	if (data.cabecera == 'M_1') {
	        		alertify.set('notifier','position', 'top-right');
					alertify.error(data.mensaje);
	        	} else if(data.cabecera == 'M_0') {
	        		$('#modal_nueva_galeria').modal('hide');
	        		$('#nombre_galeria').val(null);
	        		$('#imagengaleria_nueva').attr('src', base_url3 + 'public/frontend/img/admin_files_imagen.jpg');
	        		document.getElementById("imagen_galeria").value = "";
	        		get_galerias(id_proyecto);
	        		$('html, body').animate({scrollBottom:0}, 'slow');
	        	}
	        }
		});
		
	}
}

function datos_proyecto(id)
{
	var url_modulo = base_url3+'admin/proyectos/buscar';
	$.ajax({
		url: url_modulo,
		type: 'POST',
		dataType: 'json',
		data: {id_proyecto: id},
		beforeSend: function()
        {	
           	$("#capa_loading").css('display', 'block');
              	$('html, body').animate({scrollTop:0}, 'slow');
        },
        success: function(data)
        {
	        $('#capa_loading').css('display','none');
			$('#editar_id_proyecto').val(data.id);
			$('#npam').text(data.nombre)
			$('#editar_nuevo_nombre').val(data.nombre);
			$('#editar_nuevo_estado').val(data.estado);
			$('#editar_nuevo_correo').val(data.correo);
			$('#nuevo_tipo_proyecto_editar').val(data.tipo_proyecto);
			CKEDITOR.instances.editar_descripcion.setData(data.descripcion);
			CKEDITOR.instances.txt_item_uno_editar.setData(data.item1);
			CKEDITOR.instances.txt_item_dos_editar.setData(data.item2);
			CKEDITOR.instances.txt_item_tres_editar.setData(data.item3);
			$('#editar_txt_comentarios').val(data.comentarios);
			$('#nueva_etapas_editar').val(data.etapas);
			$('#nuevo_departamento_editar').val(data.departamento);
			$('#editar_imagenmuestra_logo').attr('src', base_url3 + 'public/frontend/img/proyecto/' + data.logo);
			$('#editar_imagenmuestra_fachada').attr('src', base_url3 + 'public/frontend/img/proyecto/' + data.imagen_fachada);
			$('#editar_imagenmuestra_principal').attr('src', base_url3 + 'public/frontend/img/proyecto/' + data.imagen_principal);
		}
	});
	
}

function get_proyectos(){
	var url_modulo = base_url3+'admin/proyectos/lista';
 	
 	$('#tabla_proyectos').dataTable().fnDestroy();
	var tablass = $('#tabla_proyectos').DataTable({
       	'ajax': {
       		'url': url_modulo,
       		"dataSrc": ""	
       	},
       	'language': {
            "search": 'Buscar:',
            "emptyTable": "No se encuentran datos para mostrar.",
            "paginate": {
                "first": "Primero",
                "previous": "Atr&aacute;s",
                "next": "Adelante",
                "last": "&Uacute;ltimo"
            },
            "infoEmpty": "Mostrando 0 al 0 de 0 entradas",
            "lengthMenu": "Mostrar _MENU_ entradas",
            "info": "Mostrando _START_ al _END_ de _TOTAL_ entradas"
        },
        'lengthMenu': [5, 10, 25, 50, 75, 100],

       	'columns': [
       		{
       			"data": "id",
       			"render" : function(data, type, row) {
              		return '<button type="button" class="btn btn-info" onclick="retornar_editar(' + data + ')"><i class="fa fa-pencil"></i>&nbsp;Editar</button>&nbsp;&nbsp;'+
              		'<button type="button" class="btn btn-danger" onclick="desactivar_proyecto(' + data + ')"><i class="fa fa-times"></i>&nbsp;Act/Des</button>'
          		} 
       		},
       		{"data": "nombre"},
       		{"data": "departamento"},
       		{"data": "estado"},
       		{
       			"data": "visual",
       			"render": function(data, type, row) {
       				if (data == 1) {
       					return '<span class="label label-info">Activo</span>';
       				} else {
       					return '<span class="label label-danger">Desactivado</span>';
       				}
       			}
       		},
       		{
       			"data": "url",
       			"render" : function(data, type, row) {
              		return base_url3 + 'proyectos/' +  data
          		} 
       		},
       		{
       			"data": "logo",
       			"render" : function(data, type, row) {
              		return '<img width="70" src="' + base_url3 + 'public/frontend/img/proyecto/' + data + '"/>'
          		} 
       		}
       	],
       	'columnDefs': [
       		{
       			"targets": [0,1,2,3,4,5,6],
       			"className": "text-center",
       			//"width": "10%"
       		}
       	],
       'order': [[ 0, "desc" ]]
    });

    tablass.ajax.reload();
	
}

function get_galerias(id_proyecto){
	var url_modulo = base_url3+'admin/galerias/listar';
 	
 	$('#tabla_galerias').dataTable().fnDestroy();
	var tablass = $('#tabla_galerias').DataTable({
       	'ajax': {
       		"type"	: "POST",
       		"url"	: url_modulo,
       		"data"	: function(d) {
       			d.id_proyecto = id_proyecto;
       		},
       		"dataSrc": ""
       	},
       	'language': {
            "search": 'Buscar:',
            "emptyTable": "No se encuentran datos para mostrar.",
            "paginate": {
                "first": "Primero",
                "previous": "Atr&aacute;s",
                "next": "Adelante",
                "last": "&Uacute;ltimo"
            },
            "infoEmpty": "Mostrando 0 al 0 de 0 entradas",
            "lengthMenu": "Mostrar _MENU_ entradas",
            "info": "Mostrando _START_ al _END_ de _TOTAL_ entradas"
        },
        'lengthMenu': [5, 10, 25, 50, 75, 100],

       	'columns': [
       		{
       			"data": "imagen",
       			"render" : function(data, type, row) {
              		return '<img width="70" src="' + base_url3 + 'public/frontend/img/proyecto/' + data + '"/>'
          		} 
       		},
       		{"data": "titulo"},
       		{
       			"data": "id",
       			"render" : function(data, type, row) {
              		return '<button type="button" class="btn btn-info" onclick="mostrar_editar_galeria(' + data + ')"><i class="fa fa-pencil"></i></button>&nbsp;&nbsp;<button type="button" class="btn btn-danger" onclick="eliminar_galeria('+data+')"><i class="fa fa-trash-o"></i></button>'
          		} 
       		},
       	],
       	'columnDefs': [
       		{
       			"targets": [0,2],
       			"className": "text-center",
       			//"width": "10%"
       		}
       	],
       'order': [[ 0, "desc" ]]
    });

    tablass.ajax.reload();
	
}

function get_planos(id_proyecto){
	var url_modulo = base_url3+'admin/planos/listar';
 	
 	$('#tabla_planos').dataTable().fnDestroy();
	var tablass = $('#tabla_planos').DataTable({
       	'ajax': {
       		"type"	: "POST",
       		"url"	: url_modulo,
       		"data"	: function(d) {
       			d.id_proyecto = id_proyecto;
       		},
       		"dataSrc": ""
       	},
       	'language': {
            "search": 'Buscar:',
            "emptyTable": "No se encuentran datos para mostrar.",
            "paginate": {
                "first": "Primero",
                "previous": "Atr&aacute;s",
                "next": "Adelante",
                "last": "&Uacute;ltimo"
            },
            "infoEmpty": "Mostrando 0 al 0 de 0 entradas",
            "lengthMenu": "Mostrar _MENU_ entradas",
            "info": "Mostrando _START_ al _END_ de _TOTAL_ entradas"
        },
        'lengthMenu': [5, 10, 25, 50, 75, 100],

       	'columns': [
       		{
       			"data": "imagen",
       			"render" : function(data, type, row) {
              		return '<img width="70" src="' + base_url3 + 'public/frontend/img/proyecto/' + data + '"/>'
          		} 
       		},
       		{"data": "titulo"},
       		{"data": "descripcion"},
       		{
       			"data": "id",
       			"render" : function(data, type, row) {
              		return '<button type="button" class="btn btn-info" onclick="modal_editar_plano(' + data + ')"><i class="fa fa-pencil"></i></button>&nbsp;&nbsp;<button type="button" class="btn btn-danger" onclick="eliminar_plano('+data+')"><i class="fa fa-trash-o"></i></button>'
          		} 
       		},
       	],
       	'columnDefs': [
       		{
       			"targets": [0,2,3],
       			"className": "text-center",
       			//"width": "10%"
       		}
       	],
       'order': [[ 0, "desc" ]]
    });

    tablass.ajax.reload();
	
}

function retornar_principal()
{
	$('#nuevo_proyecto').hide();
	$('#edicion_proyecto').hide();
	$('#lista_proyectos').show();
}

function retornar_nuevo()
{
	$('#lista_proyectos').hide();
	$('#edicion_proyecto').hide();
	$('#nuevo_proyecto').show();
}

function retornar_editar(id)
{
	$('#lista_proyectos').hide();
	$('#nuevo_proyecto').hide();

	get_galerias(id);
	get_planos(id);
	datos_proyecto(id);

	$('#edicion_proyecto').show();
}

function retornar_editar_especial()
{
	var id = $('#id_proyecto_temporal').val();
	$('#lista_proyectos').hide();
	$('#nuevo_proyecto').hide();

	get_galerias(id);
	get_planos(id);
	datos_proyecto(id);
	$('#modal_registro_exitoso').modal('hide');

	$('#edicion_proyecto').show();
	$('html, body').animate({scrollBottom:0}, 'slow');
}

function mostrar_modal_galeria() 
{
	$('#modal_nueva_galeria').modal('show');
	$('#modal_nueva_galeria').on('shown.bs.modal', function () {
	    $('#nombre_galeria').focus();
	})
}

function mostrar_modal_plano() 
{
	$('#modal_nuevo_plano').modal('show');
	$('#modal_nuevo_plano').on('shown.bs.modal', function () {
	    $('#nombre_plano').focus();
	})
}

function editar_proyecto()
{
	var nuevo_nombre 	= $('#editar_nuevo_nombre').val();
	var id_proyecto 	= $('#editar_id_proyecto').val();
	var nuevo_estado 	= $('#editar_nuevo_estado').val();
	var nuevo_tipo_proyecto_editar = $('#nuevo_tipo_proyecto_editar').val();
	var departamento 	= $('#nuevo_departamento_editar').val();
	var nuevo_correo    = $('#editar_nuevo_correo').val();
	var etapas    		= $('#nueva_etapas_editar').val();
	var descripcion 	 = CKEDITOR.instances.editar_descripcion.getData();
	var comentarios 	= $('#editar_txt_comentarios').val();
	var imagen_logo 	 = $("#editar_imagen_logo").prop("files")[0]; 
	var imagen_fachada 	 = $("#editar_imagen_fachada").prop("files")[0]; 
	var imagen_principal = $("#editar_imagen_principal").prop("files")[0]; 
	var item1			 = CKEDITOR.instances.txt_item_uno_editar.getData();
	var item2			 = CKEDITOR.instances.txt_item_dos_editar.getData();
	var item3 			 = CKEDITOR.instances.txt_item_tres_editar.getData();


	// validaciones
	if (nuevo_nombre.length <= 0) {
		alertify.set('notifier','position', 'top-right');
		alertify.error('Debes ingresar un Nombre para el Proyecto');
	
	} else if(nuevo_tipo_proyecto_editar == 0){
		alertify.set('notifier','position', 'top-right');
		alertify.error('Debes seleccionar un tipo de Proyecto');
	
	} else if(departamento.length <= 0) {
		alertify.set('notifier','position', 'top-right');
		alertify.error('Debes ingresar un departamento para el Proyecto');
	} else if(nuevo_correo.length <= 0) {
		alertify.set('notifier','position', 'top-right');
		alertify.error('Debes ingresar un correo para el Proyecto');
	} else {

		var formdata = new FormData();

		// iniciando variables
		formdata.append('nombre', nuevo_nombre);
		formdata.append('id', id_proyecto);
		formdata.append('estado', nuevo_estado);
		formdata.append('tipo_proyecto', nuevo_tipo_proyecto_editar);
		formdata.append('departamento', departamento);
		formdata.append('correo', nuevo_correo);
		formdata.append('etapas', etapas);
		formdata.append('descripcion', descripcion);
		formdata.append('comentarios', comentarios);
		formdata.append('logo', imagen_logo);
		formdata.append('imagen_fachada', imagen_fachada);
		formdata.append('imagen_principal', imagen_principal);
		formdata.append('item1', item1);
		formdata.append('item2', item2);
		formdata.append('item3', item3);


		var url_modulo = base_url3+'admin/proyectos/actualizar';

		$.ajax({
			url: url_modulo,
			type: 'POST',
			dataType: 'json',
	        cache: false,
	        contentType: false,
	        processData: false,
	        data: formdata,
	        beforeSend: function()
	        {	
	           	$("#capa_loading").css('display', 'block');
              $('html, body').animate({scrollTop:0}, 'slow');
	        },
	        success: function(data)
	        {
	        	$('#capa_loading').css('display','none');
	        	if (data.cabecera == 'M_1') {
	        		alertify.set('notifier','position', 'top-right');
					alertify.error(data.mensaje);
	        	} else if(data.cabecera == 'M_0') {
	        		alertify.set('notifier','position', 'top-right');
					alertify.success(data.mensaje);
	        		datos_proyecto(id_proyecto);
	        	}
	        }
		});

	}


}

function nuevo_proyecto(){
	var nuevo_nombre 	= $('#nuevo_nombre').val();
	var nuevo_estado 	= $('#nuevo_estado').val();
	var descripcion = CKEDITOR.instances.txt_descripcion.getData();
	var nueva_etapas = $('#nueva_etapas').val();
	var nuevo_tipo_proyecto	= $('#nuevo_tipo_proyecto').val();
	var nuevo_departamento	= $('#nuevo_departamento').val();
	var nuevo_correo	= $('#nuevo_correo').val();
	var comentarios = $('#txt_comentarios').val();
	var item1 = CKEDITOR.instances.txt_lista_uno.getData();
	var item2 = CKEDITOR.instances.txt_lista_dos.getData();
	var item3 = CKEDITOR.instances.txt_lista_tres.getData();

	// imagenes
	var imagen_logo 	 = $("#imagen_logo").prop("files")[0]; 
	var imagen_fachada 	 = $("#imagen_fachada").prop("files")[0]; 
	var imagen_principal = $("#imagen_principal").prop("files")[0]; 

	// validaciones
	if (nuevo_nombre.length <= 0) {
		alertify.set('notifier','position', 'top-right');
		alertify.error('Debes ingresar un Nombre para el Proyecto');
	} else if(document.getElementById("imagen_logo").files.length == 0){
		alertify.set('notifier','position', 'top-right');
		alertify.error('Debes seleccionar un logo para el Proyecto');
	} else if(document.getElementById("imagen_fachada").files.length == 0){
		alertify.set('notifier','position', 'top-right');
		alertify.error('Debes seleccionar una imagen de fachada para el Proyecto');
	} else if(document.getElementById("imagen_principal").files.length == 0){
		alertify.set('notifier','position', 'top-right');
		alertify.error('Debes seleccionar una imagen principal para el Proyecto');
	/*} else if(nuevo_estado.length <= 0) {
		alertify.set('notifier','position', 'top-right');
		alertify.error('Debes ingresar un estado para el Proyecto');*/
	} else if(nuevo_departamento.length <= 0) {
		alertify.set('notifier','position', 'top-right');
		alertify.error('Debes ingresar una departamento para el Proyecto');
	/*} else if(nueva_etapas.length <= 0) {
		alertify.set('notifier','position', 'top-right');
		alertify.error('Debes ingresar las etapas para el Proyecto');*/
	} else if(nuevo_correo.length <= 0) {
		alertify.set('notifier','position', 'top-right');
		alertify.error('Debes ingresar un correo para el Proyecto');
	} else if(nuevo_tipo_proyecto == 0){
		alertify.set('notifier','position', 'top-right');
		alertify.error('Debes seleccionar un tipo para el Proyecto');
	} else {

		var formdata = new FormData();

		// iniciando variables
		formdata.append('nombre', nuevo_nombre);
		formdata.append('estado', nuevo_estado);
		formdata.append('descripcion', descripcion);
		formdata.append('etapas', nueva_etapas);
		formdata.append('tipo_proyecto', nuevo_tipo_proyecto);
		formdata.append('departamento', nuevo_departamento);
		formdata.append('correo', nuevo_correo);
		formdata.append('comentarios', comentarios);
		formdata.append('logo', imagen_logo);
		formdata.append('imagen_fachada', imagen_fachada);
		formdata.append('imagen_principal', imagen_principal);
		formdata.append('item1', item1);
		formdata.append('item2', item2);
		formdata.append('item3', item3);

		var url_modulo = base_url3+'admin/proyectos/nuevo';

		$.ajax({
			url: url_modulo,
			type: 'POST',
			dataType: 'json',
	        cache: false,
	        contentType: false,
	        processData: false,
	        data: formdata,
	        beforeSend: function()
	        {	
	           	$("#capa_loading").css('display', 'block');
				$('html, body').animate({scrollTop:0}, 'slow');
	        },
	        success: function(data)
	        {
		        $('#capa_loading').css('display','none');
	        	if (data.cabecera == 'M_1') {
	        		alertify.set('notifier','position', 'top-right');
					alertify.error(data.mensaje);
	        	} else if(data.cabecera == 'M_0') {
	        		get_proyectos();
	        		retornar_principal();
	        		$('#modal_registro_exitoso').modal('show');
	        		$('#id_proyecto_temporal').val(data.id);
	        		limpiar();
	        		$("#capa_loading").css('display', 'none');
	        	}
	        }
		});

	}
	
}

function cambio_imagen(input,elemento, alto, ancho) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        

        reader.onload = function(e) {
            var image = new Image();
            image.src = e.target.result;

            image.onload = function() {
                 $('#' + elemento).attr('src', this.src);
            }
        }
        reader.readAsDataURL(input.files[0]);
    }
}