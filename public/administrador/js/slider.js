get_banners();
CKEDITOR.replace('txt_slider');
CKEDITOR.replace('txt_slider_editar');


$('#imagenbanner_nueva').attr('src', base_url3 + 'public/frontend/img/admin_files_imagen.jpg');
$('#imagenbanner_editar').attr('src', base_url3 + 'public/frontend/img/admin_files_imagen.jpg');

$('#imagen_banners').change(function(event) {
    cambio_imagen(this,'imagenbanner_nueva');
});

$('#imagen_banners_editar').change(function(event) {
    cambio_imagen(this,'imagenbanner_editar');
});

function get_banners(){
	var url_modulo = base_url3+'admin/banners/listar';

  $('#tabla_sliders').dataTable().fnDestroy();
  var tablass = $('#tabla_sliders').DataTable({
    'ajax': {
     'url': url_modulo,
     "dataSrc": ""	
 },
 'language': {
    "search": 'Buscar:',
    "emptyTable": "No se encuentran datos para mostrar.",
    "paginate": {
        "first": "Primero",
        "previous": "Atr&aacute;s",
        "next": "Adelante",
        "last": "&Uacute;ltimo"
    },
    "infoEmpty": "Mostrando 0 al 0 de 0 entradas",
    "lengthMenu": "Mostrar _MENU_ entradas",
    "info": "Mostrando _START_ al _END_ de _TOTAL_ entradas"
},
'lengthMenu': [5, 10, 25, 50, 75, 100],

'columns': [
    {
      "data": "id",
        "render" : function(data, type, row) {
            return '<button type="button" class="btn btn-info" onclick="retornar_editar(' + data + ')"><i class="fa fa-pencil"></i>&nbsp;Editar</button>&nbsp;&nbsp;'+
            '<button type="button" class="btn btn-danger" onclick="eliminar_slider(' + data + ')"><i class="fa fa-times"></i>&nbsp;Eliminar</button>'
        } 
    },
    {
        "data": "imagen",
        "render" : function(data, type, row) {
            return '<img width="70" src="' + base_url3 + 'public/frontend/img/' + data + '"/>'
        }
    },
    {"data": "texto"}
],
'columnDefs': [
{
  "targets": [0,1,2],
  "className": "text-center",
       			//"width": "10%"
            }
            ],
            'order': [[ 0, "desc" ]]
        });

  tablass.ajax.reload();

}

function retornar_editar(id) {
    var url_modulo = base_url3+'admin/banners/buscar';
    $.ajax({
        url: url_modulo,
        type: 'POST',
        dataType: 'json',
        data: {id_slider: id},
        beforeSend: function()
        {   
            /*alertify.set('notifier','position', 'top-right');
            alertify.success('<i class="fa fa-spinner fa-spin fa-fw"></i>&nbsp;Espere un momento');*/
        },
        success: function(data)
        {
            $('#id_banner_editar').val(data.id);
            $('#imagenbanner_editar').attr('src', base_url3 + 'public/frontend/img/' + data.imagen);
            CKEDITOR.instances.txt_slider_editar.setData(data.texto);
            $('#modal_editar_banner').modal('show');
        }
    });
}

function editar_banner()
{
    var id_banner = $('#id_banner_editar').val();
    var texto   = CKEDITOR.instances.txt_slider_editar.getData();
    var imagen      = $("#imagen_banners_editar").prop("files")[0];

   if(texto.length <= 0) {
        alertify.set('notifier','position', 'top-right');
        alertify.error('Debes ingresar un texto para el banner');
    } else {
        var formdata = new FormData();

        // iniciando variables
        formdata.append('id_banner', id_banner);
        formdata.append('texto', texto);
        formdata.append('imagen', imagen);
        var url_modulo = base_url3+'admin/banners/editar';

        $.ajax({
            url: url_modulo,
            type: 'POST',
            dataType: 'json',
            cache: false,
            contentType: false,
            processData: false,
            data: formdata,
            beforeSend: function()
            {   
                $("#capa_loading").css('display', 'block');
                $('html, body').animate({scrollTop:0}, 'slow');
            },
            success: function(data)
            {
                $("#capa_loading").css('display', 'none');
                if (data.cabecera == 'M_1') {
                    alertify.set('notifier','position', 'top-right');
                    alertify.error(data.mensaje);
                } else if(data.cabecera == 'M_0') {
                    alertify.set('notifier','position', 'top-right');
                    alertify.success(data.mensaje);
                     $('#modal_editar_banner').modal('hide');
                     get_banners();
                }
            }
        });
    }
}

function nuevo_banner() {
    var texto   = CKEDITOR.instances.txt_slider.getData();
    var imagen  = $("#imagen_banners").prop("files")[0];

    if(document.getElementById("imagen_banners").files.length == 0) {
        alertify.set('notifier','position', 'top-right');
        alertify.error('Debes ingresar una imagen para el banner');
    } else if(texto.length <= 0) {
        alertify.set('notifier','position', 'top-right');
        alertify.error('Debes un texto para el banner');
    } else {
        var formdata = new FormData();

        // iniciando variables
        formdata.append('texto', texto);
        formdata.append('imagen', imagen);
        var url_modulo = base_url3+'admin/banners/nuevo';

        $.ajax({
            url: url_modulo,
            type: 'POST',
            dataType: 'json',
            cache: false,
            contentType: false,
            processData: false,
            data: formdata,
            beforeSend: function()
            {   
                $("#capa_loading").css('display', 'block');
                $('html, body').animate({scrollTop:0}, 'slow');
            },
            success: function(data)
            {
                $("#capa_loading").css('display', 'none');
                if (data.cabecera == 'M_1') {
                    alertify.set('notifier','position', 'top-right');
                    alertify.error(data.mensaje);
                } else if(data.cabecera == 'M_0') {
                    $('#modal_nuevo_banner').modal('hide');
                    CKEDITOR.instances.txt_slider.setData('');
                    document.getElementById("imagen_banners").value = "";
                    $('#imagenbanner_nueva').attr('src', base_url3 + 'public/frontend/img/admin_files_imagen.jpg');
                    get_banners();
                    alertify.set('notifier','position', 'top-right');
                    alertify.success(data.mensaje);
                }
            }
        });
    }
}

function eliminar_slider(id) {

    alertify.confirm('JVG Inmobiliaria', '¿Seguro desea eliminar este banner?', 
        function(){ 
            var url_modulo = base_url3+'admin/banners/eliminar';
            $.ajax({
                url: url_modulo,
                type: 'POST',
                dataType: 'json',
                data: {id_banner: id},
                beforeSend: function()
                {   
                    // alertify.set('notifier','position', 'top-right');
                    // alertify.success('<i class="fa fa-spinner fa-spin fa-fw"></i>&nbsp;Espere un momento');
                },
                success: function(data)
                {
                    // alertify.dismissAll();
                    if (data.cabecera == 'M_1') {
                    alertify.set('notifier','position', 'top-right');
                    alertify.error(data.mensaje);
                    } else if(data.cabecera == 'M_0') {
                        alertify.set('notifier','position', 'top-right');
                        alertify.success(data.mensaje);
                       get_banners();
                    }
                }
            });
        }, 
        function(){ 
            // alertify.error('Cancel')
        }); 
}

function cambio_imagen(input,elemento, alto, ancho) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        

        reader.onload = function(e) {
            var image = new Image();
            image.src = e.target.result;

            image.onload = function() {
                 $('#' + elemento).attr('src', this.src);
            }
        }
        reader.readAsDataURL(input.files[0]);
    }
}